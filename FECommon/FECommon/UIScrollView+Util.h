//
//  UIScrollView+Util.h
//  Account
//
//  Created by Tracy on 3/16/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Util)

@property (nonatomic) CGFloat offsetX;
@property (nonatomic) CGFloat offsetY;
@property (nonatomic) CGFloat offsetRight;
@property (nonatomic) CGFloat offsetBottom;

@end
