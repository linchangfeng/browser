//
//  IAP.m
//  
//
//  Created by Tracy on 14-6-13.
//
//

#import "IAP.h"

//
@interface FEProductsRequest : SKProductsRequest
@property (nonatomic) int tag;
@end

@implementation FEProductsRequest
@end


//
static NSInteger const kRequestTagPurchase = 100;
static NSInteger const kRequestTagFetchPrice = 101;
static BOOL transactionObserverAdded = NO;


//
@implementation IAP

#pragma mark - Private
+ (void)addTransactionObserver {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:(id)self];
    transactionObserverAdded = YES;
}

#pragma mark - Public
+ (void)restorePreviousPurchase {
    if (!transactionObserverAdded) {
        [self addTransactionObserver];
    }
    
    if ([SKPaymentQueue canMakePayments]) {
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
}

+ (void)purchaseProductWithID:(NSString*)productID {
    if (!transactionObserverAdded) {
        [self addTransactionObserver];
    }
    
    if ([SKPaymentQueue canMakePayments]) {
        NSSet *set = [NSSet setWithArray:@[productID]];
        FEProductsRequest *request = [[FEProductsRequest alloc] initWithProductIdentifiers:set];
        request.delegate = (id)self;
        request.tag = kRequestTagPurchase;
        [request start];
    }
}

+ (void)fetchPriceOfProductWithIDs:(NSArray*)productIDs {
    if (!transactionObserverAdded) {
        [self addTransactionObserver];
    }
    
    if ([SKPaymentQueue canMakePayments] && productIDs.count > 0) {
        NSSet *set = [NSSet setWithArray:productIDs];
        FEProductsRequest *request = [[FEProductsRequest alloc] initWithProductIdentifiers:set];
        request.delegate = (id)self;
        request.tag = kRequestTagFetchPrice;
        [request start];
    }
}

#pragma mark - Private
+ (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    BOOL isFetchingPrice = (((FEProductsRequest*)request).tag == kRequestTagFetchPrice);
    if (response.products.count == 0) {
        if (isFetchingPrice) {
            FEPostNotification(FetchProductPriceFailedNotification, nil, nil);
        }
        else {
            FEPostNotification(FetchProductInfoFailedNotification, nil, nil);
        }
    }
    else {
        if (isFetchingPrice) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
            for (SKProduct *p in response.products) {
                NSNumberFormatter *priceFormatter = [[NSNumberFormatter alloc] init];
                [priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                [priceFormatter setLocale:p.priceLocale];
                [userInfo setValue:[priceFormatter stringFromNumber:p.price] forKey:p.productIdentifier];
            }
            
            FEPostNotification(FetchProductPriceCompletedNotification, nil, userInfo);
        }
        else {
            SKPayment *payment = [SKPayment paymentWithProduct:[response.products lastObject]];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
    }
}

+ (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    if (queue.transactions.count == 0) {
        FEPostNotification(RestorePurchaseFailedNotification, nil, nil);
    }
}

+ (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    FEPostNotification(RestorePurchaseFailedNotification, nil, @{ @"error": error });
}

+ (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions) {
        NSDictionary *userInfo = nil;
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchasing: //商品添加进列表
                break;
            case SKPaymentTransactionStatePurchased: //交易完成
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                userInfo = @{ @"productID": transaction.payment.productIdentifier };
                FEPostNotification(PurchaseProductCompletedNofitication, nil, userInfo);
                break;
            case SKPaymentTransactionStateFailed: //交易失败
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                userInfo = @{ @"productID": transaction.payment.productIdentifier };
                FEPostNotification(PurchaseProductFailedNotification, nil, userInfo);
                break;
            case SKPaymentTransactionStateRestored: //恢复购买
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                userInfo = @{ @"productID": transaction.payment.productIdentifier };
                FEPostNotification(RestoriePurchaseCompletedNotificatoin, nil, userInfo);
                break;
            default:
                break;
        }
    }
}


@end
