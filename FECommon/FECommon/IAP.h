//
//  IAP.h
//  
//
//  Created by Tracy on 14-6-13.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#define FetchProductPriceCompletedNotification @"FetchProductPriceCompletedNotification"
#define FetchProductPriceFailedNotification @"FetchProductPriceFailedNotification"

#define FetchProductInfoFailedNotification @"FetchProductInfoFailedNotification"

#define PurchaseProductCompletedNofitication @"PurchaseProductCompletedNofitication"
#define PurchaseProductFailedNotification @"PurchaseProductFailedNotification"

#define RestoriePurchaseCompletedNotificatoin @"RestoriePurchaseCompletedNotificatoin"
#define RestorePurchaseFailedNotification @"RestorePurchaseFailedNotification"

@interface IAP : NSObject

+ (void)restorePreviousPurchase;
+ (void)purchaseProductWithID:(NSString*)productID;
+ (void)fetchPriceOfProductWithIDs:(NSArray*)productIDs;

@end
