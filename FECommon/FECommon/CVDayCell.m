//
//  CVDayCell.m
//  Accounts
//
//  Created by Tracy on 15/9/21.
//  Copyright (c) 2015年 Tracy. All rights reserved.
//

#import "CVDayCell.h"

@interface CVDayCell() {
    UIView *_highlightBackground;
}

@end

@implementation CVDayCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {        
        _highlightBackground = [[UIView alloc] init];
        _highlightBackground.backgroundColor = _highlightedBackgroundColor;
        _highlightBackground.hidden = YES;
        [self addSubview:_highlightBackground];
        
        _textLabel = [[UILabel alloc] init];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_textLabel];
        
        _todaySymbol = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 3.0, 3.0)];
        _todaySymbol.backgroundColor = FEHexRGBA(0xFA5033, 1.0);
        _todaySymbol.layer.cornerRadius = _todaySymbol.width/2.0;
        _todaySymbol.hidden = YES;
        [self addSubview:_todaySymbol];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _textLabel.frame = self.bounds;
    
    _highlightBackground.size = CGSizeMake(self.height*1.2, self.height);
    [_highlightBackground moveToCenterOfSuperview];
    
    _todaySymbol.center = CGPointMake(self.width/2.0,
                                   self.height/2.0 + _textLabel.font.pointSize/2.0 + 4.0);
}

- (void)setState:(CVDayCellState)state {
    _state = state;
    
    _highlightBackground.hidden = YES;
    _textLabel.hidden = NO;
    
    if (_state == CVDayCellStateRegular) {
        _textLabel.textColor = _textColor;
    }
    else if (_state == CVDayCellStateFadeout) {
        _textLabel.textColor = _fadeoutTextColor;
    }
    else if (_state == CVDayCellStateSelected) {
        _textLabel.textColor = _highlightedTextColor;
        _highlightBackground.hidden = NO;
    }
    else if (_state == CVDayCellStateInvisible) {
        _textLabel.hidden = YES;
    }
}

- (void)setHighlightedTextColor:(UIColor *)highlightedTextColor {
    _highlightedTextColor = highlightedTextColor;
    _textLabel.textColor = highlightedTextColor;
}

- (void)setHighlightedBackgroundColor:(UIColor *)highlightedBackgroundColor {
    _highlightedBackgroundColor = highlightedBackgroundColor;
    _highlightBackground.backgroundColor = highlightedBackgroundColor;
}

@end
