//
//  UIView+Mixed.h
//  FECommon
//
//  Created by Tracy on 16/1/7.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Util)

- (CGRect)convertBoundsToView:(UIView*)view;

@end
