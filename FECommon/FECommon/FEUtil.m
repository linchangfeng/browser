//
//  FECommon.m
//  onePass
//
//  Created by Darlene on 13-9-12.
//  Copyright (c) 2013年 Elite. All rights reserved.
//

#import "FEUtil.h"
#import <CoreData/CoreData.h>
#import "UIView+Position.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import <AudioToolbox/AudioToolbox.h>

#import <sys/utsname.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#import <ifaddrs.h>
#import <net/if.h>

@implementation FEUtil

#pragma mark - Init
- (void)sharedInstanceInitializer {
    _documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    _libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
    _tmpPath = [NSHomeDirectory() stringByAppendingPathComponent:@"tmp"];
    
    _defaultUserAgent = [self generateDefaultUserAgent];
    _UUID = [[UIDevice currentDevice].identifierForVendor UUIDString];
    _BOM = @"\xEF\xBB\xBF";
    
    //键盘
    FEAddObserver(self, UIKeyboardWillChangeFrameNotification, @selector(keyboardWillChangeFrame:));
}

#pragma mark - Notification
- (void)keyboardWillChangeFrame:(NSNotification*)notification {
    NSDictionary *info = [self parseKeyboardInfo:notification];
    BOOL keyboardWillShow = [info[@"shown"] boolValue];
    if (keyboardWillShow) {
        _keyboardWidth = [info[@"width"] floatValue];
        _keyboardHeight = [info[@"height"] floatValue];
    }
    else {
        _keyboardWidth = 0.0;
        _keyboardHeight = 0.0;
    }
}

#pragma mark - Private
- (NSString*)generateDefaultUserAgent {
    return FEStringWithFormat(@"%@/%@ (%@; iOS %@; Scale/%0.2f)", [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleExecutableKey] ?: [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleIdentifierKey], (__bridge id)CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), kCFBundleVersionKey) ?: [[[NSBundle mainBundle] infoDictionary] objectForKey:(__bridge NSString *)kCFBundleVersionKey], [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemVersion], ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] ? [[UIScreen mainScreen] scale] : 1.0f));
}

- (NSDictionary*)parseKeyboardInfo:(NSNotification*)notification {
    CGRect rect = [[notification.userInfo valueForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    
    BOOL shown = NO;
    CGRect convertedRect = CGRectZero;
    if ([self iosVersion] >= 8.0) {
        convertedRect = rect;
        shown = CGRectIntersectsRect(window.bounds, rect);
    }
    else {
        UIViewController *top = window.rootViewController;
        while (top.presentedViewController) {
            top = top.presentedViewController;
        }
        convertedRect = [window convertRect:rect toView:top.view];
        shown = CGRectIntersectsRect(top.view.bounds, convertedRect);
    }

    return @{
             @"width": @(convertedRect.size.width),
             @"height": @(convertedRect.size.height),
             @"shown": @(shown)
             };
}

@end



@implementation FEUtil (String)

- (NSString*)join:(NSString *)path, ... {
    NSString *r = [path copy];
    
    va_list valist;
    if (path) {
        va_start(valist, path);
        id arg;
        while ((arg = va_arg(valist, id))) {
            r = [r stringByAppendingPathComponent:arg];
        }
        va_end(valist);
    }
    return r;
}

- (NSString*)pathForResource:(NSString *)filename {
    NSString *pure = [filename stringByDeletingPathExtension];
    NSString *type = [filename pathExtension];
    return [[NSBundle mainBundle] pathForResource:pure ofType:type];
}

@end



@implementation FEUtil (UI)

- (UIImage *)viewToImage:(UIView*)aView scale:(CGFloat)scale {
    UIGraphicsBeginImageContextWithOptions(aView.frame.size, NO, scale);
    if (self.iosVersion >= 7.0 && aView.window) {
        [aView drawViewHierarchyInRect:aView.bounds afterScreenUpdates:YES];
    }
    else {
        [aView.layer renderInContext:UIGraphicsGetCurrentContext()];
    }
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshot;
}

- (UIBarButtonItem*)createBarButtonItemWithTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents image:(UIImage*)image {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    [btn setImage:image forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:controlEvents];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return item;
}

- (UIImage*)tintImage:(UIImage*)image withColor:(UIColor*)color {
    if (image && color) {
        UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0);
        CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
        [image drawInRect:rect];
        
        [color set];
        UIRectFillUsingBlendMode(rect, kCGBlendModeSourceAtop);
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return image;
}

- (UIImage*)scaleImage:(UIImage*)image maxSide:(CGFloat)maxSide {
    if (image.size.width == 0 || image.size.height == 0) {
        return image;
    }
    
    float longger = MAX(image.size.width, image.size.height);
    UIImage *result = image;
    if (longger > maxSide) {
        
        float newHeight = 0;
        float newWidth = 0;
        
        if (image.size.height > image.size.width) {
            newHeight = maxSide;
            newWidth = image.size.width * newHeight / image.size.height;
        }
        else {
            newWidth = maxSide;
            newHeight = image.size.height * newWidth / image.size.width;
        }
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        
        CGRect drawRect = CGRectMake(0, 0, newWidth, newHeight);
        [image drawInRect:drawRect];
        
        result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return result;
}

- (UIImage*)scaleImage:(UIImage*)image targetSize:(CGSize)targetSize {
    if (image.size.width == 0 || image.size.height == 0) {
        return image;
    }
    
    UIImage *result = image;
    
    CGFloat imageWidthToHeight = image.size.width / image.size.height;
    CGFloat targetWidthToHeight = targetSize.width / targetSize.height;
    
    UIGraphicsBeginImageContext(targetSize);
    
    if (imageWidthToHeight > targetWidthToHeight) {
        CGFloat drawHeight = targetSize.height;
        CGFloat drawWidth = (image.size.width / image.size.height) * drawHeight;
        CGFloat drawX = (targetSize.width - drawWidth) / 2;
        
        CGRect drawRect = CGRectMake(drawX, 0, drawWidth, drawHeight);
        [image drawInRect:drawRect];
    }
    else {
        CGFloat drawWidth = targetSize.width;
        CGFloat drawHeight = drawWidth / (image.size.width / image.size.height);
        CGFloat drawY = (targetSize.height - drawHeight) / 2;
        
        CGRect drawRect = CGRectMake(0, drawY, drawWidth, drawHeight);
        [image drawInRect:drawRect];
    }
    
    result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

- (void)refreshUI {
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate date]];
}

- (UIImage*)cropImage:(UIImage*)image inRect:(CGRect)rect {
    CGImageRef cgImage = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *r = [UIImage imageWithCGImage:cgImage scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(cgImage);
    return r;
}

- (id)findSuperViewWithClass:(Class)aClass subview:(id)subview {
    id v = subview;
    while (YES) {
        v = [v superview];
        if ([v isKindOfClass:aClass] || v == nil) {
            return v;
        }
    }
    
    return nil;
}

- (CGFloat)metaPoint {
    return 1.0 / [self screenScale];
}

- (UIBarButtonItem*)createBackButtonItemWithTitle:(NSString*)title target:(id)target action:(SEL)action {
    // 生成图标
    float targetImageHeight = 26;
    
    UIView *v = [[UIView alloc] init];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [v addSubview:imageView];
    UIImage *icon = [UIImage imageNamed:@"Back"];
    imageView.image = icon;
    imageView.frame = CGRectMake(0, 0, icon.size.width, icon.size.height);
    imageView.centerY = targetImageHeight / 2.0;
    
    UILabel *label = [[UILabel alloc] init];
    [v addSubview:label];
    label.text = title;
    label.font = [UIFont systemFontOfSize:17];
    [label sizeToFit];
    label.x = imageView.right - 5;
    label.y = (targetImageHeight - label.height) / 2.0;
    
    v.frame = CGRectMake(0, 0, label.right, targetImageHeight);
    UIImage *image = [kUtil viewToImage:v scale:0];
    
    // 创建BarButtonItem
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:target action:action];
    backButtonItem.imageInsets = UIEdgeInsetsMake(0, -10, 0, 10);
    return backButtonItem;
}

@end


@implementation FEUtil (Debugging)

- (void)printSubviewsInView:(UIView*)view {
    [self printSubviewsInView:view index:0];
}

- (void)printCGRect:(CGRect)rect {
    NSLog(@"%@", NSStringFromCGRect(rect));
}

- (void)printCGPoint:(CGPoint)point {
    NSLog(@"%@", NSStringFromCGPoint(point));
}

- (void)printCGSize:(CGSize)size {
    NSLog(@"%@", NSStringFromCGSize(size));
}

#pragma mark - Private
- (void)printSubviewsInView:(UIView*)view index:(int)index {
    NSMutableString *prefix = [NSMutableString string];
    [prefix appendFormat:@"%d", index];
    for (int i=0; i<index; i++) {
        [prefix appendString:@"--"];
    }
    
    for (UIView *subview in view.subviews) {
        subview.backgroundColor = [UIColor redColor];
        [self printSubviewsInView:subview index:index+1];
    }
}

@end


@implementation FEUtil (Device)

- (BOOL)isPortrait {
    UIInterfaceOrientation o = [UIApplication sharedApplication].statusBarOrientation;
    return UIInterfaceOrientationIsPortrait(o);
}

- (BOOL)isRetina {
    return [self screenScale] > 1.0;
}

- (float)screenScale {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        return [[UIScreen mainScreen] scale];
    }
    
    return 1.0;
}

- (CGSize)screenPoint {
    CGSize size = [UIScreen mainScreen].bounds.size;
    float width = MIN(size.width, size.height);
    float height = MAX(size.width, size.height);
    return CGSizeMake(width, height);
}

- (FEScreenInch)screenInch {
    CGSize size = [self screenPoint];
    if (size.height == FEScreenPoint3_5.height) {
        return FEScreenInch3_5;
    }
    else if (size.height == FEScreenPoint4_0.height) {
        return FEScreenInch4_0;
    }
    else if (size.height == FEScreenPoint4_7.height) {
        return FEScreenInch4_7;
    }
    else if (size.height == FEScreenPoint5_5.height) {
        return FEScreenInch5_5;
    }
    else if (size.height == FEScreenPoint7_9.height) {
        return FEScreenInch7_9;
    }
    else if (size.height == FEScreenPoint9_7.height) {
        return FEScreenInch9_7;
    }
    
    return FEScreenInchUnknown;
}

- (NSString*)languageCode {
    NSLocale *currentLocale = [NSLocale currentLocale];
    return [currentLocale objectForKey:NSLocaleLanguageCode];
}

- (NSString*)countryCode {
    NSLocale *currentLocale = [NSLocale currentLocale];
    return [currentLocale objectForKey:NSLocaleCountryCode];
}

- (NSString*)machine {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

- (float)iosVersion {
    return [[UIDevice currentDevice].systemVersion floatValue];
}

- (BOOL)isWiFiEnabled {
    NSCountedSet *set = [[NSCountedSet alloc] init];
    
    struct ifaddrs *interface;
    getifaddrs(&interface);
    while (interface) {
        BOOL up = (interface->ifa_flags & IFF_UP) == IFF_UP;
        if (up) {
            NSString *name = [[NSString alloc] initWithUTF8String:interface->ifa_name];
            [set addObject:name];
        }
        interface = interface->ifa_next;
    }
    return ([set countForObject:@"awdl0"] > 1);
}

- (NSString*)currentNetworkName {
    NSString *name = nil;
    NSArray *interfaces = (__bridge NSArray*)CNCopySupportedInterfaces();
    for (NSString* interface in interfaces) {
        CFDictionaryRef networkDetails = CNCopyCurrentNetworkInfo((__bridge CFStringRef)interface);
        if (networkDetails) {
            name = (NSString *)CFDictionaryGetValue(networkDetails, kCNNetworkInfoKeySSID);
            CFRelease(networkDetails);
        }
    }
    
    return name;
}

- (NSString*)localIPAddress {
#if TARGET_OS_SIMULATOR
    return @"localhost";
#endif
    
    NSString *address = NULL;
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    
    if (!address) {
        char baseHostName[255];
        gethostname(baseHostName, 255);
        
        // Adjust for iPhone -- add .local to the host name
        char hn[255];
        sprintf(hn, "%s.local", baseHostName);
        
        struct hostent *host = gethostbyname(hn);
        if (host == NULL)
        {
            address = NULL;
        }
        else {
            struct in_addr **list = (struct in_addr **)host->h_addr_list;
            address = [NSString stringWithCString:inet_ntoa(*list[0]) encoding:NSUTF8StringEncoding];
        }
    }
    
    return address;
}

- (BOOL)isTouchIDAvailable {
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
    if (error) {
        if (error.code == LAErrorTouchIDNotAvailable) {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)isTouchIDEnrolled {
    LAContext *context = [[LAContext alloc] init];
    return [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil];
}

- (void)vibrate {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

@end


@implementation FEUtil (Date)

- (BOOL)isDate:(NSDate*)date1 inSameDayAsDate:(NSDate *)date2 {
    if (date1 == nil || date2 == nil) {
        return date1 == date2;
    }
    
    NSDateComponents *components1 = [[NSCalendar currentCalendar] components:kCFCalendarUnitYear|kCFCalendarUnitMonth|kCFCalendarUnitDay fromDate:date1];
    NSDateComponents *components2 = [[NSCalendar currentCalendar] components:kCFCalendarUnitYear|kCFCalendarUnitMonth|kCFCalendarUnitDay fromDate:date2];
    
    return (components1.year == components2.year &&
            components1.month == components2.month &&
            components1.day == components2.day);
}

- (NSDateComponents*)componentsOfDate:(NSDate *)date {
    NSCalendarUnit calendarUnit = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;
    return [kCalendar components:calendarUnit fromDate:date];
}

- (NSInteger)daysOfMonthWithDateComponents:(NSDateComponents*)dateComponents {
    NSDate *date = [kCalendar dateFromComponents:dateComponents];
    return [kCalendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date].length;
}

- (NSDate*)firstDateOfMonthWithDateComponents:(NSDateComponents *)dateComponents {
    NSDateComponents *begin = [[NSDateComponents alloc] init];
    begin.year = dateComponents.year;
    begin.month = dateComponents.month;
    begin.day = 1;
    return [kCalendar dateFromComponents:begin];
}

- (NSDate*)firstDateOfYear:(NSInteger)year {
    NSDateComponents *firstDateComponents = [[NSDateComponents alloc] init];
    firstDateComponents.year = year;
    firstDateComponents.month = 1;
    firstDateComponents.day = 1;
    return [kCalendar dateFromComponents:firstDateComponents];
}

- (NSInteger)thisYear {
    return [kCalendar component:NSCalendarUnitYear fromDate:[NSDate new]];
}

- (BOOL)isLaterDate:(NSDate*)date {
    NSDate *now = [NSDate new];
    return [now compare:date] == NSOrderedAscending;
}

- (NSInteger)weekdayOfDate:(NSDate*)date {
    return [kCalendar component:NSCalendarUnitWeekday fromDate:date];
}

- (NSString*)stringFromDate:(NSDate*)date withFormat:(NSString*)format {
    static NSDateFormatter *f = nil;
    if (f == nil) {
        f = [[NSDateFormatter alloc] init];
    }
    
    f.dateFormat = format;
    return [f stringFromDate:date];
}

- (NSDate*)dateFromString:(NSString *)stringDate format:(NSString *)format {
    static NSDateFormatter *f = nil;
    if (f == nil) {
        f = [[NSDateFormatter alloc] init];
    }
    
    f.dateFormat = format;
    return [f dateFromString:stringDate];
}

@end


@implementation FEUtil (Others)

- (NSString*)appVersion {
    return [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
}

- (NSString*)appBundleName {
    return [NSBundle mainBundle].infoDictionary[@"CFBundleName"];
}

- (NSString*)appDisplayName {
    NSString *name = [NSBundle mainBundle].infoDictionary[@"CFBundleDisplayName"];
    if (name == nil) {
        name = [self appBundleName];
    }
    return name;
}

- (NSString*)localizedAppName {
    NSString *name = [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:@"CFBundleDisplayName"];
    if (name == nil) {
        name = [self appBundleName];
    }
    return name;
}

- (void)openAppInAppStoreWithAppID:(NSString*)appID {
	NSString *appStoreURL = FEStringWithFormat(@"itms-apps://itunes.apple.com/us/app/id%@", appID);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreURL]];
}

- (void)repeatBlock:(void (^)(BOOL* stop))aBlock interval:(NSTimeInterval)interval {
    BOOL stop = NO;
    aBlock(&stop);
    if (stop == NO) {
        setTimeout(interval, ^{
            [self repeatBlock:aBlock interval:interval];
        });
    }
}

- (void)performBlockInMainThread:(void (^)(void))aBlock {
    if ([NSThread isMainThread]) {
        aBlock();
    }
    else {
        dispatch_sync(dispatch_get_main_queue(), aBlock);
    }
}

- (BOOL)isValidEmailAddress:(NSString *)address {
    return [address rangeOfString:@"^\\S+@\\S+\\.\\S+$" options:NSRegularExpressionSearch].location != NSNotFound;
}

- (NSString *)formatDuration:(NSInteger)duration {
    if (duration == NSNotFound || duration < 0) {
        duration = 0;
    }
    
    NSInteger second = duration % 60;
    NSInteger minute = duration / 60 % 60;
    NSInteger hour = duration / 60 / 60 % 60;

    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hour, minute, second];
}

- (NSString*)generateRandomID {
    static NSMutableArray *charList = nil;
    if (charList == nil) {
        charList = [[NSMutableArray alloc] init];
        
        for (int i=48; i<=57; i++) { //ASCII 0-9
            [charList addObject:[NSString stringWithFormat:@"%c", i]];
        }
        for (int i=97; i<=122; i++) { //ASCII a-z
            [charList addObject:[NSString stringWithFormat:@"%c", i]];
        }
    }
    
    NSMutableString *r = [NSMutableString string];
    for (int i=0; i<=20; i++) {
        int index = (arc4random() % charList.count);
        [r appendString:charList[index]];
    }
    
    return r;
}

@end


