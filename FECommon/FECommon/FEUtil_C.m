//
//  FEUtilC.m
//  Accounts
//
//  Created by Tracy on 15/9/15.
//  Copyright (c) 2015年 Tracy. All rights reserved.
//

#import "FEUtil_C.h"
#import "FEUtil.h"
#import <objc/runtime.h>

void FEAddObserver(NSObject *observer, NSString *name, SEL action) {
    [[NSNotificationCenter defaultCenter] addObserver:observer selector:action name:name object:nil];
}

void FERemoveObserver(NSObject *observer) {
    [[NSNotificationCenter defaultCenter] removeObserver:observer];
}

void FERemoveObserver2(NSObject *observer, NSString *name) {
    [[NSNotificationCenter defaultCenter] removeObserver:observer name:name object:nil];
}

void FEPostNotification(NSString *name, NSObject *object, NSDictionary *userInfo) {
    [[NSNotificationCenter defaultCenter] postNotificationName:name object:object userInfo:userInfo];
}

NSData* FEArchiveObject(id obj) {
    NSData *data = nil;
    @try {
        data = [NSKeyedArchiver archivedDataWithRootObject:obj];
    }
    @catch (NSException *exception) {
        data = nil;
    }
    
    return data;
}

id FEUnarchiveData(NSData* data) {
    id r = nil;
    @try {
        r = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    @catch (NSException *exception) {
        r = nil;
    }
    
    return r;
}

CGFloat FENavBarHeight(void) {
    if (FEIsPhone && [kUtil screenInch] < FEScreenInch5_5) {
        return [[FEUtil sharedInstance] isPortrait] ? 44 : 32;
    }
    else {
        return 44;
    }
}

CGFloat FEToolbarHeight(void) {
    return FENavBarHeight();
}

CGFloat CGRectGetX(CGRect rect) {
    return rect.origin.x;
}

CGFloat CGRectGetY(CGRect rect) {
    return rect.origin.y;
}

CGFloat CGRectGetRight(CGRect rect) {
    return rect.origin.x + rect.size.width;
}

CGFloat CGRectGetBottom(CGRect rect) {
    return rect.origin.y + rect.size.height;
}

// Transform3D
CATransform3D FECATransform3DMakePerspective(CGPoint center, float disZ) {
    CATransform3D transToCenter = CATransform3DMakeTranslation(-center.x, -center.y, 0);
    CATransform3D transBack = CATransform3DMakeTranslation(center.x, center.y, 0);
    CATransform3D scale = CATransform3DIdentity;
    scale.m34 = -1.0f/disZ;
    return CATransform3DConcat(CATransform3DConcat(transToCenter, scale), transBack);
}

CATransform3D FECATransform3DPerspect(CATransform3D t, CGPoint center, float disZ) {
    return CATransform3DConcat(t, FECATransform3DMakePerspective(center, disZ));
}

//
BOOL FEIsNumber(id obj) {
    return [obj isKindOfClass:[NSNumber class]];
}

BOOL FEIsArray(id obj) {
    return [obj isKindOfClass:[NSArray class]];
}

BOOL FEIsDictionary(id obj) {
    return [obj isKindOfClass:[NSDictionary class]];
}

//
void exchangeMethodImplementations(Class cls, SEL sel1, SEL sel2) {
    Method m1 = class_getInstanceMethod(cls, sel1);
    Method m2 = class_getInstanceMethod(cls, sel2);
    method_exchangeImplementations(m1, m2);
}

void setTimeout(NSTimeInterval delay, void (^aBlock)(void)) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), aBlock);
}