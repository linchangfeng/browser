//
//  FEInternal.h
//  FECommon
//
//  Created by Tracy on 5/23/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import <Foundation/Foundation.h>

#define _(key) [FEInternal localizedString:key]

@interface FEInternal : NSObject

+ (NSString*)localizedString:(NSString*)key;

@end
