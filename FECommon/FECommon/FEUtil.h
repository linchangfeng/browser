//
//  FECommon.h
//  onePass
//
//  Created by Darlene on 13-9-12.
//  Copyright (c) 2013年 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FESingleton.h"
#import "FEUtil_Macro.h"
#import "FEUtil_C.h"
#import "UIView+Position.h"

typedef enum {
    FEScreenInch3_5,
    FEScreenInch4_0,
    FEScreenInch4_7,
    FEScreenInch5_5,
    FEScreenInch7_9,
    FEScreenInch9_7,
    FEScreenInchUnknown
} FEScreenInch;

@interface FEUtil : FESingleton

@property (nonatomic, readonly) NSString *documentsPath;
@property (nonatomic, readonly) NSString *tmpPath;
@property (nonatomic, readonly) NSString *libraryPath;
@property (nonatomic, readonly) NSString *defaultUserAgent;
@property (nonatomic, readonly) NSString *UUID;
@property (nonatomic) float keyboardWidth;
@property (nonatomic) float keyboardHeight;
@property (nonatomic, readonly) NSString *BOM;

@end

@interface FEUtil (String)
- (NSString*)join:(NSString*)path, ...NS_REQUIRES_NIL_TERMINATION;
- (NSString*)pathForResource:(NSString *)filename;
@end

@interface FEUtil (UI)
- (UIImage *)viewToImage:(UIView*)aView scale:(CGFloat)scale;
- (UIBarButtonItem*)createBarButtonItemWithTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents image:(UIImage*)image;
- (UIImage*)tintImage:(UIImage*)image withColor:(UIColor*)color;
- (UIImage*)cropImage:(UIImage*)image inRect:(CGRect)rect;
- (UIImage*)scaleImage:(UIImage*)image maxSide:(CGFloat)maxLenght;
- (UIImage*)scaleImage:(UIImage*)image targetSize:(CGSize)targetSize;
- (id)findSuperViewWithClass:(Class)aClass subview:(id)subview;
- (CGFloat)metaPoint;
- (void)refreshUI;
- (UIBarButtonItem*)createBackButtonItemWithTitle:(NSString*)title target:(id)target action:(SEL)action;

@end

@interface FEUtil (Debugging)

- (void)printSubviewsInView:(UIView*)view;
- (void)printCGRect:(CGRect)rect;
- (void)printCGPoint:(CGPoint)point;
- (void)printCGSize:(CGSize)size;

@end

@interface FEUtil (Device)
- (BOOL)isPortrait;
- (BOOL)isRetina;
- (float)screenScale;
- (CGSize)screenPoint;
- (FEScreenInch)screenInch;
- (float)iosVersion;
- (NSString*)languageCode;
- (NSString*)countryCode;
- (NSString*)machine;
- (NSString*)localIPAddress;
- (NSString*)currentNetworkName;

- (BOOL)isTouchIDAvailable;
- (BOOL)isTouchIDEnrolled;
- (void)vibrate;

@end

@interface FEUtil (Date)
- (BOOL)isDate:(NSDate*)date1 inSameDayAsDate:(NSDate *)date2;
- (NSDateComponents*)componentsOfDate:(NSDate *)date;
- (NSDate*)firstDateOfYear:(NSInteger)year;
- (NSInteger)daysOfMonthWithDateComponents:(NSDateComponents*)dateComponents;
- (NSDate*)firstDateOfMonthWithDateComponents:(NSDateComponents*)dateComponents;
- (NSInteger)thisYear;
- (BOOL)isLaterDate:(NSDate*)date;
- (NSInteger)weekdayOfDate:(NSDate*)date;

- (NSString*)stringFromDate:(NSDate*)date withFormat:(NSString*)format;
- (NSDate*)dateFromString:(NSString *)stringDate format:(NSString *)format;
@end

@interface FEUtil (Others)
- (NSString*)appVersion;
- (NSString*)appDisplayName;
- (NSString*)appBundleName;
- (NSString*)localizedAppName;
- (void)openAppInAppStoreWithAppID:(NSString*)appID;
- (void)repeatBlock:(void (^)(BOOL* stop))aBlock interval:(NSTimeInterval)interval;
- (void)performBlockInMainThread:(void (^)(void))aBlock;
- (BOOL)isValidEmailAddress:(NSString*)address;
- (NSString *)formatDuration:(NSInteger)duration;
- (NSString *)generateRandomID;
@end
