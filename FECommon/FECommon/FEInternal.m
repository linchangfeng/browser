//
//  FEInternal.m
//  FECommon
//
//  Created by Tracy on 5/23/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

@implementation FEInternal

+ (NSString*)localizedString:(NSString*)key {
    static NSBundle *bundle = nil;
    if (bundle == nil) {
        bundle = [NSBundle bundleForClass:[self class]];
    }
    return NSLocalizedStringWithDefaultValue(key, @"FELocalizations", bundle, @"", @"");
}

@end