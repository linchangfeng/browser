//
//  FEAlertController.m
//  OnePass
//
//  Created by Tracy on 14/9/25.
//  Copyright (c) 2014年 Elite. All rights reserved.
//

#import "FEAlertController.h"

@interface FEAlertController () {
    FEAlertController *_me;
    FEAlertControllerHandler _handler;
    UIAlertControllerStyle _style;
}

@end

@implementation FEAlertController

- (id)initWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray*)buttonTitles cancelButtonTitle:(NSString*)cancelButtonTitle destructiveButtonTitle:(NSString*)destructiveButtonTitle preferredStyle:(UIAlertControllerStyle)preferredStyle handler:(void (^)(FEAlertController *c, NSString* buttonTitle))handler {
    self = [super init];
    if (self) {
        _handler = handler;
        _style = preferredStyle;
        
        if ([kUtil iosVersion] >= 8.0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:preferredStyle];
            
            if (preferredStyle == UIAlertControllerStyleActionSheet && destructiveButtonTitle) {
                UIAlertAction *action = [UIAlertAction actionWithTitle:destructiveButtonTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                    [self didClickButtonWithTitle:action.title];
                }];
                [alert addAction:action];
            }
            
            if (cancelButtonTitle) {
                UIAlertAction *action = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    [self didClickButtonWithTitle:action.title];
                }];
                [alert addAction:action];
            }
            
            for (NSString *title in buttonTitles) {
                UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self didClickButtonWithTitle:action.title];
                }];
                [alert addAction:action];
            }
            
            _internal = alert;
        }
        
        else {
            if (preferredStyle == UIAlertControllerStyleActionSheet) {
                UIActionSheet *sheet = [[UIActionSheet alloc] init];
                sheet.title = title;
                sheet.delegate = (id)self;
                
                int theLastButtonIndex = - 1;
                
                if (destructiveButtonTitle) {
                    [sheet addButtonWithTitle:destructiveButtonTitle];
                    theLastButtonIndex++;
                    [sheet setDestructiveButtonIndex:theLastButtonIndex];
                }
                
                for (NSString *title in buttonTitles) {
                    [sheet addButtonWithTitle:title];
                    theLastButtonIndex++;
                }
                
                if (cancelButtonTitle) {
                    [sheet addButtonWithTitle:cancelButtonTitle];
                    theLastButtonIndex++;
                    [sheet setCancelButtonIndex:theLastButtonIndex];
                }
                
                _internal = sheet;
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = title;
                alert.message = message;
                alert.delegate = (id)self;
                
                int theLastButtonIndex = - 1;
                
                if (cancelButtonTitle) {
                    [alert addButtonWithTitle:cancelButtonTitle];
                    theLastButtonIndex++;
                    [alert setCancelButtonIndex:theLastButtonIndex];
                }
                
                for (NSString *title in buttonTitles) {
                    [alert addButtonWithTitle:title];
                    theLastButtonIndex++;
                }
                
                _internal = alert;
            }
        }
    }
    
    return self;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = nil;
    @try {
        title = [actionSheet buttonTitleAtIndex:buttonIndex];
    }
    @catch (NSException *exception) {
        //
    }
    
    actionSheet.delegate = nil;
    [self didClickButtonWithTitle:title];
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = nil;
    @try {
        title = [alertView buttonTitleAtIndex:buttonIndex];
    }
    @catch (NSException *exception) {
        //
    }
    
    alertView.delegate = nil;
    [self didClickButtonWithTitle:title];
}

#pragma mark - Private
- (void)didClickButtonWithTitle:(NSString*)title {
    if (_handler) {
        _handler(self, title);
    }
    [self clean];
}

- (void)clean {
    _internal = nil;
    _me = nil;
}

#pragma mark - Public
- (void)showInController:(UIViewController *)controller animated:(BOOL)animated completion:(void (^)(void))completion {
    _me = self;
    
    if ([kUtil iosVersion] >= 8.0) {
        if (FEIsPhone == NO && _style == UIAlertControllerStyleActionSheet) {
            UIAlertController *sheet = _internal;
            sheet.popoverPresentationController.sourceView = controller.view;
            sheet.popoverPresentationController.sourceRect = _sourceRect;
        }
        
        [controller presentViewController:_internal animated:animated completion:completion];
    }
    else {
        if (_style == UIAlertControllerStyleActionSheet) {
            UIActionSheet *sheet = _internal;
            [sheet showFromRect:_sourceRect inView:controller.view animated:animated];
        }
        else {
            UIAlertView *alert = _internal;
            [alert show];
        }
    }
}


- (void)dismiss {
    if ([kUtil iosVersion] >= 8.0) {
        UIAlertController *c = _internal;
        [c dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        if (_style == UIAlertControllerStyleActionSheet) {
            UIActionSheet *sheet = _internal;
            [sheet dismissWithClickedButtonIndex:NSNotFound animated:YES];
        }
        else {
            UIAlertView *alert = _internal;
            [alert dismissWithClickedButtonIndex:NSNotFound animated:YES];
        }
    }
    
    [self clean];
}

- (void)simulateClickButtonWithTitle:(NSString*)buttonTitle {
    if (_handler) {
        _handler(self, buttonTitle);
    }
    
    [self dismiss];
}

- (UITextField*)textFieldAtIndex:(NSUInteger)index {
    UITextField *r = nil;
    if (_style == UIAlertControllerStyleAlert) {
        if ([kUtil iosVersion] >= 8.0) {
            UIAlertController *c = _internal;
            r = c.textFields[index];
        }
        else {
            UIAlertView *v = _internal;
            r = [v textFieldAtIndex:index];
        }
    }
    
    return r;
}

@end
