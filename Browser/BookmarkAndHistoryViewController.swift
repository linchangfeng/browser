//
//  BookmarkAndHistoryViewController.swift
//  Browser
//
//  Created by lin on 18/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class BookmarkAndHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var selectedCallback: ((_ url: String?) -> Void)?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segment: UISegmentedControl!
    
    private var bookmarks: [Bookmark]?
    private var histories: [History]?
    
    private var isBookmark = true {
        didSet {
            
            self.navigationItem.rightBarButtonItem = self.isBookmark ? self.editItem : self.clearItem
            
            self.tableView.reloadData()
        }
    }
    
    private var isTableViewEditing: Bool = false {
        didSet {
            self.editItem.title = (self.isTableViewEditing ? __("完成") : __("编辑"))
        }
    }
    
    private var editItem: UIBarButtonItem!
    private var clearItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.segment.setTitle(__("书签"), forSegmentAt: 0)
        self.segment.setTitle(__("历史"), forSegmentAt: 1)
        
        self.editItem = UIBarButtonItem(title: __("编辑"), style: .plain, target: self, action: #selector(editItemAction))
        self.clearItem = UIBarButtonItem(title: __("清空"), style: .plain, target: self, action: #selector(clearHistoriesAction))
        
        self.navigationItem.rightBarButtonItem = self.editItem
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bookmarks = BookmarkModel.default.allBookmarks()
        self.histories = HistoryModel.default.allHistories()
        
        self.tableView.reloadData()
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - TableView delegate & data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isBookmark {
            if let _ = self.bookmarks, self.bookmarks!.count > 0 {
                self.editItem.isEnabled = true
            } else {
                self.editItem.isEnabled = false
            }
            return self.bookmarks?.count ?? 0
        } else {
            if let _ = self.histories, self.histories!.count > 0 {
                self.clearItem.isEnabled = true
            } else {
                self.clearItem.isEnabled = false
            }
            return self.histories?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WebsiteInfoCell", for: indexPath) as! WebsiteInfoTableViewCell
        
        
        let content = self.infoContent(info: self.isBookmark ? self.bookmarks![indexPath.row] : self.histories![indexPath.row])
        
        cell.titleLabel.text = content.title
        cell.urlLabel.text = content.url
        
        if let data = content.icon {
            let image = UIImage(data: data as Data)
            if image == nil {
                cell.iconImageView.image = UIImage(named: "Browser")
            } else {
                cell.iconImageView.image = image
            }
        } else if let iconLink = content.iconLink, iconLink != "" {
            cell.iconImageView.sd_setImage(with: URL(string: iconLink), completed: { (image, error, _, url) in
                if image == nil {
                    cell.iconImageView.image = UIImage(named: "Browser")
                }
            })
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let block = self.selectedCallback {
            if self.isBookmark {
                block(self.bookmarks![indexPath.row].url)
            } else {
                block(self.histories![indexPath.row].url)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .normal, title: __("编辑")) { (action, indexPath) in
            if let _ = self.bookmarks, self.bookmarks!.count > 0 {
                let bookAdditionControllerView = self.storyboard?.instantiateViewController(withIdentifier: "BookmarkAdditionTableViewController") as! BookmarkAdditionTableViewController
                
                bookAdditionControllerView.existBookmark = self.bookmarks![indexPath.row]
                bookAdditionControllerView.isNew = false
                
                self.navigationController?.pushViewController(bookAdditionControllerView, animated: true)
            }
        }
        
        let addBookmarkAction = UITableViewRowAction(style: .normal, title: __("添加书签")) { (action, indexPath) in
            let history = self.histories![indexPath.row]
            
            BookmarkModel.default.createNewBookmark(title: history.title!, url: history.url!, iconLink: history.iconLink!, iconData: history.icon!)
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.animationType = .zoomOut
            hud.mode = .text
            hud.label.text = __("完成")
            hud.hide(animated: true, afterDelay: 1)
            
            tableView.isEditing = false
            
            self.bookmarks = BookmarkModel.default.allBookmarks()
        }
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: __("删除")) { (action, indexPath) in
            if self.isBookmark {
                BookmarkModel.default.deleteBookmark(self.bookmarks![indexPath.row])
                self.bookmarks!.remove(at: indexPath.row)
            } else {
                HistoryModel.default.deleteHisotry(self.histories![indexPath.row])
                self.histories?.remove(at: indexPath.row)
            }
            
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        if self.isBookmark {
            return [deleteAction, editAction]
        } else {
            return [deleteAction, addBookmarkAction]
        }
    }
    
    // Action
    
    @IBAction func segmentedValueChanged(_ sender: UISegmentedControl) {
        print(#function, sender.selectedSegmentIndex)
        
        self.isBookmark = sender.selectedSegmentIndex == 0 ? true : false
        self.tableView.setEditing(self.isTableViewEditing && self.segment.selectedSegmentIndex == 0, animated: true)
    }
    
    func editItemAction() {
        self.isTableViewEditing = !self.isTableViewEditing
        self.tableView.setEditing(self.isTableViewEditing && self.segment.selectedSegmentIndex == 0, animated: true)
        
    }
    
    func clearHistoriesAction() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: __("清空历史记录"), style: .destructive) { (_) in
            HistoryModel.default.deleteAllHisotries()
            self.histories?.removeAll()
            self.tableView.reloadData()
        }
        
        let cancel = UIAlertAction(title: __("取消"), style: .cancel, handler: nil)
        
        alertController.addAction(delete)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Private
    
    func infoContent(info: Any) -> (title: String?, url: String?, iconLink: String?, icon: NSData?){
        if self.isBookmark {
            let bookmark = info as! Bookmark
            return (bookmark.title, bookmark.url, bookmark.iconLink, bookmark.icon)
        } else {
            let history = info as! History
            return (history.title, history.url, history.iconLink, history.icon)
        }
    }
}
