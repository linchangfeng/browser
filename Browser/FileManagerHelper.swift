//
//  FileManagerHelper.swift
//  Browser
//
//  Created by lin on 04/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class FileManagerHelper: NSObject {
    
    static let share = FileManagerHelper()
    
    var downloadedFiles = [(path: URL, name: String, size: Int64)]()
    
    func fileSysAttributes() -> [FileAttributeKey: Any]? {
        if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            return try? FileManager.default.attributesOfFileSystem(forPath: path)
        }
        
        return nil
    }
    
    func allStorage() -> Double {
        if let attribtues = self.fileSysAttributes() {
            return (attribtues[.systemSize] as! Double) / 1024.0 / 1024.0 / 1024.0
        }
        
        return 0.0
    }
    
    func freeStorage() -> Double {
        if let attribtues = self.fileSysAttributes() {
            return (attribtues[.systemFreeSize] as! Double) / 1024.0 / 1024.0 / 1024.0
        }
        
        return 0.0
    }
    
    func usedStorage() -> Double {
        return self.allStorage() - self.freeStorage()
    }
    
    func usedRatio() -> Float {
        return Float(self.usedStorage() / self.allStorage())
    }
    
    func initFileProperty() {
        if let fileEnumerator = FileManager.default.enumerator(at: kDownloadedURL, includingPropertiesForKeys: [], options: [.skipsSubdirectoryDescendants, .skipsHiddenFiles], errorHandler: nil) {
            
            self.downloadedFiles.removeAll()
            
            for object in fileEnumerator.allObjects {
                
                if let url = object as? NSURL, let urlString = url.path {
                    let path = URL(fileURLWithPath: urlString)
                    let name = path.lastPathComponent.removingPercentEncoding!
                    do {
                        print(#function, path)
                        let attribute = try FileManager.default.attributesOfItem(atPath: path.path)
                        let fileSize = attribute[.size] as! Int64
                        
                        self.downloadedFiles.append((path, name, fileSize))
                    } catch {
                        debugPrint(error)
                    }
                }
                
            }
        }
    }
}
