//
//  WebsiteInfoTableViewCell.swift
//  Browser
//
//  Created by LinChangfeng on 18/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class WebsiteInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
