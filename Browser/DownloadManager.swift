//
//  DownloadManager.swift
//  Browser
//
//  Created by lin on 04/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Alamofire

class DownloadManager: NSObject {
    
    static let share = DownloadManager()
    
    var downloadedDirectory: URL?
    
    var tasks = [(request: DownloadRequest, model: DownloadModel)]()
    
    func initDownloadFinder(rootURL root: URL) {
        
        self.downloadedDirectory = root.appendingPathComponent("downloaded")
        
        if self.directoryExist(directory: root) == false {
            
            do {
                try FileManager.default.createDirectory(at: root, withIntermediateDirectories: true, attributes: nil)
                try FileManager.default.createDirectory(at: self.downloadedDirectory!, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(#function, error)
            }
            
        } else  {
            
            if self.directoryExist(directory: self.downloadedDirectory!) == false {
                do {
                    try FileManager.default.createDirectory(at: self.downloadedDirectory!, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print(#function, error)
                }
            }
        }
    }
    
    fileprivate func directoryExist(directory: URL) -> Bool {
        return FileManager.default.fileExists(atPath: directory.absoluteString)
    }
    
    func addDownloadtasks(withLink link: URL, fileName name: String, expectedContentLength length:  Int64) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = self.downloadedDirectory!.appendingPathComponent(name)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        let model = DownloadModel(fileName: name, fileURL: link, destination: destination)
        model.file = (DownloadUtility.calculateFileSizeInUnit(length), DownloadUtility.calculateUnit(length))
        
        let utility = DispatchQueue.global(qos: .utility)
        
        let request = download(link.absoluteString, to: destination).downloadProgress(queue: utility, closure: { (progress) in

            model.progress = Float(progress.completedUnitCount) / Float(length)
            
            print("Download Progress: \(model.progress)")
            
            self.postUpdateDownloadProgressNotification(model: model)
        })
        .responseData(queue: utility, completionHandler: { (response) in
            switch response.result {
            case .success(let data):
                model.data = data
                model.status = .completed
                
                self.postCompletlyDownloadProgressNotification(model: model)
                
            case .failure:
                model.resumeData = response.resumeData
                model.status = .failed
            }
        })
        
        self.tasks.append((request, model))
    }
    
    func suspendDownload(with request: DownloadRequest) {
        request.suspend()

        if self.tasks.isEmpty == false {
            if let index = self.tasks.index(where: { $0.request.request == request.request }) {
                self.tasks[index].model.status = .paused
            }
        }
    }
    
    func cancelDownload(with request: DownloadRequest) {
        request.cancel()
        
        if self.tasks.isEmpty == false {
            if let index = self.tasks.index(where: { $0.request.request == request.request }) {
                self.postCancelDownloadProgressNotification(model: self.tasks[index].model)
            }
        }
    }
    
    func resumeDownload(with request: DownloadRequest) {
        request.resume()
        
        if self.tasks.isEmpty == false {
            if let index = self.tasks.index(where: { $0.request.request == request.request }) {
                let model = self.tasks[index].model
                self.tasks[index].request = download(model.fileURL.absoluteString, to: model.destination).responseData(completionHandler: { (response) in
                    
                    switch response.result {
                    case .success(let data):
                        model.data = data
                        model.status = .completed
                        
                        if self.tasks.isEmpty == false {
                            if let index = self.tasks.index(where: { $0.model == model } ) {
                                self.tasks.remove(at: index)
                            }
                        }
                        
                    case .failure:
                        model.resumeData = response.resumeData
                        model.status = .failed
                    }
                })
            }
        }
    }
    
    func postUpdateDownloadProgressNotification(model: DownloadModel) {
        DispatchQueue.global(qos: .utility).sync {
            NotificationCenter.default.post(name: kUpdateDownloadProgressNotification, object: model)
        }
    }
    
    func postCompletlyDownloadProgressNotification(model: DownloadModel) {
        DispatchQueue.global(qos: .utility).sync {
            NotificationCenter.default.post(name: kCompletlyDownloadProgressNotification, object: model)
        }
    }
    
    func postCancelDownloadProgressNotification(model: DownloadModel) {
        NotificationCenter.default.post(name: kCancelDownloadProgressNotification, object: model)
    }
}
