//
//  MenuView.swift
//  Browser
//
//  Created by lin on 17/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class MenuView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var collectionView: UICollectionView!
    var buttons = [String: UIButton]()
    var collectionViewInfo = [String: IndexPath]()
    
    private var layout: UICollectionViewFlowLayout!
    private var actions: [Selector]!
    
    private var visualEffectView: UIVisualEffectView!

    init(actions: [Selector]) {
        super.init(frame: .zero)
        
        self.actions = actions
        
        self.layout = UICollectionViewFlowLayout()
        
        self.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        
        let blur = UIBlurEffect(style: .extraLight)
        self.visualEffectView = UIVisualEffectView(effect: blur)
        
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(MenuCollectionViewCell.self, forCellWithReuseIdentifier: "MenuCell")
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.isScrollEnabled = false
        
        self.initButtons()
        
        self.addSubview(self.visualEffectView)
        self.visualEffectView.addSubview(self.collectionView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let isOrientation = self.superview!.width > self.superview!.height
        
        self.visualEffectView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
        self.collectionView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
        
        self.layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        if isOrientation {
            self.layout.itemSize = CGSize(width: CGFloat((Int(self.collectionView.width / 6.0))), height: self.collectionView.height / 2)
        } else {
            self.layout.itemSize = CGSize(width: CGFloat((Int(self.collectionView.width / 4.0))), height: self.collectionView.height / 3)
        }
        
        self.layout.minimumInteritemSpacing = 0
        self.layout.minimumLineSpacing = 0
        self.collectionView.collectionViewLayout = self.layout
        
        for button in self.buttons {
            button.value.frame.size = CGSize(width: self.layout.itemSize.width, height: self.layout.itemSize.height)
        }
    }
    
    // MARK: - CollectionView delegate & data source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.buttons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCollectionViewCell
        
        let button = self.buttons[kMenuIconInfo[indexPath.item].name]!
        let action = self.actions[indexPath.item]

        cell.button.setImage(button.image(for: .normal), for: .normal)
        cell.button.setImage(button.image(for: .highlighted), for: .highlighted)
        cell.button.setImage(button.image(for: .selected), for: .selected)
        cell.button.setTitle(button.title(for: .normal), for: .normal)
        cell.button.setTitle(button.title(for: .highlighted), for: .highlighted)
        cell.button.setTitleColor(button.titleColor(for: .normal), for: .normal)
        cell.button.setTitleColor(button.titleColor(for: .highlighted), for: .highlighted)
        cell.button.setTitleColor(button.titleColor(for: .selected), for: .selected)
        cell.button.titleEdgeInsets = button.titleEdgeInsets
        cell.button.imageEdgeInsets = button.imageEdgeInsets
        cell.button.titleLabel?.font = button.titleLabel?.font
        cell.button.isSelected = button.isSelected
        cell.button.isEnabled = button.isEnabled
        
        cell.button.addTarget(nil, action: action, for: .touchUpInside)
        
        self.collectionViewInfo[kMenuIconInfo[indexPath.item].name] = indexPath
        
        return cell
    }
    
    // Private
    
    func initButtons() {
        
        for i in 0..<kMenuIconInfo.count {
            let button = UIButton()
            
            let tintImage = (FEUtil.sharedInstance() as! FEUtil).tintImage(UIImage(named: kMenuIconInfo[i].icon), with: self.tintColor)
            
            button.setImage(UIImage(named: kMenuIconInfo[i].icon), for: .normal)
            button.setImage(tintImage, for: .highlighted)
            button.setImage(tintImage, for: .selected)
            button.setTitle(kMenuIconInfo[i].title, for: .normal)
            button.setTitle(kMenuIconInfo[i].title, for: .highlighted)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 10.0)
            button.setTitleColor(kMenuButtonTitleColor, for: .normal)
            button.setTitleColor(self.tintColor, for: .highlighted)
            button.setTitleColor(self.tintColor, for: .selected)
            button.addTarget(nil, action: self.actions[i], for: .touchUpInside)
            
            self.setupButton(button: button)
            
            self.buttons[kMenuIconInfo[i].name] = button
        }
        
        self.collectionView.reloadData()
    }
    
    func setupButton(button: UIButton) {
        let spacing: CGFloat = 6.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        let labelString = NSString(string: button.titleLabel!.text!)
        let titleSize = labelString.size(attributes: [NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        button.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
    }

}
