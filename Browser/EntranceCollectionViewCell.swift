//
//  EntranceCollectionViewCell.swift
//  Browser
//
//  Created by lin on 14/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

class EntranceCollectionViewCell: UICollectionViewCell {
    
    var entranceView: UIView? {
        didSet {
            self.entranceView?.addGestureRecognizer(self.panGesture)
            self.contentView.addSubview(self.entranceView!)
            
            self.entranceView?.snp.makeConstraints({ (make) in
                make.top.left.right.bottom.equalToSuperview()
            })
        }
    }
    
    var webPageViewController: WebPageViewController? {
        didSet {
            if self.webPageViewController != nil {
                self.entranceView = self.webPageViewController?.view
                self.webView = self.webPageViewController!.webView
            }
        }
    }
    
    var webView: WKWebView?
    
    var panGesture = UIPanGestureRecognizer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.panGesture.isEnabled = false
        
        if self.entranceView != nil {
            
            self.entranceView!.snp.makeConstraints({ (make) in
                make.top.left.right.equalToSuperview()
                if kParamsManager.bool(forName: kParamNameShowsBanner) {
                    make.height.equalToSuperview().offset(-44)
                } else {
                    make.bottom.equalToSuperview()
                }
            })
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.entranceView != nil {
            self.entranceView!.updateConstraints()
        }
    }
}
