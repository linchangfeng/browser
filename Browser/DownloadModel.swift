//
//  DownloadModel.swift
//  Browser
//
//  Created by lin on 04/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Alamofire

public enum TaskStatus: Int {
    case unknown
    case gettingInfo
    case downloading
    case paused
    case interrtuped
    case failed
    case completed
}

class DownloadModel: NSObject {

    open var fileName: String!
    open var fileURL: URL!
    open var status: TaskStatus = .gettingInfo
    open var destination: DownloadRequest.DownloadFileDestination!
    
    open var file: (size: Float, unit: String)?
    open var downloadFile: (size: Float, unit: String)?
    open var data: Data?
    open var resumeData: Data?
    
    open var remainingTime: (hours: Int, minutes: Int, seconds: Int)?
    
    open var speed: (speed: Float, unit: String)?
    
    open var progress: Float = 0
    
    open var startTime: Date?
    
    convenience init(fileName: String, fileURL: URL, destination: @escaping DownloadRequest.DownloadFileDestination) {
        self.init()
        
        self.fileName = fileName
        self.fileURL = fileURL
        self.destination = destination
    }
}
