//
//  PasscodeAssistant.swift
//  Browser
//
//  Created by Tracy on 10/31/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import Foundation
import CocoaSecurity

class PasscodeAssistant: NSObject, PasscodeViewControllerDelegate {
    let kUDKeyMD5Passcode = "PA.1"
    let kUDKeyTouchIDEnabled = "PA.2"
    
    static let `default` = PasscodeAssistant()
    
    private var passcodeViewController: PasscodeViewController?
    private var overlayView = UIImageView.init(image: UIImage(named: "PasscodeBackground"))
    
    private override init() {
        super.init()
        self.overlayView.contentMode = .scaleAspectFill
    }
    
    func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground(notification:)), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidLaunchOrWillEnterForeground(notification:)), name: Notification.Name.UIApplicationDidFinishLaunching, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidLaunchOrWillEnterForeground(notification:)), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    func savePasscode(text: String?, touchIDEnabled: Bool) {
        var md5: String?
        if (text != nil) {
            md5 = CocoaSecurity.md5(text).hex
        }
        
        UserDefaults.standard.set(md5, forKey: kUDKeyMD5Passcode)
        UserDefaults.standard.set(touchIDEnabled, forKey: kUDKeyTouchIDEnabled)
    }
    
    func hasPasscodeSet() -> Bool {
        return self.MD5passcode() != nil
    }
    
    func cleanPasscode() -> Void {
        self.savePasscode(text: nil, touchIDEnabled: false)
    }
    
    func MD5passcode() -> String? {
        return UserDefaults.standard.string(forKey: kUDKeyMD5Passcode)
    }
    
    func isPasscodeMatched(text: String) -> Bool {
        return CocoaSecurity.md5(text).hex == self.MD5passcode()
    }
    
    func isTouchIDEnrolledAndEnabled() -> Bool {
        return kUtil.isTouchIDEnrolled() && UserDefaults.standard.bool(forKey: kUDKeyTouchIDEnabled)
    }
    
    // MARK: - - Notification
    func appDidEnterBackground(notification: Notification) {
        if self.shouldPresentPasscodeViewController() {
            self.showOverlay()
        }
    }
    
    func appDidLaunchOrWillEnterForeground(notification: Notification) {
        if self.shouldPresentPasscodeViewController() {
            self.showOverlay()
            
            let c = PasscodeViewController(type: .Validation)
            c.delegate = self
            self.topViewController().present(c, animated: false, completion: {
                self.removeOverlay()
            })
            
            self.passcodeViewController = c
            self.passcodeViewController?.showTouchIDValidationIfNeeded()
        }
        else if (self.hasAlreadyPresentedPasscodeViewController()) {
            self.passcodeViewController?.showTouchIDValidationIfNeeded()
        }
    }
    
    // MARK: - - PasscodeViewControllerDelegate
    func validatePasscodeDidSucceed(controller: PasscodeViewController) {
        setTimeout(0.1, {
            self.passcodeViewController?.dismiss(animated: false, completion: nil)
            self.passcodeViewController = nil
        })
    }
    
    func validatePasscodeDidFail(controller: PasscodeViewController) {
        //
    }
    
    // MARK: - - Private
    func topViewController() -> UIViewController {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var c = appDelegate.window?.rootViewController
        while c?.presentedViewController != nil {
            c = c?.presentedViewController
        }
        
        return c!
    }
    
    func shouldPresentPasscodeViewController() -> Bool {
        return self.hasPasscodeSet() && self.hasAlreadyPresentedPasscodeViewController() == false
    }
    
    func hasAlreadyPresentedPasscodeViewController() -> Bool {
        return self.topViewController() == self.passcodeViewController
    }
    
    func showOverlay() {
        if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
            window.addSubview(self.overlayView)
            self.overlayView.frame = window.bounds
        }
    }
    
    func removeOverlay() {
        self.overlayView.removeFromSuperview()
    }
}
