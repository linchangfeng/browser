//
//  DownloadFileTableViewCell.swift
//  Browser
//
//  Created by lin on 04/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class DownloadFileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var downloadedIconImageView: UIImageView!
    @IBOutlet weak var downloadedFileNameLabel: UILabel!
    @IBOutlet weak var downloadedFileSizeLabel: UILabel!
    
    @IBOutlet weak var downloadingIconImageView: UIImageView!
    @IBOutlet weak var downloadingFileNameLabel: UILabel!
    @IBOutlet weak var downloadingProgressView: UIProgressView!
    @IBOutlet weak var downloadingProgressLabel: UILabel!
    @IBOutlet weak var downloadingNetworkSpeedLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
