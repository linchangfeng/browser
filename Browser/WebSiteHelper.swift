//
//  WebSiteHelper.swift
//  Browser
//
//  Created by lin on 08/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Alamofire

class WebSiteHelper: NSObject {
    
    class func webSiteImageLink(withURLString urlString: String, complete: @escaping (_ iconLink: String) -> Void) {
        
        let headers = [
            "Cookie":"BAIDUID=768DF1334C14FA05D19E974FC5F06C0F:FG=1; BIDUPSID=768DF1334C14FA05D19E974FC5F06C0F; PSTM=1476352020; BDSVRTM=0; BD_HOME=0; H_PS_PSSID=1425_19035_18240_21089_21191_21161_21340; BD_NOT_HTTPS=1",
            "User-Agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1",
            ]
        
        request(urlString, method: .get, headers: headers).responseString { (response) in
            if let data = response.data {
                
                var iconLink = ""
                
                if let html = String(data: data, encoding: .utf8) {
                    
                    if let appleIconRange = html.range(of: "<link(.)*?rel=\"apple-touch-icon(.)*?\"(.)*?>", options: .regularExpression, range: nil, locale: nil) {
                        
                        if appleIconRange.isEmpty == false {
                            var appleIconContent = html.substring(with: appleIconRange)
                            
                            if let hrefRange = appleIconContent.range(of: "href=\"(.)*?\"", options: .regularExpression, range: nil, locale: nil), hrefRange.isEmpty == false {
                                
                                appleIconContent = appleIconContent.substring(with: hrefRange)
                                
                                if let urlRange = appleIconContent.range(of: "\""), urlRange.isEmpty == false {
                                    
                                    let range = Range(uncheckedBounds: (lower: appleIconContent.index(after: urlRange.lowerBound), upper: appleIconContent.index(before: appleIconContent.endIndex)))
                                    
                                    appleIconContent = appleIconContent.substring(with: range)
                                    
                                    if appleIconContent.hasPrefix("http:") == false && appleIconContent.hasPrefix("https:") == false {
                                        appleIconContent = "http:" + appleIconContent
                                    }
                                    iconLink = appleIconContent
                                }
                            }
                        }
                    }
                }
                
                do {
                    if let url = URL(string: iconLink) {
                        let _ = try Data(contentsOf: url)
                    } else {
                        if let url = URL(string: urlString), let host = url.host, let scheme = url.scheme {
                            iconLink = scheme + "://" + host + "/favicon.ico"
                        }
                    }
                    complete(iconLink)
                    
                } catch {
                    print(error)
                    
                    if let url = URL(string: urlString), let host = url.host, let scheme = url.scheme {
                        iconLink = scheme + "://" + host + "/favicon.ico"
                    }
                    
                    complete(iconLink)
                }
            }
        }
    }

}
