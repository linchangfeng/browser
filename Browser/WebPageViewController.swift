//
//  ViewController.swift
//  Browser
//
//  Created by lin on 13/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import WebKit
import SnapKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

enum HintType {
    case history
    case searchHint
    case synthesized
}

class WebPageViewController: UIViewController, UISearchBarDelegate, WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var visualEffectViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressView: UIProgressView!

    @IBOutlet weak var searchBar: UISearchBar!
    
    fileprivate var refreshImage = UIImage(named: "SearchBarRefresh")
    fileprivate var cancelImage = UIImage(named: "Cancel")
    
    fileprivate var hintVisualEffectView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
    fileprivate var hintTableView: UITableView = UITableView()
    fileprivate var frequentylHeaderView = HintTableViewHeaderView(withTitle: __("经常访问"), icon: UIImage(named: "Frequently")!, deleteAction: #selector(deleteFrequentlyAction))
    fileprivate var searchRecordHeaderView = HintTableViewHeaderView(withTitle: __("历史"), icon: UIImage(named: "Time")!, deleteAction: #selector(deleteSearchRecordAction))
    
    var configuration: WKWebViewConfiguration = WKWebViewConfiguration() {
        didSet {
            self.webView = WKWebView(frame: .zero, configuration: self.configuration)
        }
    }
    var webView: WKWebView = WKWebView()
    var defaultURL: URL?
    var currentURL: URL?
    
    var bannerView: GADBannerView?
    
    private var visualView: UIVisualEffectView!
    
    fileprivate var hintType = HintType.synthesized {
        didSet {
            
            if self.hintType == .history {
                self.allHistories = HistoryModel.default.allHistories()
            } else if self.hintType == .synthesized {
                self.frequentlyHistories = HistoryModel.default.mostVisitHistory()
                self.searchRecords = SearchRecordModel.default.allSearchRecordRecord()
            }
            
            self.hintTableView.reloadData()
        }
    }
    
    fileprivate var frequentlyHistories = HistoryModel.default.mostVisitHistory()
    fileprivate var searchRecords = SearchRecordModel.default.allSearchRecordRecord()
    fileprivate var allHistories = HistoryModel.default.allHistories()
    fileprivate var searchHint = [String]()
    
    private var showsCancelLoading = false
    private var showsRefreshing = false
    private var isWebViewSuccessfulLoaded = false
    private var maxTryRequest = 0;
    private var isVisualViewShowing = true
    private var currentSearchtext: String?
    private var currentDownloadingURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBar.placeholder = __("搜索或输入网址")
    
        self.searchBar.delegate = self
        self.searchBar.backgroundImage = UIImage()
        self.searchBar.showsBookmarkButton = true
        self.searchBar.autocapitalizationType = .none
        
        self.initWebView()
        
        if kParamsManager.bool(forName: kParamNameShowsBanner) {
            self.bannerView = AdAssistant.default.createBannerView(rootViewController: self)
            self.view.addSubview(self.bannerView!)
            self.bannerView!.snp.makeConstraints({ (make) in
                make.left.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(0)
            })
        }
        
        self.webView.snp.makeConstraints { (make) in
            make.width.top.equalToSuperview()
            
            if self.bannerView == nil {
                make.height.equalToSuperview()
            }
            else {
                make.bottom.equalTo(self.bannerView!.snp.top)
            }
        }
        
        self.initTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.progressView.removeConstraints(self.progressView.constraints)
        self.webView.updateConstraints()
        self.hintTableView.updateConstraints()
        
        self.progressView.snp.makeConstraints { (make) in
            make.height.equalTo(2)
            make.left.right.equalTo(self.visualEffectView)
            make.bottom.equalTo(self.visualEffectView)
        }
        
        let isOrientation = self.view.width > self.view.height
        
        self.visualEffectViewHeightContraint.constant = isOrientation ? 44 : 64
        
        self.searchBarTopConstraint.constant = isOrientation ? 0 : 20
        
        let bottomInset: CGFloat = kParamsManager.bool(forName: kParamNameShowsBanner) ? 0 : 44
        self.webView.scrollView.contentInset = UIEdgeInsets(top: self.visualEffectViewHeightContraint.constant, left: 0, bottom: bottomInset, right: 0)
        
        self.hintVisualEffectView.snp.updateConstraints { (make) in
            make.top.equalTo(self.visualEffectViewHeightContraint.constant)
        }
        
        Util.setTimeout(time: DispatchTime.now() + 0.1, execute: {
            if UserDefaults.standard.bool(forKey: kFullScreen) && self.isVisualViewShowing == false && isOrientation {
                self.hideVisualEffectView(withDuration: 0.1)
            }
        })
        
        self.frequentylHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: 30)
        self.searchRecordHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: 30)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        
        self.hideHintVisualEffectView(withDuration: 0.01)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(WebPageViewController.webViewInteractDisable), name: NSNotification.Name(rawValue: "WebViewInteractDisable"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WebPageViewController.webViewInteractEnable), name: NSNotification.Name(rawValue: "WebViewInteractEnable"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WebPageViewController.privateModeChanged), name: NSNotification.Name(rawValue: "PrivateModeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WebPageViewController.clearSearchBarNotificatinHandler), name: kClearSearchBarNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "WebViewInteractDisable"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "WebViewInteractEnable"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "PrivateModeChanged"), object: nil)
        NotificationCenter.default.removeObserver(self, name: kClearSearchBarNotification, object: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let url = self.webView.url {
            if keyPath == "estimatedProgress" && Global.default.isBaseURL(urlString: url.absoluteString) == false {
                if let webView = object as? WKWebView {
                    
                    self.progressView.alpha = 1.0
                    self.progressView.setProgress(Float(webView.estimatedProgress), animated: true)
                    
                    if webView.estimatedProgress >= 1.0 {
                        UIView.animate(withDuration: 0.35, delay: 0.35, options: [.curveEaseInOut], animations: {
                            self.progressView.alpha = 0
                        }, completion: { (finished) in
                            if finished {
                                self.progressView.progress = 0.0
                            }
                        })
                    }
                } else {
                    super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
                }
            }
        }
    }
    
    // MARK: - Notification handler
    
    func webViewInteractDisable() {
        self.webView.isUserInteractionEnabled = false
        self.searchBar.isUserInteractionEnabled = false
    }
    
    func webViewInteractEnable() {
        self.webView.isUserInteractionEnabled = true
        self.searchBar.isUserInteractionEnabled = true
    }
    
    func privateModeChanged() {
        if #available(iOS 9.0, *) {
            self.webView.configuration.websiteDataStore = UserDefaults.standard.bool(forKey: kPrivateMode) ? WKWebsiteDataStore.nonPersistent() : WKWebsiteDataStore.default()
        } else {
            
        }
    }
    
    func clearSearchBarNotificatinHandler() {
        self.searchBar.text = ""
    }
    
    // MARK: - Gesture action
    func tapOnWebViewGesture(tapGesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // MARK: - webView delegate
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        self.searchBar.resignFirstResponder()
        
        if self.isNotHomePageOrBaseURL(url: webView.url!) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.searchBar.text = webView.url?.absoluteString
            
            self.showsCancelLoading = true
            self.showsRefreshing = false
            self.searchBar.showsBookmarkButton = true
            self.searchBar.setShowsCancelButton(false, animated: true)
            self.searchBar.setImage(self.cancelImage, for: .bookmark, state: .normal)
        } else {
            self.searchBar.text = ""
        }
        
        NotificationCenter.default.post(name: kSwitchBackButtonEnableNotification, object: webView.canGoBack)
        NotificationCenter.default.post(name: kSwitchForwardButtonEnableNotification, object: webView.canGoForward)
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.isWebViewSuccessfulLoaded = true
        
        if let title = webView.title {
            if Global.default.isBaseURL(urlString: webView.url!.absoluteString) == false {
                self.searchBar.text = title
            }
        }
        
        if self.isNotHomePageOrBaseURL(url: webView.url!) {
            self.showsRefreshing = true
            self.showsCancelLoading = false
            self.searchBar.setImage(self.refreshImage, for: .bookmark, state: .normal)
            
            if UserDefaults.standard.bool(forKey: kPrivateMode) == false {
                self.generateHistory(withWebView: webView)
                
                if self.currentSearchtext != nil {
                    
                    self.refreshRecord(withUrl: webView.url?.absoluteString, andType: true)
                    self.currentSearchtext = nil
                }
            }
        } else {
            self.searchBar.text = ""
        }
        
        NotificationCenter.default.post(name: kSwitchBackButtonEnableNotification, object: webView.canGoBack)
        NotificationCenter.default.post(name: kSwitchForwardButtonEnableNotification, object: webView.canGoForward)
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        if navigationAction.targetFrame == nil {
            self.showsCancelLoading = false
            self.showsRefreshing = true
            self.searchBar.setImage(self.refreshImage, for: .bookmark, state: .normal)
            NotificationCenter.default.post(name: kWebViewOpenBlankNotification, object: webView, userInfo: ["url": navigationAction.request.url as Any])
        }
        
        return nil
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        let response = navigationResponse.response
        
        if let mimeType = response.mimeType {
            if Util.downloadableType(mimeTye: mimeType) {
                self.currentDownloadingURL = webView.url!.absoluteString
                let fileName = response.suggestedFilename ?? "Unknown"
                
                let fileSize = String(format: "%.2f", DownloadUtility.calculateFileSizeInUnit(response.expectedContentLength))
                let sizeUnit = DownloadUtility.calculateUnit(response.expectedContentLength)
                let alertSheet = UIAlertController(title: "\(fileName.removingPercentEncoding!)", message: "\(__("文件大小"))：\(fileSize)\(sizeUnit)", preferredStyle: .actionSheet)
                
                let downloadAction = UIAlertAction(title: __("下载"), style: .default, handler: { (action) in
                    DownloadManager.share.addDownloadtasks(withLink: URL(string: self.currentDownloadingURL)!, fileName: fileName, expectedContentLength: response.expectedContentLength)
                })
                
                let cancelAction = UIAlertAction(title: __("取消"), style: .cancel, handler: nil)
                
                alertSheet.addAction(downloadAction)
                alertSheet.addAction(cancelAction)
                
                self.present(alertSheet, animated: true, completion: nil)
                decisionHandler(.cancel)
                
                self.showsRefreshing = true
                self.showsCancelLoading = false
                self.searchBar.setImage(self.refreshImage, for: .bookmark, state: .normal)
            }
        }
        
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        if kGlobal.isHomePage(url: webView.url!.absoluteString) == false {
            self.isWebViewSuccessfulLoaded = false
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(#function, error)
        if (error as NSError).code == -1009 {   // Network unavaliable
            let alert = UIAlertController(title: "", message: __("请检查WiFi无线网络设置"), preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: __("确定"), style: .default, handler: nil)
            alert.addAction(confirmAction)
            
            self.present(alert, animated: true, completion: nil)
            
            self.showsRefreshing = true
            self.showsCancelLoading = false
            self.searchBar.setImage(self.refreshImage, for: .bookmark, state: .normal)
            
        } else if (error as NSError).code == -999 {
            //
        } else {
            
            if self.maxTryRequest < kMaxTryRequest {
                self.maxTryRequest += 1
                if let text = self.searchBar.text {
                    var urlString = text;
                    if Util.contentHttps(text) == false && Util.contentHttp(text) == false {
                        urlString = Util.formatURL(text, withPrefix: "https://")
                    } else if Util.contentHttp(text) {
                        urlString = Util.formatURL(text, withPrefix: "https://")
                    } else {
                        urlString = Util.formatURL(text, withPrefix: "http://")
                    }
                    
                    if let url = URL(string: urlString) {
                        let request = URLRequest(url: url)
                        webView.load(request)
                        
                        self.showsCancelLoading = true
                        self.showsRefreshing = false
                        self.searchBar.setImage(self.cancelImage, for: .bookmark, state: .normal)
                    }
                }
            } else {
                print(#function, "fail load page")
                
                if let text = self.searchBar.text {
                    let urlString = kDefaultSearchEngine + text
                    if let url = URL(string: urlString) {
                        webView.load(URLRequest(url: url))
                        
                        if UserDefaults.standard.bool(forKey: kPrivateMode) == false && self.currentSearchtext != nil {
                            
                            self.refreshRecord(withUrl: urlString, andType: false)
                            
                            self.currentSearchtext = nil
                            
                            self.showsRefreshing = true
                            self.showsCancelLoading = false
                            self.searchBar.setImage(self.refreshImage, for: .bookmark, state: .normal)
                        }
                    }
                }
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
//        self.webView.configuration.userContentController.removeAllUserScripts()
//        
//        if UserDefaults.standard.bool(forKey: kNoGrapeMode) && self.isNotHomePageOrBaseURL(url: webView.url!) {
//            self.webView.configuration.userContentController.addUserScript(self.filterImageUserController().userScripts.first!)
//        }
        
        self.currentURL = webView.url
        
        if self.isNotHomePageOrBaseURL(url: webView.url!) {
            self.searchBar.showsBookmarkButton = true
            self.showsCancelLoading = true
            self.showsRefreshing = false
            self.searchBar.setImage(self.cancelImage, for: .bookmark, state: .normal)
        }
        
        if let url = navigationAction.request.url {
            if url.absoluteString == "action:search" {
                if self.searchBarShouldBeginEditing(self.searchBar) {
                    self.searchBar.becomeFirstResponder()
                }
                
                decisionHandler(.cancel)
                return
            }
        }
        
        decisionHandler(.allow)
    }
    
    // MARK: - ScrollView delegate
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.webView.scrollView {
            self.searchBar.resignFirstResponder()
            
            if self.isWebViewSuccessfulLoaded && Global.default.isBaseURL(urlString: self.webView.url!.absoluteString) == false {
                self.searchBar.text = self.webView.title
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if UserDefaults.standard.bool(forKey: kFullScreen) && scrollView == self.webView.scrollView {
            let velocity = scrollView.panGestureRecognizer.velocity(in: view)
            
            if velocity.y < 0.0 && self.isVisualViewShowing == true {
                self.hideVisualEffectView(withDuration: 0.35)
            } else if velocity.y > kVelocityToShowToolBar && self.isVisualViewShowing == false {
                self.showVisualEffectView(withDuration: 0.35)
            }
        } else {
            self.searchBar.resignFirstResponder()
        }
    }
    
    // MARK: - Search bar delegate
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if kGlobal.hasUserRated() == false && kParamsManager.bool(forName: kParamNameShowsRatingAlert) {
            let title = kParamsManager.value(forName: kParamNameRatingTitle) as? String
            let message = kParamsManager.value(forName: kParamNameRatingMessage) as? String
            let cancelText = kParamsManager.value(forName: kParamNameRatingCancelText) as? String
            let confirmText = kParamsManager.value(forName: kParamNameRatingConfirmTitle) as? String
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: cancelText, style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: confirmText, style: .default, handler: { (action) in
                kUtil.openAppInAppStore(withAppID: kAppID)
                kGlobal.hasRate()
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            return false
        }
        
        return true
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if self.showsRefreshing {
            self.webView.reload()
        } else if self.showsCancelLoading {
            self.webView.stopLoading()
            self.showsRefreshing = true
            self.showsCancelLoading = false
            searchBar.setImage(self.refreshImage, for: .bookmark, state: .normal)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.showsBookmarkButton = false
        self.maxTryRequest = 0
        
        if self.currentSearchtext != nil {
            self.searchBar.text = self.currentSearchtext
        } else {
            if self.isWebViewSuccessfulLoaded {
                if let url = self.webView.url  {
                    if Global.default.isBaseURL(urlString: url.absoluteString) == false {
                        searchBar.text = url.absoluteString
                    }
                }
            }
        }
        
        self.showHintVisualEffectView(withDuration: 0.35)
        
        if searchBar.text == "" {
            self.hintType = .synthesized
        } else {
            self.hintType = .history
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            self.hintType = .synthesized
        } else {
            self.currentSearchtext = self.searchBar.text
            if self.hintType != .searchHint {
                self.hintType = .searchHint
            }
            
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            self.perform(#selector(fetchHint(withText:)), with: searchBar.text, afterDelay: 0.5)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        searchBar.showsBookmarkButton = true
        searchBar.setShowsCancelButton(false, animated: true)
        
        self.currentSearchtext = searchBar.text
        
        if self.webView.isLoading {
            self.webView.stopLoading()
        }
        
        if let url = self.webView.url {
            if searchBar.text != url.absoluteString, let text = searchBar.text {
                if Util.urlValid(text) {
                    
                    var finalURLString = text
                    if Util.contentHttp(text) == false && Util.contentHttps(text) == false {
                        finalURLString = Util.formatURL(text, withPrefix: "http://")
                    }
                    
                    if let url = URL(string: finalURLString) {
                        let request = URLRequest(url: url)
                        self.webView.load(request)
                    }
                    
                } else {
                    let urlString = kDefaultSearchEngine + text
                    if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                        self.webView.load(URLRequest(url: url))
                        
                        if UserDefaults.standard.bool(forKey: kPrivateMode) == false && self.currentSearchtext != nil {
                            
                            self.refreshRecord(withUrl: urlString, andType: false)
                            
                            self.currentSearchtext = nil
                        }
                    }
                }
            }
        } else {
            if self.isWebViewSuccessfulLoaded && Global.default.isBaseURL(urlString: webView.url!.absoluteString) == false {
                searchBar.text = self.webView.title
            }
        }
        
        self.hideHintVisualEffectView(withDuration: 0.35)
        
        AdAssistant.default.signal(signal: .search)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.showsBookmarkButton = true
        searchBar.setImage(self.webView.isLoading ? self.cancelImage : self.refreshImage, for: .bookmark, state: .normal)
        self.showsCancelLoading = self.webView.isLoading
        self.showsRefreshing = !self.webView.isLoading
        self.currentSearchtext = nil
        
        if self.isWebViewSuccessfulLoaded  && Global.default.isBaseURL(urlString: webView.url!.absoluteString) == false {
            searchBar.text = self.webView.title
        }
        
        self.hideHintVisualEffectView(withDuration: 0.35)
    }

    // MARK: Button Action
    func deleteFrequentlyAction() {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: __("清空经常访问记录"), style: .destructive, handler: { (action) in
            HistoryModel.default.deleteFrequentlies()
            self.frequentlyHistories.removeAll()
            self.hintTableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: __("取消"), style: .cancel, handler: nil))
        self.present(sheet, animated: true, completion: nil)
    }
    
    func deleteSearchRecordAction() {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: __("清空历史记录"), style: .destructive, handler: { (action) in
            SearchRecordModel.default.deleteAllRecord()
            self.searchRecords.removeAll()
            self.hintTableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: __("取消"), style: .cancel, handler: nil))
        self.present(sheet, animated: true, completion: nil)
    }
    
    // MARK: - Private
    func initWebView() {
        
        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.scrollView.delegate = self
        self.searchBar.setImage(UIImage(), for: .bookmark, state: .normal)
        
        let tapOnWebViewGesture = UITapGestureRecognizer(target: self, action: #selector(WebPageViewController.tapOnWebViewGesture))
        tapOnWebViewGesture.cancelsTouchesInView = true
        self.webView.addGestureRecognizer(tapOnWebViewGesture)
        
        self.view.addSubview(self.webView)
        
        let currentUA = UserDefaults.standard.object(forKey: kCurrentUA)
        
        if currentUA == nil {
            
            self.webView.evaluateJavaScript("navigator.userAgent") { (result, error) in
                let userAgent = result as! String
                UserDefaults.standard.set(userAgent, forKey: kCurrentUA)
            }
        }
        
        if #available(iOS 9.0, *) {
            self.webView.configuration.websiteDataStore = UserDefaults.standard.bool(forKey: kPrivateMode) ? WKWebsiteDataStore.nonPersistent() : WKWebsiteDataStore.default()
        }
        
        self.view.bringSubview(toFront: self.visualEffectView)
        
        if self.defaultURL != nil {
            self.loadRequest(withURL: self.defaultURL!)
        } else {
            self.loadRequest(withURL: URL(string: Global.default.homePage()))
        }
    }
    
    func loadRequest(withURL url: URL?) {
        let request = URLRequest(url: url!)
        self.webView.load(request)
    }
    
    func generateHistory(withWebView webView: WKWebView) {
        if let title = webView.title, let url = webView.url {
            
            let urlString = url.absoluteString
            
            if HistoryModel.default.recordExist(title: title, url: urlString) {
                let records = HistoryModel.default.historyRecord(withTitle: title, andUrl: urlString)
                
                for record in records {
                    HistoryModel.default.updateHistoryDate(history: record, date: NSDate())
                    HistoryModel.default.updateHistoryVisitTimes(history: record)
                }
            } else {
                var data: NSData = NSData()
                
                WebSiteHelper.webSiteImageLink(withURLString: urlString, complete: { (iconLink) in
                    
                    if iconLink != "" {
                        if let url = URL(string: iconLink) {
                            if let tmp = NSData(contentsOf: url) {
                                data = tmp
                            }
                        }
                    } else {
                        if let image = UIImage(named: "Browser"), let pngData = UIImagePNGRepresentation(image) {
                            data = NSData(data: pngData)
                        }
                    }
                    
                    HistoryModel.default.createNewHistory(title: title, url: urlString, iconLink: iconLink, iconData: data)
                })
            }
        }
    }
    
    func refreshRecord(withUrl url: String?, andType type: Bool) {
        if SearchRecordModel.default.isSearchRecordExist(withTitle: self.currentSearchtext!, andUrl: url, andType: type) {
            let record = SearchRecordModel.default.searchRecord(withTitle: self.currentSearchtext!, andUrl: url, andType: type)
            SearchRecordModel.default.updateSearchRecordDate(searchRecord: record.first!, date: NSDate())
        } else {
            SearchRecordModel.default.createSearchRecord(withTitle: self.currentSearchtext!, andUrl: url, andType: type)
        }
    }
    
    func hideVisualEffectView(withDuration duration: Float, after delay: Float = 0) {
        NotificationCenter.default.post(name: kHideToolBarNotification, object: nil, userInfo: nil)
        
        let offset: CGFloat = (self.view.width > self.view.height ? 0 : 20)
        
        UIView.animate(withDuration: TimeInterval(duration), delay: TimeInterval(delay), usingSpringWithDamping: 8, initialSpringVelocity: 8, options: [.allowUserInteraction], animations: {
            
            self.visualEffectView.y = -self.visualEffectViewHeightContraint.constant + offset
            self.searchBar.isHidden = true
            self.webView.scrollView.contentInset.top = offset
            self.webView.scrollView.contentInset.bottom = 0.0
            
        }, completion: { (finished) in
            self.isVisualViewShowing = false
        })
    }
    
    func showVisualEffectView(withDuration duration: Float, after delay: Float = 0) {
        NotificationCenter.default.post(name: kShowToolBarNotification, object: nil, userInfo: nil)
        
        UIView.animate(withDuration: TimeInterval(duration), delay: TimeInterval(delay), usingSpringWithDamping: 8, initialSpringVelocity: 8, options: [.allowUserInteraction], animations: {
            self.searchBar.isHidden = false
            self.visualEffectView.y = 0
            self.webView.scrollView.contentInset.bottom = 44
            self.webView.scrollView.contentInset.top = self.visualEffectViewHeightContraint.constant
        }, completion: { (finished) in
            self.isVisualViewShowing = true
        })
    }
    
    func showHintVisualEffectView(withDuration duration: Float) {
        UIView.animate(withDuration: TimeInterval(duration), animations: {
            self.hintVisualEffectView.alpha = 1
        }, completion: nil)
    }
    
    func hideHintVisualEffectView(withDuration duration: Float) {
        UIView.animate(withDuration: TimeInterval(duration), animations: {
            self.hintVisualEffectView.alpha = 0
        }, completion: nil)
    }
    
    func isNotHomePageOrBaseURL(url: URL) -> Bool {
        return kGlobal.isHomePage(url: url.absoluteString) == false && kGlobal.isBaseURL(urlString: url.absoluteString) == false
    }
    
    func filterImageSource() -> String {
        if let path = Bundle.main.path(forResource: "FilterImage", ofType: "js") {
            do {
                let source = try String(contentsOfFile: path, encoding: .utf8)
                return source
            } catch {
                debugPrint(error)
                return ""
            }
        }
        return ""
    }
    
    func filterImageUserController() -> WKUserContentController {
        let source = self.filterImageSource()
        let userScript = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let userContentController = WKUserContentController()
        userContentController.addUserScript(userScript)
        return userContentController
    }
    
}

extension WebPageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func initTableView() {
        self.hintTableView.delegate = self
        self.hintTableView.dataSource = self
        self.hintTableView.backgroundColor = UIColor.clear
        self.hintTableView.register(HintTableViewCell.self, forCellReuseIdentifier: "HintCell")
        self.hintVisualEffectView.addSubview(self.hintTableView)
        
        let tableFootView = UIView()
        tableFootView.backgroundColor = UIColor.clear
        self.hintTableView.tableFooterView = tableFootView
        
        self.view.addSubview(self.hintVisualEffectView)
        
        self.hintVisualEffectView.snp.remakeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.left.bottom.equalTo(self.view)
            make.top.equalTo(self.visualEffectViewHeightContraint.constant)
        }
        
        self.hintTableView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        self.hintVisualEffectView.alpha = 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.hintType == .synthesized {
            if self.frequentlyHistories.isEmpty == false && self.searchRecords.isEmpty == false {
                return 2
            }
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.hintType == .history {
            return self.allHistories.count
        } else if self.hintType == .searchHint {
            return self.searchHint.count
        } else {
            if self.frequentlyHistories.isEmpty == false && self.searchRecords.isEmpty == false {
                if section == 0 {
                    return self.frequentlyHistories.count
                } else {
                    return self.searchRecords.count
                }
            } else {
                if self.frequentlyHistories.isEmpty {
                    return self.searchRecords.count
                } else {
                    return self.frequentlyHistories.count
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HintCell", for: indexPath) as! HintTableViewCell
        
        if self.hintType == .history {
            
            let history = self.allHistories[indexPath.row]
            self.fillCellData(cell, with: history)
            
        } else if self.hintType == .searchHint {
            cell.iconImageView.image = UIImage(named: "Search")
            cell.searchHintLabel.text = self.searchHint[indexPath.row]
            
            cell.isLink(link: false)
            
        } else {
            if self.frequentlyHistories.isEmpty == false && self.searchRecords.isEmpty == false {
                
                if indexPath.section == 0 {
                    
                    let history = self.frequentlyHistories[indexPath.row]
                    self.fillCellData(cell, with: history)
                    
                } else {
                    let record = self.searchRecords[indexPath.row]
                    self.fillCell(cell, with: record)
                }
                
            } else {
                if self.frequentlyHistories.isEmpty {
                    let record = self.searchRecords[indexPath.row]
                    
                    self.fillCell(cell, with: record)
                } else {
                    let history = self.frequentlyHistories[indexPath.row]
                    self.fillCellData(cell, with: history)
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.searchBar.resignFirstResponder()
        self.searchBar.showsBookmarkButton = true
        self.searchBar.setShowsCancelButton(false, animated: true)
        
        self.hideHintVisualEffectView(withDuration: 0.35)
        
        if self.hintType == .history {
            if let urlString = self.allHistories[indexPath.row].url, let url = URL(string: urlString) {
                self.webView.load(URLRequest(url: url))
            }
        } else if self.hintType == .searchHint {
            let searchText = self.searchHint[indexPath.row]
            let urlString = kDefaultSearchEngine + searchText
            if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                self.webView.load(URLRequest(url: url))
            }
        } else {
            if self.frequentlyHistories.isEmpty == false && self.searchRecords.isEmpty == false {
                if indexPath.section == 0 {
                    if let urlString = self.frequentlyHistories[indexPath.row].url, let url = URL(string: urlString) {
                        self.webView.load(URLRequest(url: url))
                    }
                } else {
                    if let urlString = self.searchRecords[indexPath.row].url, let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                        self.webView.load(URLRequest(url: url))
                    }
                }
            } else if self.frequentlyHistories.isEmpty {
                if let urlString = self.searchRecords[indexPath.row].url, let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    self.webView.load(URLRequest(url: url))
                }
            } else if self.searchRecords.isEmpty {
                if let urlString = self.frequentlyHistories[indexPath.row].url, let url = URL(string: urlString) {
                    self.webView.load(URLRequest(url: url))
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.hintType == .synthesized {
            if self.frequentlyHistories.isEmpty == false || self.searchRecords.isEmpty == false {
                return 30
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.hintType == .synthesized {
            if self.frequentlyHistories.isEmpty == false && self.searchRecords.isEmpty == false {
                if section == 0 {
                    return self.frequentylHeaderView
                } else {
                    return self.searchRecordHeaderView
                }
            } else if self.frequentlyHistories.isEmpty {
                return self.searchRecordHeaderView
            } else if self.searchRecords.isEmpty {
                return self.frequentylHeaderView
            } else {
                return nil
            }
        }
        
        return nil
    }
    
    // Private
    
    func fillCellData(_ cell: HintTableViewCell, with history: History) {
        
        cell.isLink(link: true)
        
        cell.titleLabel.text = history.title
        cell.urlLabel.text = history.url
        
        if let data = history.icon {
            let image = UIImage(data: data as Data)
            if image == nil {
                cell.iconImageView.image = UIImage(named: "Browser")
            } else {
                cell.iconImageView.image = image
            }
        } else if let iconLink = history.iconLink, iconLink != "" {
            cell.iconImageView.sd_setImage(with: URL(string: iconLink), completed: { (image, error, _, url) in
                if image == nil {
                    cell.iconImageView.image = UIImage(named: "Browser")
                }
            })
        }
    }
    
    func fillCell(_ cell: HintTableViewCell, with record: SearchRecord) {
        if record.isLink {
            cell.iconImageView.image = UIImage(named: "Browser")
            cell.titleLabel.text = record.title
            cell.urlLabel.text = record.url
            
            cell.isLink(link: true)
        } else {
            cell.iconImageView.image = UIImage(named: "Search")
            cell.searchHintLabel.text = record.title
            cell.isLink(link: false)
        }
    }
    
    func fetchHint(withText text: String) {
    
        if let urlString = (kHintUrl + text).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            request(urlString, method: .get, parameters: nil).validate(statusCode: 200..<300).responseString { response in
                if var result = response.result.value {
                    
                    result = result.substring(from: result.range(of: kcb + "(")!.upperBound)
                    let result = result.substring(to: result.range(of: ");")!.lowerBound)
                    
                    let json = JSON.parse(result)
                    
                    self.searchHint = json["s"].arrayValue.map({ $0.stringValue })
                    self.hintTableView.reloadData()
                }
            }
        }
    }
}
