//
//  Util.swift
//  Browser
//
//  Created by lin on 13/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage


class Util: NSObject {
    
    class func urlValid(_ text: String) -> Bool {
        let reg = "((http|https)://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        
        let urlTest = NSPredicate(format: "SELF MATCHES %@", reg)
        
        return urlTest.evaluate(with: text)
    }
    
    class func urlReachable(string urlString: String) -> Bool {
        
        if let url = URL(string: urlString), let _ = url.scheme, let _ = url.host {
            return true
        }
        
        return false
    }
    
    class func contentHttp(_ string: String) -> Bool {
        return string.hasPrefix("http://")
    }
    
    class func contentHttps(_ string: String) -> Bool {
        return string.hasPrefix("https://")
    }
    
    class func formatURL(_ string: String, withPrefix prefix: String) -> String {
        
        var urlString = string
        
        let httpRange = string.range(of: "http://")
        let httpsRange = string.range(of: "https://")
        
        if httpRange != nil {
            urlString.removeSubrange(httpRange!)
        } else if httpsRange != nil {
            urlString.removeSubrange(httpsRange!)
        }
        
        urlString = "\(prefix)\(urlString)"
        
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlPathAllowed)!
        
        return urlString
    }
    
    class func replaceUserAgent(with sourceUA: String, to targetUA: String) -> String {
        
        var finalUA = sourceUA
        
        let regex = try! NSRegularExpression(pattern: "\\(.*?\\)", options: .caseInsensitive)
        let range = NSRange(location: 0, length: sourceUA.characters.count)
        
        if let firstMatchRange = regex.firstMatch(in: sourceUA, options: [], range: range)?.range {
            finalUA = regex.stringByReplacingMatches(in: sourceUA, options: [.anchored], range: firstMatchRange, withTemplate: targetUA)
        }
        
        return finalUA
    }
    
    class func setTimeout(time: DispatchTime, execute block: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: time, execute: block)
    }
    
    class func moveFile(from source: URL, to target: URL) {
        if FileManager.default.fileExists(atPath: source.absoluteString) == false {
            debugPrint("Souce file does't exist")
            return
        }
        
        if FileManager.default.fileExists(atPath: target.absoluteString) == false {
            debugPrint("Target file does't exist")
            return
        }
        
        do {
            try FileManager.default.moveItem(at: source, to: target)
        } catch {
            debugPrint(#function, error)
        }
    }
    
    class func downloadableType(mimeTye: String) -> Bool {
        return kDownloadableContentType.contains(mimeTye)
    }
    
    class func topViewController() -> UIViewController {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var c = appDelegate.window?.rootViewController
        while c?.presentedViewController != nil {
            c = c?.presentedViewController
        }
        
        return c!
    }
}
