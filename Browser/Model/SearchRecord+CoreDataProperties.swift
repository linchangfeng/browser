//
//  SearchRecord+CoreDataProperties.swift
//  Browser
//
//  Created by lin on 01/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import Foundation
import CoreData


extension SearchRecord {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SearchRecord> {
        return NSFetchRequest<SearchRecord>(entityName: "SearchRecord");
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var isLink: Bool
    @NSManaged public var url: String?

}
