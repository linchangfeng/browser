//
//  History+CoreDataProperties.swift
//  Browser
//
//  Created by lin on 01/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import Foundation
import CoreData


extension History {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<History> {
        return NSFetchRequest<History>(entityName: "History");
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var icon: NSData?
    @NSManaged public var iconLink: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?
    @NSManaged public var visitTimes: Int16

}
