//
//  SearchRecordRecordModel.swift
//  Browser
//
//  Created by lin on 01/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import CoreData

class SearchRecordModel: NSObject {
    
    static let `default` = SearchRecordModel()
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.shared.delegate as! AppDelegate
    }()
    
    lazy var context: NSManagedObjectContext = {
        
//        return self.appDelegate.persistentContainer.viewContext
        return CoreDataStack.sharedStack.managedObjectContext
    }()
    
    lazy var fetchRequest: NSFetchRequest<SearchRecord> = {
        return SearchRecord.fetchRequest()
    }()
    
    func saveContext() {
        do {
            try self.context.save()
        } catch {
            print(#function, error)
        }
    }
    
    func allSearchRecordRecord() -> [SearchRecord] {
        do {
            let fetchRequest: NSFetchRequest<SearchRecord> = SearchRecord.fetchRequest()
            
            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            fetchRequest.sortDescriptors = [sortDescriptor]
            
            let searchRecord = try self.context.fetch(fetchRequest)
            
            return searchRecord
            
        } catch {
            print(#function, error)
        }
        
        return []
    }
    
    func searchRecord(withTitle title: String, andUrl url: String!, andType isLink: Bool) -> [SearchRecord] {
        
        let predicate = NSPredicate(format: "title == %@ AND url == %@ AND isLink == %@", title, url!, NSNumber(booleanLiteral: isLink))
        
        let fetchRequest: NSFetchRequest<SearchRecord> = SearchRecord.fetchRequest()
        fetchRequest.predicate = predicate
        
        do {
            let fetchResult = try self.context.fetch(fetchRequest)
            
            return fetchResult
        } catch {
            print(#function, error)
        }
        
        return []
    }
    
    func updateSearchRecordDate(searchRecord: SearchRecord, date: NSDate) {
        
        let fetchResult = self.searchRecord(withTitle: searchRecord.title!, andUrl: searchRecord.url!, andType: searchRecord.isLink)
        
        for result in fetchResult {
            result.date = date
        }
        
        self.saveContext()
    }
    
    func isRecordExist(record: SearchRecord) -> Bool {
        let fetchResult = self.searchRecord(withTitle: record.title!, andUrl: record.url!, andType: record.isLink)
        
        if fetchResult.count > 0 {
            return true
        }
        
        return false
    }
    
    func createSearchRecord(withTitle title: String, andUrl url: String?, andType isLink: Bool) {
        let newRecord = NSEntityDescription.insertNewObject(forEntityName: "SearchRecord", into: self.context) as! SearchRecord
        
        newRecord.title = title
        newRecord.url = url
        newRecord.isLink = isLink
        newRecord.date = NSDate()
        
        self.saveContext()
    }
    
    func isSearchRecordExist(withTitle title: String, andUrl url: String?, andType isLink: Bool) -> Bool {
        
        let record = self.searchRecord(withTitle: title, andUrl: url, andType: isLink)
        
        if record.count > 0 {
            return true
        }
        
        return false
    }
    
    func deleteAllRecord() {
        for record in self.allSearchRecordRecord() {
            self.context.delete(record)
        }
        
        self.saveContext()
    }
}
