//
//  HistoryModel.swift
//  Browser
//
//  Created by lin on 19/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import CoreData

class HistoryModel: NSObject {
    
    static let `default` = HistoryModel()
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.shared.delegate as! AppDelegate
    }()
    
    lazy var context: NSManagedObjectContext = {
        
//        return self.appDelegate.persistentContainer.viewContext
        return CoreDataStack.sharedStack.managedObjectContext
    }()
    
    lazy var fetchRequest: NSFetchRequest<History> = {
        return History.fetchRequest()
    }()
    
    func saveContext() {
        do {
            try self.context.save()
        } catch {
            print(#function, error)
        }
    }
    
    func allHistories() -> [History] {
        do {
            let fetchRequest: NSFetchRequest<History> = History.fetchRequest()
            
            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            fetchRequest.sortDescriptors = [sortDescriptor]
            
            let histories = try self.context.fetch(fetchRequest)
            
            return histories
            
        } catch {
            print(#function, error)
        }
        
        return []
    }
    
    func historyRecord(withTitle title: String, andUrl url: String) -> [History] {
        let predicate = NSPredicate(format: "title == %@ AND url == %@", title, url)
        let fetchRequest: NSFetchRequest<History> = History.fetchRequest()
        fetchRequest.predicate = predicate
        
        do {
            let fetchResult = try self.context.fetch(fetchRequest)
            
            return fetchResult
        } catch {
            print(#function, error)
        }
        
        return []
    }
    
    func createNewHistory(title: String, url: String, iconLink: String, iconData: NSData) {
        let newHistory = NSEntityDescription.insertNewObject(forEntityName: "History", into: self.context) as! History
        
        let date = NSDate()
        
        newHistory.title = title
        newHistory.url = url
        newHistory.icon = iconData
        newHistory.iconLink = iconLink
        newHistory.date = date
        newHistory.visitTimes = 1
        
        self.saveContext()
    }
    
    func isHistoryExist(history: History) -> Bool {
        let historyRecord = self.historyRecord(withTitle: history.title!, andUrl: history.url!)
        
        if historyRecord.count > 0 {
            return true
        }
        
        return false
    }
    
    func recordExist(title: String, url: String) -> Bool {
        let historyRecord = self.historyRecord(withTitle: title, andUrl: url)
        
        if historyRecord.count > 0 {
            return true
        }
        
        return false
    }
    
    func deleteHisotry(_ history: History) {
        self.context.delete(history)
        self.saveContext()
    }
    
    func deleteAllHisotries() {
        for history in self.allHistories() {
            self.context.delete(history)
        }
        
        self.saveContext()
    }
    
    func updateHistoryDate(history: History, date: NSDate) {
        let historyRecords = self.historyRecord(withTitle: history.title!, andUrl: history.url!)
        
        for historyRecord in historyRecords {
            historyRecord.date = date
        }
        
        self.saveContext()
    }
    
    func updateHistoryVisitTimes(history: History) {
        let historyRecotds = self.historyRecord(withTitle: history.title!, andUrl: history.url!)
        
        for historyRecord in historyRecotds {
            historyRecord.visitTimes = historyRecord.visitTimes + 1
        }
        
        self.saveContext()
    }
    
    func mostVisitHistory() -> [History] {
        let fetchRequest: NSFetchRequest<History> = History.fetchRequest()
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "visitTimes", ascending: false)]
        
        var final = [History]()
        
        do {
            let fetchResult = try self.context.fetch(fetchRequest)
            
            for result in fetchResult {
                if result.visitTimes >= Int16(kMaxFrequentlyVisit) {
                    final.append(result)
                }
            }
            
            return final
        } catch {
            print(#function, error)
        }
        
        return []
    }
    
    func deleteFrequentlies() {
        let histories = self.mostVisitHistory()
        
        for history in histories {
            self.context.delete(history)
        }
        
        self.saveContext()
    }
}
