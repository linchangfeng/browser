//
//  BookmarkModel.swift
//  Browser
//
//  Created by lin on 18/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import CoreData

class BookmarkModel: NSObject {
    
    static let `default` = BookmarkModel()
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.shared.delegate as! AppDelegate
    }()
    
    lazy var context: NSManagedObjectContext = {
        
//        return self.appDelegate.persistentContainer.viewContext
        return CoreDataStack.sharedStack.managedObjectContext
    }()
    
    lazy var fetchRequest: NSFetchRequest<Bookmark> = {
        return Bookmark.fetchRequest()
    }()
    
    func saveContext() {
        do {
            try self.context.save()
        } catch {
            print(#function, error)
        }
    }
    
    func allBookmarks() -> [Bookmark] {
        do {
            let fetchRequest: NSFetchRequest<Bookmark> = Bookmark.fetchRequest()
            let bookmarks = try self.context.fetch(fetchRequest)
            
            return bookmarks
            
        } catch {
            print(#function, error)
        }
        
        return []
    }
    
    func bookmarkRecord(withTitle title: String, andUrl url: String) -> [Bookmark] {
        let predicate = NSPredicate(format: "title == %@ AND url == %@", title, url)
        let fetchRequest: NSFetchRequest<Bookmark> = Bookmark.fetchRequest()
        fetchRequest.predicate = predicate
        
        do {
            let fetchResult = try self.context.fetch(fetchRequest)
            
            return fetchResult
        } catch {
            print(#function, error)
        }
        
        return []
    }
    
    func recordExist(title: String, url: String) -> Bool {
        
        let results = self.bookmarkRecord(withTitle: title, andUrl: url)
        
        if results.count > 0 {
            return true
        }
        
        return false
    }
    
    func createNewBookmark(title: String, url: String, iconLink: String, iconData: NSData) {
        
        if self.recordExist(title: title, url: url) == false {
            let newBookmark = NSEntityDescription.insertNewObject(forEntityName: "Bookmark", into: self.context) as! Bookmark
            
            newBookmark.title = title
            newBookmark.url = url
            newBookmark.icon = iconData
            newBookmark.iconLink = iconLink
            
            self.saveContext()
        }
    }
    
    func deleteBookmark(_ bookmark: Bookmark) {
        self.context.delete(bookmark)
        self.saveContext()
    }
    
}
