//
//  Bookmark+CoreDataProperties.swift
//  Browser
//
//  Created by lin on 01/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import Foundation
import CoreData


extension Bookmark {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Bookmark> {
        return NSFetchRequest<Bookmark>(entityName: "Bookmark");
    }

    @NSManaged public var icon: NSData?
    @NSManaged public var iconLink: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?

}
