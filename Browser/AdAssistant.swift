//
//  Ad.swift
//  Browser
//
//  Created by Tracy on 11/4/16.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class AdAssistant: NSObject, GADInterstitialDelegate {
    static let `default` = AdAssistant()
    
    enum Signal {
        case search, switchTab, launch
    }
    
    typealias SignalResultHandler = (Bool) -> Void
    
    private var interstitial: GADInterstitial?
    
    private var bannerUnitID: String!
    private var interstitialUnitID: String!
    
    private var hasReceivedInterstitialBefore = false
    
    private var searchTimes = 0
    private var switchTabTimes = 0
    
    let kUDKeyLaunchTimes = "AdAssistant.1"
   
    private override init() {
        super.init()
    }
    
    func setup(bannerUnitID: String, interstitialUnitID: String) {
        self.bannerUnitID = bannerUnitID
        self.interstitialUnitID = interstitialUnitID
        
        requestInterstitial()
        
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive(notification:)), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    func signal(signal: Signal, resultHandler: SignalResultHandler? = nil) {
        switch signal {
        case .search:
            self.searchTimes += 1
            
        case .switchTab:
            self.switchTabTimes += 1
            
        case .launch:
            var times = self.launchTimes()
            times += 1
            UserDefaults.standard.set(times, forKey: kUDKeyLaunchTimes)
        }
        
        self.presentInterstitialIfNeeded(signal: signal, resultHandler: resultHandler)
    }
    
    func createBannerView(rootViewController: UIViewController) -> GADBannerView {
        let bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = self.bannerUnitID
        bannerView.rootViewController = rootViewController
        
        let request = GADRequest()
        bannerView.load(request)
        
        return bannerView
    }
    
    // MARK: - - Notification
    func appDidBecomeActive(notification: Notification) {
        self.signal(signal: .launch, resultHandler: nil)
    }
    
    // MARK: - - GADInterstitialDelegate
    func interstitialDidReceiveAd(_ ad: GADInterstitial!) {
        if self.hasReceivedInterstitialBefore == false {
            self.hasReceivedInterstitialBefore = true
            self.presentInterstitialIfNeeded(signal: .launch, resultHandler: nil) // 因为第一次启动时的signal如果触发了interstitial，会因为interstitial未请求完成而present失败
                                                                            // 因此在第一次收到interstitial判断是否需要present
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial!) {
        self.requestInterstitial()
    }
    
    func interstitialDidFail(toPresentScreen ad: GADInterstitial!) {
        setTimeout(10, {
            self.requestInterstitial()
        })
    }
    
    // MARK: - - Private
    private func requestInterstitial() {
        self.interstitial = GADInterstitial(adUnitID: self.interstitialUnitID)
        self.interstitial?.delegate = self
        let request = GADRequest()
        self.interstitial?.load(request)
    }
    
    private func presentInterstitial() {
        self.interstitial?.present(fromRootViewController: Util.topViewController())
    }
    
    private func launchTimes() -> Int {
        return UserDefaults.standard.integer(forKey: kUDKeyLaunchTimes)
    }
    
    private func presentInterstitialIfNeeded(signal: Signal, resultHandler: SignalResultHandler?) {
        var isInterstitialPresented = false
        
        switch signal {
        case .search:
            let interval = kParamsManager.integer(forName: kParamNameSearchIntervalToShowInterstitial)
            if interval > 0 && self.searchTimes % interval == 0 {
                self.presentInterstitial()
                isInterstitialPresented = true
            }
            
        case .launch:
            let interval = kParamsManager.integer(forName: kParamNameLaunchIntervalToShowInterstitial)
            if interval > 0 && self.launchTimes() % interval == 0 {
                kUtil.repeat({ (stop) in
                    if Util.topViewController().isKind(of: PasscodeViewController.self) == false {
                        stop?[0] = true
                        self.presentInterstitial()
                    }
                }, interval: 0.5)
                
                isInterstitialPresented = true
            }
            
        default:
            print(#function)
        }
        
        resultHandler?(isInterstitialPresented)
    }
}
