//
//  FileManagerViewController.swift
//  Browser
//
//  Created by lin on 02/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import Alamofire

class FileManagerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var storageLabel: UILabel!
    @IBOutlet weak var storageProgress: UIProgressView!
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var editBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.editBarButtonItem.title = __("编辑")
        
        self.segmented.setTitle(__("正在下载"), forSegmentAt: 0)
        self.segmented.setTitle(__("下载完成"), forSegmentAt: 1)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        self.storageLabel.text = String(format: "%.1fG/%.1fG", FileManagerHelper.share.freeStorage(), FileManagerHelper.share.allStorage())
        self.storageProgress.progress = FileManagerHelper.share.usedRatio()
        
        FileManagerHelper.share.initFileProperty()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateDownloadProgressNotificationHandler(notification:))
            , name: kUpdateDownloadProgressNotification,
              object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(cancelDownloadNotificationHandler(notification:)),
            name: kCancelDownloadProgressNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(completeDownloadNotificationHandler(notification:)),
            name: kCompletlyDownloadProgressNotification,
            object: nil
        )
//        NotificationCenter.default.addObserver(self, selector: #selector(FileManagerViewController.cancelDownloadNotificationHandler(notification:)), name: Notification.Name.Task.DidResume, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(FileManagerViewController.cancelDownloadNotificationHandler(notification:)), name: Notification.Name.Task.DidSuspend, object: nil)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - TableView delegate & data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmented.selectedSegmentIndex == 0 {
            return kDownloadManager.tasks.count
        } else {
            return FileManagerHelper.share.downloadedFiles.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifiy = self.segmented.selectedSegmentIndex == 0 ? "DownloadingCell" : "DownloadedCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifiy, for: indexPath) as! DownloadFileTableViewCell
        
        if self.segmented.selectedSegmentIndex == 0 {
            let task = DownloadManager.share.tasks[indexPath.row]
            
            cell.downloadingFileNameLabel.text = task.model.fileName.removingPercentEncoding!
            cell.downloadingProgressView.progress = task.model.progress
            cell.downloadingProgressLabel.text = String(format: "\(__("已下载")): %0.2f%%", task.model.progress * 100)
        } else {
            let file = FileManagerHelper.share.downloadedFiles[indexPath.row]
            
            cell.downloadedFileNameLabel.text = file.name
            cell.downloadedFileSizeLabel.text = "\(DownloadUtility.calculateFileSizeInUnit(file.size))\(DownloadUtility.calculateUnit(file.size))"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let cancelAction = UITableViewRowAction(style: .destructive, title: __("取消")) { (action, indexPath) in
            DownloadManager.share.cancelDownload(with: DownloadManager.share.tasks[indexPath.row].request)
        }
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: __("删除"))  { (action, indexPath) in
            let file = FileManagerHelper.share.downloadedFiles[indexPath.row]
            
            do {
                try FileManager.default.removeItem(at: file.path)
                FileManagerHelper.share.downloadedFiles.remove(at: indexPath.row)
                
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                
            } catch {
                debugPrint(error)
            }
        }
        
        if self.segmented.selectedSegmentIndex == 0 {
            return [cancelAction]
        } else {
            return [deleteAction]
        }
    }

    
    // MARK Action
    
    @IBAction func segmentValueChangeAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            FileManagerHelper.share.initFileProperty()
        }
        self.tableView.reloadData()
    }
    
    
    // MARK: - Notification Handler
    
    func updateDownloadProgressNotificationHandler(notification: Notification) {
        if self.segmented.selectedSegmentIndex == 0 {
            let model = notification.object as! DownloadModel
            if let index = DownloadManager.share.tasks.index(where: { $0.model == model }) {
                let indexPath = IndexPath(row: index, section: 0)
                
                let cell = self.tableView.cellForRow(at: indexPath) as! DownloadFileTableViewCell
                
                DispatchQueue.main.async {
                    cell.downloadingProgressView.progress = model.progress
                    cell.downloadingProgressLabel.text = String(format: "\(__("已下载")): %0.2f%%", model.progress * 100)
                }
            }
        }
    }
    
    func cancelDownloadNotificationHandler(notification: Notification) {
        let model = notification.object as! DownloadModel
        
        if let index = DownloadManager.share.tasks.index(where: { $0.model == model }) {
            DispatchQueue.main.async {
                DownloadManager.share.tasks.remove(at: index)
                self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
        }
    }
    
    func completeDownloadNotificationHandler(notification: Notification) {
        let model = notification.object as! DownloadModel
        
        if let index = DownloadManager.share.tasks.index(where: { $0.model == model }) {
            DispatchQueue.main.async {
                print(#function)
                DownloadManager.share.tasks.remove(at: index)
                self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            }
        }
    }
    
    @IBAction func editBarButtonItemAction(_ sender: UIBarButtonItem) {
        if self.tableView.isEditing {
            self.editBarButtonItem.title = __("编辑")
            self.tableView.setEditing(false, animated: true)
        } else {
            self.tableView.setEditing(true, animated: true)
            self.editBarButtonItem.title = __("完成")
        }
    }
    
    
//    func suspendDownloadNotificationHandler(notification: Notification) {
//        let request = notification.object as! DownloadRequest
//        
//        if let index = DownloadManager.share.tasks.index(where: { $0.request.request == request.request }) {
//            let _ = IndexPath(row: index, section: 0)
//        }
//    }
//    
//    func resumeDownloadNotificationHandler(notification: Notification) {
//        
//    }
    
}
