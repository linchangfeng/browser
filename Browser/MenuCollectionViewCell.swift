//
//  MenuCollectionViewCell.swift
//  Browser
//
//  Created by lin on 17/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    var button: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        self.button = UIButton()
        
        self.contentView.addSubview(self.button)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        if self.button != nil {
            self.button.removeTarget(nil, action: Selector(self.button.actions(forTarget: nil, forControlEvent: .touchUpInside)!.first!), for: .touchUpInside)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.button.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
    }
}
