//
//  PasscodeButtonCell.swift
//  Account
//
//  Created by Tracy on 16/4/4.
//  Copyright © 2016年 Tina. All rights reserved.
//

import UIKit

class PasscodeButtonCell: UICollectionViewCell {
    
    let titleLabel = UILabel()
    let imageView = UIImageView()
    
    var backgroundHighlightColor: UIColor? {
        didSet {
            self.backgroundView?.backgroundColor = self.backgroundHighlightColor
        }
    }
    
    var backgroundViewSize: CGSize = CGSize(width: 40, height: 40) {
        didSet {
            self.backgroundView?.size = self.backgroundViewSize
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.highlightStateDidChangWithAnimation(animated: true)
        }
    }
    
    //
    private let highlightedBackgroundView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundView = UIView()
        self.backgroundView?.alpha = 0.0
        self.backgroundView?.size = self.backgroundViewSize
        self.backgroundView?.layer.cornerRadius = self.backgroundViewSize.width / 2
        
        self.addSubview(self.titleLabel)
        self.titleLabel.textAlignment = NSTextAlignment.center
        
        self.titleLabel.textColor = UIColor.init(hex: 0xffffff)
        
        self.addSubview(self.imageView)
        self.imageView.contentMode = UIViewContentMode.center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundView?.moveToCenterOfSuperview()
        
        self.titleLabel.frame = self.bounds
        self.imageView.frame = self.bounds
    }
    
    // MARK: - - Private
    func setTitleColorAndImageColor(color: UIColor) {
        self.titleLabel.textColor = color
        let image = self.imageView.image
        if image != nil {
            self.imageView.image = kUtil.tintImage(image, with: color)
        }
    }
    
    func highlightStateDidChangWithAnimation(animated: Bool) {
        if animated == false {
            self.backgroundView?.alpha = self.isHighlighted ? 1.0 : 0.0
        }
        else {
            self.backgroundView?.layer.removeAllAnimations()
            
            if self.isHighlighted {
                UIView.animate(withDuration: 0.2, delay: 0, options: .allowUserInteraction, animations: {
                    self.backgroundView?.alpha = 1.0
                    self.backgroundView?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                    }, completion: nil)
            }
            else {
                self.backgroundView?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                UIView.animate(withDuration: 0.2, delay: 0, options: .allowUserInteraction, animations: {
                    self.backgroundView?.alpha = 0
                    self.backgroundView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                    }, completion: nil)
            }
        }
    }
    
}
