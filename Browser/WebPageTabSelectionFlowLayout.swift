//
//  WebPageTabSelectionFlowLayout.swift
//  Browser
//
//  Created by lin on 24/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class WebPageTabSelectionFlowLayout: UICollectionViewFlowLayout {
    
    private var scaleTransform = CGAffineTransform(scaleX: kPageScale, y: kPageScale)
    
    override init() {
        super.init()
        
        self.minimumLineSpacing = 1
        self.minimumInteritemSpacing = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if let attribtues = self.layoutAttributesForItem(at: itemIndexPath) {
            
            return attribtues
        }
        return nil
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var attributes: Array<UICollectionViewLayoutAttributes> = []
        
        if self.collectionView!.numberOfSections > 0 {
            for i in 0..<self.collectionView!.numberOfItems(inSection: 0) {
                let indexPath = IndexPath(item: i, section: 0)
                attributes.append(self.layoutAttributesForItem(at: indexPath)!)
            }
        }
        
        return attributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if let attribute = super.layoutAttributesForItem(at: indexPath) {
            let newAttribute = attribute.copy() as! UICollectionViewLayoutAttributes
            
            newAttribute.transform = self.scaleTransform
            let originCenterX = newAttribute.center.x
            
            if indexPath.item != 0 {
                newAttribute.center.x = originCenterX - ((1 - kPageScale - 0.05) * CGFloat(indexPath.item) * self.collectionView!.width)
            }
            
            return newAttribute
        }
        
        return nil
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if let attribute = super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath) {
            
            let newAttribute = attribute.copy() as! UICollectionViewLayoutAttributes
            
            if let cell = self.collectionView?.cellForItem(at: itemIndexPath) as? EntranceCollectionViewCell {
                
                newAttribute.center = cell.center
                
                return newAttribute
            }
        }
        
        return nil
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        let oldBounds = self.collectionView!.bounds
        
        if __CGSizeEqualToSize(oldBounds.size, newBounds.size) {
            return true
        }
        
        return false
    }
    
    override func prepare() {
        super.prepare()
    }
    
    override func prepareForTransition(from oldLayout: UICollectionViewLayout) {
        super.prepareForTransition(from: oldLayout)
    }
    
    override var collectionViewContentSize: CGSize {
        set {
            self.collectionViewContentSize = CGSize(width: self.collecViewContentWidth(), height: self.collectionView!.height)
        }
        
        get {
            return CGSize(width: self.collecViewContentWidth(), height: self.collectionView!.height)
        }
    }
    
    func collecViewContentWidth() -> CGFloat {
        let A = self.collectionView!.width
        let B = self.collectionView!.numberOfItems(inSection: 0)
        let C = kPageScale
        let D = CGFloat(B) * CGFloat(C + 0.05)
        let newContentWidth = A * (D + 1 - 0.05 - C)
        
        return newContentWidth
    }
}
