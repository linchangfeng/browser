//
//  Constants.swift
//  Browser
//
//  Created by lin on 08/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

let kBaseURL = "http://browser.tracycool.com"

let kcb = "jQuery1102006613994323603412_1477980877586"
let kHintUrl = "https://sp0.baidu.com/5a1Fazu8AA54nxGko9WTAnF6hhy/su?json=1&cb=\(kcb)&wd="
let kMaxTryRequest = 5
let kMaxFrequentlyVisit = 5
let kDownloadManager = DownloadManager.share

let kDocumentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
let kLibiraryURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first

let kFileURL = kLibiraryURL!.appendingPathComponent("file")
let kDownloadingURL = kFileURL.appendingPathComponent("downloading")
let kDownloadedURL = kFileURL.appendingPathComponent("downloaded")

// User defaults
let kUDKeyHasUserRated = "Global.1"
let kPrivateMode = "PrivateMode"
let kNoGrapeMode = "NoGrapheMode"
let kNightShift = "NightShift"
let kFullScreen = "FullScreen"
let kGrayLevel = "GrayLevel"
let kNotFirstLaunch = "NotFirstLaunch"
let kUAIdentify = "UAIdentify"
let kCurrentUA = "currentUA"

let currentVersion = UIDevice.current.systemVersion.replacingOccurrences(of: ".", with: "_")
let iPadUA = "(iPad; CPU OS \(currentVersion) like Mac OS X)"
let iPhoneUA = "(iPhone; CPU iPhone OS \(currentVersion) like Mac OS X)"
let macOSUA = "(Macintosh; Intel Mac OS X 10_12)"
let AndroidUA = "(Linux; Android 7.0)"

let kPageScale: CGFloat = 0.6
let kVelocityToShowToolBar: CGFloat = 1000

let kMenuIconInfo = [
    (icon: "AddBookmark", title: __("添加书签"), name: "AddBookmark"),
    (icon: "History", title: __("书签/历史"), name: "History"),
    (icon: "Refresh", title: __("刷新"), name: "Refresh"),
    (icon: "Share", title: __("分享"), name: "Share"),
    (icon: "NightShift", title: __("夜间模式"), name: kNightShift),
    (icon: "Traceless", title: __("无痕模式"), name: kPrivateMode),
//    (icon: "NoImages", title: __("无图模式"), name: kNoGrapeMode),
    (icon: "FullScreen", title: __("全屏模式"), name: kFullScreen),
    (icon: "WebPageCut", title: __("网页截图"), name: "WebPageCut"),
    (icon: "File", title: __("文件管理"), name: "FileManager"),
    (icon: "Clear", title: __("清理缓存"), name: "ClearCache"),
    (icon: "Setting", title: __("设置"), name: "Setting")
]

let kDownloadableContentType: [String] = [
    "application/vnd.android.package-archive",
    "application/x-rar-compressed",
    "application/rar",
    "application/octet-stream",
    "application/zip",
    "application/x-zip-compressed",
    "application/x-msdownload",
    "application/x-bittorrent"
]

let kSwitchBackButtonEnableNotification = NSNotification.Name(rawValue: "switchBackButtonEnableNotification")
let kSwitchForwardButtonEnableNotification = NSNotification.Name(rawValue: "switchForwardButtonEnableNotification")
let kWebViewOpenBlankNotification = NSNotification.Name(rawValue: "webViewOpenBlankNotification")
let kHideToolBarNotification = NSNotification.Name(rawValue: "hideToolBarNotification")
let kShowToolBarNotification = NSNotification.Name(rawValue: "ShowToolBarNotification")
let kClearSearchBarNotification = NSNotification.Name(rawValue: "ClearSearchBarNotification")
let kUpdateDownloadProgressNotification = NSNotification.Name(rawValue: "UpdateDownloadProgressNotification")
let kCompletlyDownloadProgressNotification = NSNotification.Name(rawValue: "CompletlyDownloadProgressNotification")
let kCancelDownloadProgressNotification = NSNotification.Name(rawValue: "CancelDownloadProgressNotification")

let kMenuButtonTitleColor = UIColor(colorLiteralRed: 93/255, green: 96/255, blue: 99/255, alpha: 1.0)

let kDefaultSearchEngine = "https://www.baidu.com/s?wd="

// 在线参数名称
let kParamNameShowsBanner = "a_1"
let kParamNameLaunchIntervalToShowInterstitial = "a_2"
let kParamNameSearchIntervalToShowInterstitial = "a_3"

let kParamNameShowsRatingAlert = "b_1"
let kParamNameRatingTitle = "b_2"
let kParamNameRatingMessage = "b_3"
let kParamNameRatingCancelText = "b_4"
let kParamNameRatingConfirmTitle = "b_5"

#if TARGET_BROWSER
let kBannerUnitID = "ca-app-pub-6862632589750009/6072208176"
let kInterstitialUnitID = "ca-app-pub-6862632589750009/7548941375"
    
let kAppID = "1170327624"
let kUmengAppKey = "581c1075e88bad313c002373"
let kParamsAppKey = "Browser"

#elseif TARGET_BROWSER_TW
let kBannerUnitID = "ca-app-pub-6862632589750009/6711872972"
let kInterstitialUnitID = "ca-app-pub-6862632589750009/8188606175"
    
let kAppID = "1175214264"
let kUmengAppKey = "5825326b1061d26cc1004966"
let kParamsAppKey = "BrowserTw"

#elseif TARGET_BROWSER_TH
let kBannerUnitID = "ca-app-pub-6862632589750009/6072208176"
let kInterstitialUnitID = "ca-app-pub-6862632589750009/7548941375"
    
let kAppID = "1170327624"
let kUmengAppKey = "581c1075e88bad313c002373"
let kParamsAppKey = "Browser"

#elseif TARGET_BROWSER_FO
let kBannerUnitID = "ca-app-pub-6862632589750009/6072208176"
let kInterstitialUnitID = "ca-app-pub-6862632589750009/7548941375"
    
let kAppID = "1170327624"
let kUmengAppKey = "581c1075e88bad313c002373"
let kParamsAppKey = "Browser"

#elseif TARGET_BROWSER_FI
let kBannerUnitID = "ca-app-pub-6862632589750009/6072208176"
let kInterstitialUnitID = "ca-app-pub-6862632589750009/7548941375"
    
let kAppID = "1170327624"
let kUmengAppKey = "581c1075e88bad313c002373"
let kParamsAppKey = "Browser"
#endif

class Constants: NSObject {

}
