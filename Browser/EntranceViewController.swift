//
//  EntryViewController.swift
//  Browser
//
//  Created by lin on 14/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import WebKit
import MBProgressHUD

class EntranceViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var operationToolbar: UIToolbar!
    @IBOutlet weak var additionalToolbar: UIToolbar!
    @IBOutlet var operationBarButtonItems: [UIBarButtonItem]!
    
    var webView: WKWebView?
    
    private var collectionView: UICollectionView!
    private var fullScreenLayout: UICollectionViewFlowLayout!
    private var tabSelectionLayout: WebPageTabSelectionFlowLayout!
    
    @IBOutlet weak var doneButtonItem: UIBarButtonItem!
    @IBOutlet weak var closeAllTabsButtonItem: UIBarButtonItem!
    
    private var menuView: MenuView!
    
    private let sunImageView = UIImageView(image: UIImage(named: "Sun"))
    private let nightImageView = UIImageView(image: UIImage(named: "Moon"))
    
    var isAnimating = false
    private var isShowingMenu = false
    private var coverView = UIView()
    
    private var movingCell: EntranceCollectionViewCell!
    private var movingIndexPath: IndexPath!
    private var starMovingDistanceToCenter: CGFloat = 0.0
    
    
    private var currentIndexPath = IndexPath(item: 0, section: 0) {
        didSet {
            if let _ = self.webController, self.webController!.count > 0 {
                self.webView = self.webController?[self.currentIndexPath.item].webView
                if self.webView != nil {
                    self.operationBarButtonItems.first?.isEnabled = self.webView!.canGoBack
                    self.operationBarButtonItems[1].isEnabled = self.webView!.canGoForward
                }
            }
        }
    }
    
    private var isFullScreenLayout = true {
        didSet {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.additionalToolbar.isHidden = self.isFullScreenLayout
            })
            
            UIApplication.shared.statusBarStyle = self.isFullScreenLayout ? .default : .lightContent
            
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.collectionView.performBatchUpdates({
                UIView.animate(withDuration: 0.2, delay: 0.0, options: [.allowUserInteraction], animations: {
                    self.collectionView.setCollectionViewLayout(self.isFullScreenLayout ? self.fullScreenLayout : self.tabSelectionLayout, animated: false)
                }, completion: { (finished) in
                    if finished {
                        self.collectionView.isScrollEnabled = !self.isFullScreenLayout
                    }
                })
            }, completion: { finished in
                
                
            })
        }
    }
    
    private var webController: Array<WebPageViewController>? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        if self.view.width > self.view.height {
            return true
        }
        
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.closeAllTabsButtonItem.title = __("关闭全部")
        self.doneButtonItem.title = __("完成")
        
        self.fullScreenLayout = UICollectionViewFlowLayout()
        self.fullScreenLayout.scrollDirection = .horizontal
        
        self.tabSelectionLayout = WebPageTabSelectionFlowLayout()
        self.tabSelectionLayout.scrollDirection = .horizontal
        
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.fullScreenLayout)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(EntranceCollectionViewCell.self, forCellWithReuseIdentifier: "WebPageCell")
        self.collectionView.isScrollEnabled = !self.isFullScreenLayout
        self.collectionView.backgroundColor = UIColor.black
        
        self.configMenuView()
        
        self.coverView.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        self.coverView.alpha = 0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EntranceViewController.coverTapGesture))
        self.coverView.addGestureRecognizer(tapGesture)
        
        self.sunImageView.isHidden = true
        self.nightImageView.isHidden = true
        
        self.view.addSubview(self.collectionView)
        self.view.addSubview(self.coverView)
        self.view.addSubview(self.menuView)
        self.view.bringSubview(toFront: self.operationToolbar)
        self.view.bringSubview(toFront: self.additionalToolbar)
        
        self.view.addSubview(self.sunImageView)
        self.view.addSubview(self.nightImageView)
        
        self.initSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if UserDefaults.standard.bool(forKey: kNightShift) {
            Global.default.nightShiftCoverView.frame = UIApplication.shared.keyWindow!.frame
            UIApplication.shared.keyWindow?.addSubview(Global.default.nightShiftCoverView)
        }
        
        self.addNotification()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.removeNotification()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.collectionView.collectionViewLayout.invalidateLayout()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            UIApplication.shared.setStatusBarHidden(self.view.width > self.view.height, with: .none)
        }
        
        if self.isAnimating {
            return
        }
        
        self.hideMenu(duration: 0.01)
        
        let heightMinus: CGFloat = kParamsManager.bool(forName: kParamNameShowsBanner) ? -44 : 0
        self.collectionView.collectionViewLayout.invalidateLayout()
        self.collectionView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height + heightMinus)
        
        self.fullScreenLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.fullScreenLayout.itemSize = CGSize(width: self.collectionView.width, height: self.collectionView.height)
        
        let tabSelectionLayoutWith = self.collectionView.width * 1.0
        let tabSelectionLayoutHeight = self.collectionView.height * 1.0
        self.tabSelectionLayout.itemSize = CGSize(width: tabSelectionLayoutWith, height: tabSelectionLayoutHeight)
        self.tabSelectionLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        self.coverView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height)
        
        self.menuView.frame = CGRect(x: 0, y: self.view.height, width: self.view.width, height: self.view.height / 3)
        
        self.sunImageView.size = CGSize(width: 44, height: 44)
        self.nightImageView.size = CGSize(width: 44, height: 44)
        
        self.sunImageView.centerX = self.view.centerX
        self.sunImageView.bottom = self.view.height - 200
        
        self.nightImageView.centerX = self.view.centerX
        self.nightImageView.bottom = self.view.height - 200
        
        self.collectionView.scrollToItem(at: self.currentIndexPath, at: .centeredHorizontally, animated: false)
    }
    
    // MARK: - Collection delegate & data source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.webController?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WebPageCell", for: indexPath) as! EntranceCollectionViewCell
        
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        cell.webPageViewController = self.webController![indexPath.item]
        
        self.webViewInteractDetection()
        
        cell.panGesture.removeTarget(self, action: #selector(EntranceViewController.cellPanGesture))
        cell.panGesture.addTarget(self, action: #selector(EntranceViewController.cellPanGesture))
        cell.panGesture.delegate = self
        cell.panGesture.isEnabled = !self.isFullScreenLayout
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.currentIndexPath = indexPath
        
        self.scrollCollectionView()
        
        let delay = collectionView.numberOfItems(inSection: 0) == 1 ? 0 : 0.3
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay) {
            if self.isFullScreenLayout == false {
                self.isFullScreenLayout = true
                self.webViewInteractDetection()
            }
        }
        
        let cell = collectionView.cellForItem(at: indexPath) as! EntranceCollectionViewCell
        cell.panGesture.isEnabled = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    // ScrollView delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        var indexPath = IndexPath(item: 0, section: 0)
        
        for cell in self.collectionView.visibleCells {
            if let indexPathForCell = self.collectionView.indexPath(for: cell) {
                indexPath = indexPathForCell
            }
        }
        
        self.currentIndexPath = indexPath
    }
    
    // MARK: - Gesture handler
    
    func cellPanGesture(panGesture: UIPanGestureRecognizer) {
        
        let locationPoint = panGesture.location(in: self.collectionView)
        
        if panGesture.state == .began {
            self.movingIndexPath = self.collectionView.indexPathForItem(at: locationPoint)
            self.movingCell = self.collectionView.cellForItem(at: movingIndexPath!) as! EntranceCollectionViewCell
            
            self.starMovingDistanceToCenter = self.collectionView.centerY - locationPoint.y
        }
        
        if panGesture.state == .changed {
            self.movingCell.center.y = locationPoint.y + self.starMovingDistanceToCenter
        }
        
        if panGesture.state == .ended {
            
            if self.movingCell.centerY >= self.collectionView.halfHeight+50 || self.movingCell.centerY <= self.collectionView.halfHeight-50 {
                self.deleteCell(at: self.movingIndexPath)
            } else {
                UIView.animate(withDuration: 0.35, animations: {
                    self.movingCell.centerY = self.collectionView.centerY
                })
            }
        }
        
        if panGesture.state == .cancelled {
            panGesture.isEnabled = true
            print(#function, "cancelled")
        }
    }
    
    func deleteCell(at indexPath: IndexPath) {
        self.collectionView.performBatchUpdates({
            
            self.webController?.remove(at: indexPath.item)
            self.collectionView.deleteItems(at: [indexPath])
            
        }, completion: { (finished) in
            if finished {
                self.updateCurrentIndexPathAfterDeleteItems(deletedIndexPath: indexPath)
            }
        })
        
    }
    
    // MARK: - Gesture delegate
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer is UIPanGestureRecognizer {
            let panGesture = gestureRecognizer as! UIPanGestureRecognizer
            let view = panGesture.view
            let velocy = panGesture.velocity(in: view)
            
            return fabs(Double(velocy.y)) > fabs(Double(velocy.x))
        }
        
        return false
    }
    
    func coverTapGesture(tapGesture: UITapGestureRecognizer) {
        self.hideMenu(duration: 0.25)
    }
    
    // MARK: - Button Action
    
    @IBAction func backwardAction(_ sender: UIBarButtonItem) {
        print(#function, self.webView?.canGoBack)
        if let webView = self.webView, webView.canGoBack {
            webView.goBack()
        }
    }
    
    @IBAction func forwardAction(_ sender: UIBarButtonItem) {
        if let webView = self.webView, webView.canGoForward {
            webView.goForward()
        }
    }
    
    @IBAction func menuAction(_ sender: UIBarButtonItem) {
        self.isShowingMenu ? self.hideMenu(duration: 0.25) : self.showMenu(duration: 0.25)
    }
    
    
    @IBAction func homeAction(_ sender: UIBarButtonItem) {
        
        NotificationCenter.default.post(name: kClearSearchBarNotification, object: nil)
        
        if let webView = self.webView {
            if let url = URL(string: Global.default.homePage()) {
                let request = URLRequest(url: url)
                webView.load(request)
            }
        }
    }
    
    @IBAction func pageAction(_ sender: UIBarButtonItem) {
        if self.webController != nil {
            self.webController![self.currentIndexPath.item].hideHintVisualEffectView(withDuration: 0.01)
        }
        
        self.isFullScreenLayout = false
        
        for i in 0..<self.collectionView.numberOfItems(inSection: 0) {
            if let cell = self.collectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? EntranceCollectionViewCell {
                cell.panGesture.isEnabled = true
            }
        }
        
        self.webViewInteractDetection()
        
        self.collectionView.scrollToItem(at: self.currentIndexPath, at: .centeredHorizontally, animated: false)
    }
    
    @IBAction func closAllTab(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let closeAction = UIAlertAction(title: __("关闭全部标签"), style: .destructive) { (_) in
            
            self.webController?.removeAll()
            self.collectionView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25, execute: {
                self.webController?.append(self.newWebController())
                self.collectionView.reloadData()
                self.currentIndexPath = IndexPath(item: 0, section: 0)
                self.scrollCollectionView()
                self.isFullScreenLayout = true
            })
            
        }
        let cancelAction = UIAlertAction(title: __("取消"), style: .cancel, handler: nil)
        
        alertController.addAction(closeAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func addNewTab(_ sender: UIBarButtonItem) {
        self.addNewTab()
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        self.isFullScreenLayout = true
        self.webViewInteractDetection()
    }
    
    // MARK: - Menu button action
    
    func addBookmarkAction() {
        self.hideMenu(duration: 0.01)
        
        let bookAdditionControllerView = storyboard?.instantiateViewController(withIdentifier: "BookmarkAdditionTableViewController") as! BookmarkAdditionTableViewController
        
        if let webView = self.webView {
            bookAdditionControllerView.bookmark = (webView.title, webView.url?.absoluteString)
            bookAdditionControllerView.isNew = true
        }
        self.navigationController?.pushViewController(bookAdditionControllerView, animated: true)
    }
    
    func bookmarksAndHistoriesAction() {
        self.hideMenu(duration: 0.01)
        
        let bookmarkAndHistoryControllerView = storyboard?.instantiateViewController(withIdentifier: "BookmarkAndHistoryViewController") as! BookmarkAndHistoryViewController

        bookmarkAndHistoryControllerView.selectedCallback = { (urlString) in
            
            _ = self.navigationController?.popViewController(animated: true)
            
            if let urlString = urlString, let _ = self.webView {
                if let url = URL(string: urlString) {
                    let request = URLRequest(url: url)
                    self.webView!.load(request)
                }
            }
        }
        
        self.navigationController?.pushViewController(bookmarkAndHistoryControllerView, animated: true)
    }
    
    func refreshAction() {
        if let webView = self.webView {
            webView.reload()
        }
        
        self.hideMenu(duration: 0.01)
    }
    
    func shareAction() {
        self.hideMenu(duration: 0.01)
        
        let string = "Browser"
        let url = URL(string: Global.default.homePage())
        let image = UIImage(named: "Browser")
        
        let activityController = UIActivityViewController(activityItems: [image!, string, url!], applicationActivities: [])
        activityController.excludedActivityTypes = [.assignToContact, .print]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let wppc = activityController.popoverPresentationController {
                wppc.sourceView = self.view
                wppc.barButtonItem = self.operationBarButtonItems[2]
            }
        }
        
        activityController.completionWithItemsHandler = {
            activityType, completed, returnedItems, error in
            
            print("share: \(completed)")
        }
        
        self.present(activityController, animated: true, completion: nil)
    }
    
    func nightShiftAction() {
        print(#function)
        self.hideMenu(duration: 0.01)
        
        let isNightShift = !UserDefaults.standard.bool(forKey: kNightShift)
        
        UserDefaults.standard.set(isNightShift, forKey: kNightShift)
        
        if isNightShift {
            
            Global.default.nightShiftCoverView.frame = UIApplication.shared.keyWindow!.frame
            UIApplication.shared.keyWindow?.addSubview(Global.default.nightShiftCoverView)
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = .text
            hud.label.text = "\(__("夜间模式")): \(__("开启"))"
            hud.hide(animated: true, afterDelay: 1)
        } else {
            Global.default.nightShiftCoverView.removeFromSuperview()
        }
        
        self.menuView.buttons[kNightShift]?.isSelected = UserDefaults.standard.bool(forKey: kNightShift)
        self.menuView.collectionView.reloadItems(at: [self.menuView.collectionViewInfo[kNightShift]!])
    }
    
    func privateModeAction() {
        print(#function)
        self.switchMode(withKey: kPrivateMode, openHint: "已开启无痕模式", closeHint: "已关闭无痕模式")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PrivateModeChanged"), object: nil)
    }
    
    func noGrapeModeAction() {
        print(#function)
        self.switchMode(withKey: kNoGrapeMode, openHint: "已开启无图模式", closeHint: "已关闭无图模式")
    }
    
    func fullScreenModeAction() {
        print(#function)
        self.switchMode(withKey: kFullScreen, openHint: "已开启全屏模式", closeHint: "已关闭全屏模式")
    }
    
    func webScreenShotAction() {
        self.hideMenu(duration: 0.01)
        
        if let webView = self.webView {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.animationType = .zoomOut
            hud.mode = .indeterminate
            hud.label.text = __("网页截图")
            
            webView.fullScreenshot({ (image) in
                if let image = image {
                    hud.mode = .text
                    hud.label.text = __("完成")
                    hud.hide(animated: true, afterDelay: 0.5)
                    self.perform(#selector(EntranceViewController.showActivityController), with: [image], afterDelay: 0.5)
                }
            })
        }
    }
    
    func fileManagerAction() {
        
        self.hideMenu(duration: 0.01)
        
        let fileManagerController = self.storyboard?.instantiateViewController(withIdentifier: "FileManagerViewController") as! FileManagerViewController
        self.navigationController?.pushViewController(fileManagerController, animated: true)
    }
    
    func clearCashAction() {
        
        self.hideMenu(duration: 0.01)
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.animationType = .zoomIn
        hud.mode = .indeterminate
        hud.label.text = __("清理缓存")
        
        if #available(iOS 9.0, *) {
            let dataTypes = WKWebsiteDataStore.allWebsiteDataTypes()
            let dateFrom = Date(timeIntervalSince1970: 0)
            WKWebsiteDataStore.default().removeData(ofTypes: dataTypes, modifiedSince: dateFrom, completionHandler: {
                hud.hide(animated: true, afterDelay: 1)
            })
        }
        else {
            URLCache.shared.removeAllCachedResponses()
            hud.hide(animated: true, afterDelay: 1)
        }
    }
    
    func settingAction() {
        
        self.hideMenu(duration: 0.01)
        
        let settingController = storyboard?.instantiateViewController(withIdentifier: "SettingTableViewController") as! SettingTableViewController
        
        self.navigationController?.pushViewController(settingController, animated: true)
    }
    
    // MARK: - Private
    
    func initSource() {
        
        self.webController = [self.newWebController()]
        self.webView = self.webController?.first?.webView
        print(#function, self.webView)
    }
    
    func newWebController(withURL url: URL? = nil) -> WebPageViewController {
        let webPageViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebPageViewController") as! WebPageViewController
        
        if url != nil {
            webPageViewController.defaultURL = url
        }
        
        return webPageViewController
    }
    
    func webViewInteractDetection() {
        if self.isFullScreenLayout {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "WebViewInteractEnable"), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "WebViewInteractDisable"), object: nil)
        }
    }
    
    func updateCurrentIndexPathAfterDeleteItems(deletedIndexPath: IndexPath) {
        if deletedIndexPath.item == 0 {
            
            if self.webController!.count == 0 {
                self.webController?.append(self.newWebController())
                self.isFullScreenLayout = true
                self.currentIndexPath = IndexPath(item: 0, section: 0)
                self.scrollCollectionView()
            } else {
                self.currentIndexPath = deletedIndexPath
            }
            
        } else if deletedIndexPath.item == self.webController!.count {
            self.currentIndexPath = IndexPath(item: deletedIndexPath.item - 1, section: 0)
            self.scrollCollectionView()
        }
    }
    
    func showMenu(duration: Float) {
        
        self.isAnimating = true
        self.menuView.alpha = 1.0
        
        for buttonItem in self.operationBarButtonItems {
            let index = self.operationBarButtonItems.index(of: buttonItem)
            
            if index != 2 {
                buttonItem.isEnabled = false
                buttonItem.tintColor = UIColor.clear
            } else {
                buttonItem.tintColor = self.view.tintColor
            }
        }
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0.0, usingSpringWithDamping: 5, initialSpringVelocity: 8, options: [.allowUserInteraction], animations: {
            print(#function, "aaaa")
            self.coverView.alpha = 1.0
            self.menuView.y = self.view.height - self.menuView.height - 44
        }, completion: { (finished) in
            if finished {
                self.isShowingMenu = true
                self.isAnimating = false
            }
        })
    }
    
    func hideMenu(duration: Float) {
        
        self.isAnimating = true
        
        for buttonItem in self.operationBarButtonItems {
            buttonItem.tintColor = kMenuButtonTitleColor
            
            let index = self.operationBarButtonItems.index(of: buttonItem)
            
            if index == 0  {
                buttonItem.isEnabled = self.webView!.canGoBack
            } else if index == 1 {
                buttonItem.isEnabled = self.webView!.canGoForward
            } else {
                buttonItem.isEnabled = true
            }
        }
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0.0, usingSpringWithDamping: 5, initialSpringVelocity: 8, options: [.allowUserInteraction], animations: {
            self.menuView.y = self.view.height
            self.coverView.alpha = 0
        }, completion: { (finished) in
            if finished {
                self.isShowingMenu = false
                self.isAnimating = false
                self.menuView.alpha = 0.0
            }
        })
    }
    
    func showActivityController(withItems items: [Any]) {
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: [])
        activityController.excludedActivityTypes = [.assignToContact, .print]
        
        activityController.completionWithItemsHandler = {
            activityType, completed, returnedItems, error in
            
            print("share: \(completed)")
        }
        
        self.present(activityController, animated: true, completion: nil)
    }
    
    func switchMode(withKey key: String, openHint: String, closeHint: String) {
        self.hideMenu(duration: 0.01)
        
        let mode = !UserDefaults.standard.bool(forKey: key)
        
        UserDefaults.standard.set(mode, forKey: key)
        
        self.menuView.buttons[key]?.isSelected = mode
        self.menuView.collectionView.reloadItems(at: [self.menuView.collectionViewInfo[key]!])
    }
    
    func scrollCollectionView(completion: ((_ finished: Bool) -> Void)? = nil) {
        if self.collectionView.numberOfItems(inSection: 0) != 0 {
            
            UIView.animate(withDuration: 0.35, animations: {
                self.collectionView.scrollToItem(at: self.currentIndexPath, at: .centeredHorizontally, animated: false)
            }, completion: { (finished) in
                if completion != nil {
                    completion!(finished)
                }
            })
        }
    }
    
    func addNewTab(loadURL url: URL? = nil) {
        if let _ = self.webController {
            self.webController!.append(self.newWebController(withURL: url))
        } else {
            self.webController = [self.newWebController(withURL: url)]
        }
        
        let indexPath = IndexPath(item: self.webController!.count - 1, section: 0)
        self.collectionView.reloadItems(at: [indexPath])
        self.currentIndexPath = indexPath
        
        self.scrollCollectionView(completion: { finished in
            if finished {
                self.isFullScreenLayout = true
            }
        })
    }
    
    func addNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(EntranceViewController.switchBackButtonEnableNotification),
            name: kSwitchBackButtonEnableNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(EntranceViewController.switchForwardButtonEnableNotification),
            name: kSwitchForwardButtonEnableNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(EntranceViewController.webViewOpenBlankNotificationHandler),
            name: kWebViewOpenBlankNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(EntranceViewController.hideToolBarNotificationHandler),
            name: kHideToolBarNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(EntranceViewController.showToolBarNotificationHandler),
            name: kShowToolBarNotification,
            object: nil
        )
    }
    
    func removeNotification() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "switchBackButtonEnableNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "switchForwardButtonEnableNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: kWebViewOpenBlankNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: kHideToolBarNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: kShowToolBarNotification, object: nil)
    }
    
    func configMenuView() {
        self.menuView = MenuView(actions: [
            #selector(EntranceViewController.addBookmarkAction),
            #selector(EntranceViewController.bookmarksAndHistoriesAction),
            #selector(EntranceViewController.refreshAction),
            #selector(EntranceViewController.shareAction),
            #selector(EntranceViewController.nightShiftAction),
            #selector(EntranceViewController.privateModeAction),
//            #selector(EntranceViewController.noGrapeModeAction),
            #selector(EntranceViewController.fullScreenModeAction),
            #selector(EntranceViewController.webScreenShotAction),
            #selector(EntranceViewController.fileManagerAction),
            #selector(EntranceViewController.clearCashAction),
            #selector(EntranceViewController.settingAction)
            ]
        )
        
        self.menuView.buttons[kNightShift]?.isSelected = UserDefaults.standard.bool(forKey: kNightShift)
        self.menuView.buttons[kPrivateMode]?.isSelected = UserDefaults.standard.bool(forKey: kPrivateMode)
        self.menuView.buttons[kNoGrapeMode]?.isSelected = UserDefaults.standard.bool(forKey: kNoGrapeMode)
        self.menuView.buttons[kFullScreen]?.isSelected = UserDefaults.standard.bool(forKey: kFullScreen)
        self.menuView.collectionView.reloadData()
        self.menuView.alpha = 0.0
    }
    
    // MARK: - Notification Handler
    func switchBackButtonEnableNotification(notification: Notification) {
        print(#function, webView?.canGoBack)
        if let canGoBack = notification.object as? Bool {
            self.operationBarButtonItems.first?.isEnabled = canGoBack
        }
    }
    
    func switchForwardButtonEnableNotification(notification: Notification) {
        if let canForward = notification.object as? Bool {
            self.operationBarButtonItems[1].isEnabled = canForward
        }
    }
    
    func webViewOpenBlankNotificationHandler(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let url = userInfo["url"] as? URL {
                
                self.isFullScreenLayout = false
                Util.setTimeout(time: DispatchTime.now() + 0.35, execute: {
                    self.addNewTab(loadURL: url)
                })
            }
        }
    }
    
    func hideToolBarNotificationHandler(notification: Notification) {
        UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 8, initialSpringVelocity: 8, options: [], animations: {
            self.additionalToolbar.y = self.view.height
            self.operationToolbar.y = self.view.height
        }, completion: nil)
    }
    
    func showToolBarNotificationHandler(notification: Notification) {
        UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 8, initialSpringVelocity: 8, options: [], animations: {
            self.additionalToolbar.y = self.view.height - self.additionalToolbar.height
            self.operationToolbar.y = self.view.height - self.operationToolbar.height
        }, completion: nil)
    }
}
