//
//  Global.swift
//  Browser
//
//  Created by lin on 13/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

enum UAIdentify: String {
    case iPhone = "iPhone"
    case iPad = "iPad"
    case Android = "Android"
    case computer = "电脑"
}

var kUtil: FEUtil {
    return FEUtil.sharedInstance() as! FEUtil
}

var kParamsManager: OnlineParamsManager {
    return OnlineParamsManager.sharedInstance() as! OnlineParamsManager
}

func __(_ text: String) -> String {
    return NSLocalizedString(text, tableName: "Localizations", bundle: Bundle.main, value: "", comment: "")
}

var kGlobal: Global {
    return Global.default
}

class Global: NSObject {
    static let `default` = Global()

    let nightShiftCoverView = UIView()
    
    func initNightCoverView() {
        self.nightShiftCoverView.backgroundColor = UIColor.black.withAlphaComponent(CGFloat(UserDefaults.standard.float(forKey: kGrayLevel)))
        self.nightShiftCoverView.isUserInteractionEnabled = false
    }
    
    func hasUserRated() -> Bool {
        return UserDefaults.standard.bool(forKey: kUDKeyHasUserRated)
    }
    
    func hasRate() {
        return UserDefaults.standard.set(true, forKey: kUDKeyHasUserRated)
    }
    
    func homePage() -> String {
        return kBaseURL
    }
    
    func isHomePage(url: String) -> Bool {
        if url == self.homePage() {
            return true
        }
        return false
    }
    
    func isBaseURL(urlString: String) -> Bool {
        if let url = URL(string: urlString), let host = url.host {
            if host == kBaseURL.substring(from: kBaseURL.range(of: "http://")!.upperBound) {
                return true
            }
        }
        
        return false
    }
}
