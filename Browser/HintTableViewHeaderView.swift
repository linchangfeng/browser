//
//  HintTableViewHeaderView.swift
//  Browser
//
//  Created by lin on 02/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class HintTableViewHeaderView: UIView {
    
    var titleLabel = UILabel()
    var iconImageView = UIImageView()
    var deleteButton = UIButton()

    init(withTitle title: String, icon image: UIImage, deleteAction selector: Selector) {
        super.init(frame: .zero)
        
        self.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        
        self.titleLabel.text = title
        self.titleLabel.textColor = UIColor.black
        self.titleLabel.font = UIFont.systemFont(ofSize: 16)
        
        self.iconImageView.image = kUtil.tintImage(image, with: self.tintColor)
        self.iconImageView.contentMode = .scaleAspectFit
        
        self.deleteButton.setImage(UIImage(named: "Clear"), for: .normal)
        self.deleteButton.addTarget(nil, action: selector, for: .touchUpInside)
        
        self.addSubview(self.iconImageView)
        self.addSubview(self.titleLabel)
        self.addSubview(self.deleteButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.iconImageView.frame = CGRect(x: 15, y: 0, width: 20, height: 20)
        self.iconImageView.centerY = self.halfHeight
        
        self.titleLabel.frame = CGRect(x: self.iconImageView.right + 8, y: 0, width: self.width - 86, height: self.height)
        self.titleLabel.centerY = self.iconImageView.centerY
        
        self.deleteButton.frame = CGRect(x: 0, y: 0, width: 20, height: 22)
        self.deleteButton.right = self.width - 15
        self.deleteButton.centerY = self.iconImageView.centerY
    }
}
