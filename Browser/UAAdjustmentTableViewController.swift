//
//  UAAdjustmentTableViewController.swift
//  Browser
//
//  Created by lin on 24/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import WebKit

class UAAdjustmentTableViewController: UITableViewController {

    @IBOutlet weak var iPhoneCell: UITableViewCell!
    @IBOutlet weak var iPadCell: UITableViewCell!
    @IBOutlet weak var AndroidCell: UITableViewCell!
    @IBOutlet weak var computerCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = __("UA标识")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let uaIdentify = UserDefaults.standard.string(forKey: kUAIdentify) {
            if uaIdentify == UAIdentify.iPhone.rawValue {
                self.UADidChange(iPhone: true, iPad: false, Android: false, computer: false)
            } else if uaIdentify == UAIdentify.iPad.rawValue {
                self.UADidChange(iPhone: false, iPad: true, Android: false, computer: false)
            } else if uaIdentify == UAIdentify.Android.rawValue {
                self.UADidChange(iPhone: false, iPad: false, Android: true, computer: false)
            } else {
                self.UADidChange(iPhone: false, iPad: false, Android: false, computer: true)
           }
        }
    }
    
    // TableView delegate & data source
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if let text = cell.textLabel?.text {
            cell.textLabel?.text = __(text)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            self.UADidChange(iPhone: true, iPad: false, Android: false, computer: false)
        } else if indexPath.row == 1 {
            self.UADidChange(iPhone: false, iPad: true, Android: false, computer: false)
        } else if indexPath.row == 2 {
            self.UADidChange(iPhone: false, iPad: false, Android: true, computer: false)
        } else {
            self.UADidChange(iPhone: false, iPad: false, Android: false, computer: true)
        }
    }
    
    // Private
    
    func UADidChange(iPhone: Bool, iPad: Bool, Android: Bool, computer: Bool) {
        self.iPhoneCell.accessoryType = iPhone ? .checkmark : .none
        self.iPadCell.accessoryType = iPad ? .checkmark : .none
        self.AndroidCell.accessoryType = Android ? .checkmark : .none
        self.computerCell.accessoryType = computer ? .checkmark : .none
        
        if let userAgent = self.userAgent() {
            var newUserAgent = userAgent
            
            if iPhone {
                UserDefaults.standard.set(UAIdentify.iPhone.rawValue, forKey: kUAIdentify)
                newUserAgent = Util.replaceUserAgent(with: userAgent, to: iPhoneUA)
            } else if iPad {
                UserDefaults.standard.set(UAIdentify.iPad.rawValue, forKey: kUAIdentify)
                newUserAgent = Util.replaceUserAgent(with: userAgent, to: iPadUA)
            } else if Android {
                UserDefaults.standard.set(UAIdentify.Android.rawValue, forKey: kUAIdentify)
                newUserAgent = Util.replaceUserAgent(with: userAgent, to: AndroidUA)
            } else {
                UserDefaults.standard.set(UAIdentify.computer.rawValue, forKey: kUAIdentify)
                newUserAgent = Util.replaceUserAgent(with: userAgent, to: macOSUA)
            }
            
            UserDefaults.standard.register(defaults: ["UserAgent": newUserAgent])
            UserDefaults.standard.set(newUserAgent, forKey: kCurrentUA)
        }
    }
    
    func userAgent() -> String? {
        
        let currentUA = UserDefaults.standard.object(forKey: kCurrentUA)
        
        if currentUA != nil {
            return currentUA as? String
        }
        return nil
    }
}
