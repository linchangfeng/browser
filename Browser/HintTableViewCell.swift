//
//  HintTableViewCell.swift
//  Browser
//
//  Created by lin on 01/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class HintTableViewCell: UITableViewCell {
    
    var iconImageView: UIImageView!
    var titleLabel: UILabel!
    var urlLabel: UILabel!
    var searchHintLabel: UILabel!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        self.iconImageView = UIImageView()
        self.iconImageView.contentMode = .scaleAspectFit
        
        self.titleLabel = UILabel()
        self.titleLabel.textColor = UIColor.black
        self.titleLabel.font = UIFont.systemFont(ofSize: 14.0)
        
        self.urlLabel = UILabel()
        self.urlLabel.textColor = UIColor.lightGray
        self.urlLabel.font = UIFont.systemFont(ofSize: 12.0)
        
        self.searchHintLabel = UILabel()
        self.searchHintLabel.textColor = UIColor.black
        self.searchHintLabel.font = UIFont.systemFont(ofSize: 15.0)
        
        self.contentView.addSubview(self.iconImageView)
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.urlLabel)
        self.contentView.addSubview(self.searchHintLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.iconImageView.frame = CGRect(x: 15, y: 0, width: 20, height: 20)
        self.iconImageView.centerY = self.halfHeight
        
        self.titleLabel.frame = CGRect(x: self.iconImageView.right + 8, y: 7, width: self.width - 51, height: 17)
        self.urlLabel.frame = CGRect(x: self.titleLabel.x, y: self.titleLabel.bottom + 2, width: self.titleLabel.width, height: 15)
        self.searchHintLabel.frame = CGRect(x: self.titleLabel.x, y: 0, width: self.titleLabel.width, height: 18)
        self.searchHintLabel.centerY = self.iconImageView.centerY
    }
    
    func isLink(link: Bool) {
        self.titleLabel.isHidden = !link
        self.urlLabel.isHidden = !link
        self.searchHintLabel.isHidden = link
    }
}
