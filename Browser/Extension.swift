//
//  Extension.swift
//  Browser
//
//  Created by lin on 20/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import WebKit

extension WKWebView {
    func screenCapture() -> UIImage? {
        let capturedView = self.snapshotView(afterScreenUpdates: false)
        
        var image: UIImage? = nil
        
        if capturedView != nil {
            let size = self.scrollView.contentSize
            
            UIGraphicsBeginImageContextWithOptions(size, true, 0)
            
            let rect = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y, width: size.width, height: size.height)
            
            capturedView?.drawHierarchy(in: rect, afterScreenUpdates: true)
            
            image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
        }
        
        return image
    }
    
    public func fullScreenshot (_ completionHandler: @escaping (_ capturedImage: UIImage?) -> Void) {
        
        let snapShotView = self.snapshotView(afterScreenUpdates: true)
        snapShotView?.frame = CGRect(x: self.x, y: self.y, width: (snapShotView?.width)!, height: (snapShotView?.height)!)
        snapShotView?.isUserInteractionEnabled = false
        self.superview?.addSubview(snapShotView!)
        
        let bakOffset = self.scrollView.contentOffset
        
        let page = floorf(Float(self.scrollView.contentSize.height / self.bounds.height))
        
        UIGraphicsBeginImageContextWithOptions(self.scrollView.contentSize, false, UIScreen.main.scale)
        
        self.swContentScrollPageDraw(0, maxIndex: Int(page), drawCallback: { [weak self] () -> Void in
            let strongSelf = self
            
            let capturedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // Recover
            strongSelf?.scrollView.setContentOffset(bakOffset, animated: false)
            snapShotView?.removeFromSuperview()
            
            completionHandler(capturedImage)
        })
        
    }
    
    fileprivate func swContentScrollPageDraw (_ index: Int, maxIndex: Int, drawCallback: @escaping () -> Void) {
        
        self.scrollView.setContentOffset(CGPoint(x: 0, y: CGFloat(index) * self.scrollView.height), animated: false)
        let splitFrame = CGRect(x: 0, y: CGFloat(index) * self.scrollView.height, width: self.bounds.size.width, height: self.bounds.size.height)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) { () -> Void in
            self.drawHierarchy(in: splitFrame, afterScreenUpdates: true)
            
            if index < maxIndex {
                self.swContentScrollPageDraw(index + 1, maxIndex: maxIndex, drawCallback: drawCallback)
            }else{
                drawCallback()
            }
        }
    }
}


extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat(hex>>16)/255.0, green: CGFloat( (hex&0x00FF00) >> 8 )/255.0, blue: CGFloat(hex&0x0000FF)/255.0, alpha: alpha)
    }
}
