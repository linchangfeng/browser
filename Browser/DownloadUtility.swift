//
//  DownloadUtility.swift
//  Browser
//
//  Created by lin on 04/11/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class DownloadUtility: NSObject {

    open class func calculateFileSizeInUnit(_ contentLength: Int64) -> Float {
        let dataLength : Float64 = Float64(contentLength)
        
        if dataLength >= (1024.0*1024.0*1024.0) {
            
            return Float(dataLength/(1024.0*1024.0*1024.0))
            
        } else if dataLength >= 1024.0*1024.0 {
            
            return Float(dataLength/(1024.0*1024.0))
            
        } else if dataLength >= 1024.0 {
            
            return Float(dataLength/1024.0)
            
        } else {
            
            return Float(dataLength)
        }
    }

    open class func calculateUnit(_ contentLength : Int64) -> String {
        if(contentLength >= (1024*1024*1024)) {
            return "GB"
        } else if contentLength >= (1024*1024) {
            return "MB"
        } else if contentLength >= 1024 {
            return "KB"
        } else {
            return "B"
        }
    }
}
