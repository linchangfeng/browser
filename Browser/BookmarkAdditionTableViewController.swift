//
//  BookmarkAdditionViewController.swift
//  Browser
//
//  Created by lin on 17/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit
import CoreData
import MBProgressHUD

class BookmarkAdditionTableViewController: UITableViewController {
    
    var bookmark: (title: String?, url: String?)?
    var existBookmark: Bookmark?
    
    var isNew: Bool!
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var urlTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.isNew == true ? __("添加书签") : __("书签")
        self.navigationItem.rightBarButtonItem?.title = __("保存")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if self.isNew == true {
            if let bookmark = self.bookmark {
                self.titleTextField.text = bookmark.title
                self.urlTextField.text = bookmark.url
            }
        } else {
            if let bookmark = self.existBookmark {
                self.titleTextField.text = bookmark.title
                self.urlTextField.text = bookmark.url
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    // MARK: - Button Action
    @IBAction func saveBookmark(_ sender: UIBarButtonItem) {
        
        let title = self.titleTextField.text!
        let url = self.urlTextField.text!
        
        if self.isNew == true {
            if let _ = self.bookmark {
                
                if title != "" && url != "" {
                    
                    if BookmarkModel.default.recordExist(title: title, url: url) == false {
                        var data: NSData = NSData()
                        
                        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                        hud.animationType = .zoomOut
                        hud.mode = .indeterminate
                        hud.label.text = __("保存中")
                        
                        WebSiteHelper.webSiteImageLink(withURLString: url, complete: { (iconLink) in
                            if iconLink != "" {
                                do {
                                    data = try NSData(contentsOf: URL(string: iconLink)!)
                                } catch {
                                    print(#function, error)
                                }
                            } else {
                                if let image = UIImage(named: "Browser"), let pngData = UIImagePNGRepresentation(image) {
                                    data = NSData(data: pngData)
                                }
                            }
                            
                            DispatchQueue.main.async {
                                if BookmarkModel.default.recordExist(title: title, url: url) == false {
                                    BookmarkModel.default.createNewBookmark(title: title, url: url, iconLink: iconLink, iconData: data)
                                }
                                
                                hud.mode = .text
                                hud.label.text = __("完成")
                                hud.hide(animated: true, afterDelay: 0.3)
                                hud.completionBlock = {
                                    self.navigationController!.popViewController(animated: true)
                                }
                            }
                        })
                    } else {
                        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                        hud.animationType = .zoomOut
                        hud.mode = .text
                        hud.label.text = __("完成")
                        hud.hide(animated: true, afterDelay: 0.3)
                        hud.completionBlock = {
                            self.navigationController!.popViewController(animated: true)
                        }
                    }
                }
            }
        } else {
            if let _ = self.existBookmark {
                if title != "" && url != "" {
                    
                    self.existBookmark!.title = title
                    self.existBookmark!.url = url
                    
                    BookmarkModel.default.saveContext()
                    
                    let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                    hud.animationType = .zoomOut
                    hud.mode = .text
                    hud.label.text = __("完成")
                    
                    hud.hide(animated: true, afterDelay: 1)
                } else {
                    let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                    hud.animationType = .zoomOut
                    hud.mode = .text
                    hud.label.text = __("标题或链接不能为空")
                    
                    hud.hide(animated: true, afterDelay: 1)
                }
            }
        }
    }

}
