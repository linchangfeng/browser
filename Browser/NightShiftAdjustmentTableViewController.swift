//
//  NightShiftAdjustmentTableViewController.swift
//  Browser
//
//  Created by lin on 24/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class NightShiftAdjustmentTableViewController: UITableViewController {
    
    @IBOutlet weak var graySlider: UISlider!
    @IBOutlet weak var brightnessSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = __("夜间模式")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(NightShiftAdjustmentTableViewController.screenBrightnessDidChangeHandler),
            name: .UIScreenBrightnessDidChange,
            object: nil
        )
        
        self.graySlider.value = UserDefaults.standard.float(forKey: kGrayLevel)
        self.brightnessSlider.value = Float(UIScreen.main.brightness)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .UIScreenBrightnessDidChange, object: nil)
    }
    
    // Slider Action
    
    @IBAction func graySliderValueChangedAction(_ sender: UISlider) {
        UserDefaults.standard.set(sender.value, forKey: kGrayLevel)
        
        Global.default.nightShiftCoverView.backgroundColor = UIColor.black.withAlphaComponent(CGFloat(sender.value))
    }
    
    @IBAction func brightnessSliderValueChangedAction(_ sender: UISlider) {
        UIScreen.main.brightness = CGFloat(sender.value)
    }
    
    // Notification
    
    func screenBrightnessDidChangeHandler(notification: Notification) {
        if let screen = notification.object as? UIScreen {
            self.brightnessSlider.value = Float(screen.brightness)
        }
    }
    
    // MARK: - TableViewDatasource
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let text = super.tableView(tableView, titleForHeaderInSection: section) {
            return __(text)
        }
        
        return nil
    }
}
