//
//  PasscodeField.swift
//  Account
//
//  Created by Tracy on 16/4/4.
//  Copyright © 2016年 Tina. All rights reserved.
//

import UIKit

@objc protocol PasscodeFieldDelegate: class {
    @objc optional func passcodeFieldValueChanged(passcodeField: PasscodeField)
}

class PasscodeField: UIView {

    // MARK: - - Properties
    weak var delegate: PasscodeFieldDelegate?
    
    var maxLenght = 4 {
        willSet(newMaxLength) {
            assert(newMaxLength > 0, "maxLength should > 0")
        }
        
        didSet {
            for passcodePattern in self.subviews {
                passcodePattern.removeFromSuperview()
            }
            
            for _ in 0 ..< self.maxLenght {
                let dot = self.createDot()
                self.addSubview(dot)
            }
            
            self.setNeedsLayout()
        }
    }
    
    var text = "" {
        didSet {
            self.updateDotsAniamtedly(animatedly: false)
        }
    }

    let dotWidth: CGFloat = 14
    
    // MARK: - - Override
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    func initialize() {
        self.backgroundColor = UIColor.clear
        
        let length = self.maxLenght
        self.maxLenght = length
        
        self.updateDotsAniamtedly(animatedly: false)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let centerXInterval = (self.width - self.dotWidth) / CGFloat(self.maxLenght - 1)
        for (index, passcodePattern) in self.subviews.enumerated() {
            let centerX = self.dotWidth/2.0 + CGFloat(index)*centerXInterval
            passcodePattern.center = CGPoint(x: centerX, y: self.halfHeight)
        }
    }
    
    override var tintColor: UIColor! {
        didSet {
            self.updateDotsAniamtedly(animatedly: false)
        }
    }
    
    // MARK: - - Public
    func appendCharacter(character: String) {
        if self.text.characters.count < self.maxLenght {
            self.text += character
            self.updateDotsAniamtedly(animatedly: true)
            
            self.delegate?.passcodeFieldValueChanged?(passcodeField: self)
        }
    }
    
    func deleteLastCharacter() {
        if self.text.characters.count > 0 {
            self.text = String(self.text.characters.dropLast())
            self.updateDotsAniamtedly(animatedly: true)
            
            self.delegate?.passcodeFieldValueChanged?(passcodeField: self)
        }
    }
    
    // MARK: - - Private
    func createDot() -> UIImageView {
        let dot = UIImageView()
        dot.contentMode = .scaleToFill
        dot.size = CGSize(width: self.dotWidth, height: self.dotWidth)
        dot.layer.cornerRadius = self.dotWidth / 2.0
        dot.layer.masksToBounds = true

        return dot
    }
    
    func updateDotsAniamtedly(animatedly: Bool) {
        let duration = animatedly ? 0.1 : 0.0
        UIView.animate(withDuration: duration) {
            for (index, dot) in self.subviews.enumerated() {
                let highlighted = (index < self.text.characters.count)
                if highlighted {
                    (dot as? UIImageView)?.image = kUtil.tintImage(UIImage(named: "PasscodeFull"), with: self.tintColor)
                }
                else {
                    (dot as? UIImageView)?.image = kUtil.tintImage(UIImage(named: "PasscodeEmpty"), with: self.tintColor)
                }
            }
        }
    }
}
