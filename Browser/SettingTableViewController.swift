//
//  SettingTableViewController.swift
//  Browser
//
//  Created by lin on 24/10/2016.
//  Copyright © 2016 com.Flow. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController, PasscodeViewControllerDelegate {

    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var uaIdentifyLabel: UILabel!
    
    @IBOutlet weak var passcodeCell: UITableViewCell!
    @IBOutlet weak var passcodeSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = __("设置")
        
        self.passcodeSwitch.addTarget(self, action: #selector(passcodeSwitchValueChanged(sender:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        if let infoDictionary = Bundle.main.infoDictionary {
            let version = infoDictionary["CFBundleShortVersionString"] as? String
            
            self.versionLabel.text = version
        }
        
        if let uaIdentify = UserDefaults.standard.string(forKey: kUAIdentify) {
            self.uaIdentifyLabel.text = uaIdentify
        }
        
        self.passcodeSwitch.isOn = PasscodeAssistant.default.hasPasscodeSet()
    }
    
    // MARK: - TableView delegate & data source
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if let text = cell.textLabel?.text {
            cell.textLabel?.text = __(text)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Action
    func passcodeSwitchValueChanged(sender: UISwitch) {
        if self.passcodeSwitch.isOn == false {
            PasscodeAssistant.default.cleanPasscode()
        }
        else {
            let c = PasscodeViewController(type: .Setup)
            c.delegate = self
            self.navigationController?.pushViewController(c, animated: true)
        }
    }

    // MARK: - PasscodeViewControllerDelegate
    func setupPasscodeDidSucceed(controller: PasscodeViewController) {
        _ = self.navigationController?.popToViewController(self, animated: true)
    }
}
