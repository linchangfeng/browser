//
//  PasscodeViewController.swift
//  Account
//
//  Created by Tracy on 16/4/4.
//  Copyright © 2016年 Tina. All rights reserved.
//

import UIKit
import LocalAuthentication

@objc protocol PasscodeViewControllerDelegate {
    @objc optional func setupPasscodeDidSucceed(controller: PasscodeViewController);
    
    @objc optional func validatePasscodeDidSucceed(controller: PasscodeViewController);
    @objc optional func validatePasscodeDidFail(controller: PasscodeViewController);
}

@objc enum PasscodeViewControllerType: Int {
    case Setup
    case Validation
}

@objc class PasscodeViewController: UIViewController, PasscodeFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - - Propertyies
    weak var delegate: PasscodeViewControllerDelegate?
    
    let type: PasscodeViewControllerType
    
    var primaryColor = UIColor(hex: 0x01BAFC) {
        didSet {
            self.passcodeField.tintColor = self.primaryColor
        }
    }
    
    @IBOutlet weak var passcodeField: PasscodeField!
    @IBOutlet weak var passcodeButtonCollectionView: UICollectionView!
    @IBOutlet weak var hintLabel: UILabel!
    
    @IBOutlet weak var touchIDElementsContainer: UIView!
    @IBOutlet weak var touchIDBackground: UIImageView!
    @IBOutlet weak var touchIDSwitchButton: UIButton!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var passcodeButtonInfos = [PasscodeButtonInfo]()
    var stagedPasscode: String?
    
    var separatorImageViews = [UIImageView]()
    
    var navigationBarTintColorOriginally: UIColor?
    var navigationBarTitleAttributesOriginally: [String : Any]?
    var navigationBarBackgroundImageOriginally: UIImage?
    
    let numberOfPasscodeButtonColumns = 3
    var numberOfPasscodeButtonRows: Int {
        let rows = ceil( CGFloat(self.passcodeButtonInfos.count) / CGFloat(numberOfPasscodeButtonColumns) )
        return Int(rows)
    }
    var passcodeButtonCellSize: CGSize {
        let width = floor( self.passcodeButtonCollectionView.width / CGFloat(self.numberOfPasscodeButtonColumns) )
        let heigth = floor( self.passcodeButtonCollectionView.height / CGFloat(self.numberOfPasscodeButtonRows) )
        return CGSize(width: width, height: heigth)
    }
    
    let cellIdentifier = "Cell"
    
    // Type & State
    enum SetupState {
        case Setting, Confirming, FailedAndStartSetting, Done
    }
    
    enum ValidationState {
        case Validating, FailedAndStartValidating, Done
    }
    
    // MARK: - - Override
    init(type: PasscodeViewControllerType) {
        self.type = type
        let nibName = PasscodeViewController.description().components(separatedBy: ".").last
        super.init(nibName: nibName, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.passcodeField.delegate = self
        
        self.hintLabel.textColor = UIColor.white
        self.hintLabel.font = UIFont.systemFont(ofSize: 18)
        self.hintLabel.textColor = UIColor.white
        self.hintLabel.adjustsFontSizeToFitWidth = true
        
        self.touchIDElementsContainer.backgroundColor = UIColor.clear
        self.touchIDSwitchButton.setTitle(__("使用Touch ID"), for: .normal)
        self.touchIDEnabled = false
        
        self.passcodeButtonCollectionView.register(PasscodeButtonCell.self, forCellWithReuseIdentifier: cellIdentifier)
        self.passcodeButtonCollectionView.delegate = self
        self.passcodeButtonCollectionView.dataSource = self
        self.passcodeButtonCollectionView.backgroundColor = UIColor.clear
        
        var touchIDButtonInfo: PasscodeButtonInfo = PasscodeButtonInfo(text: nil, image: nil, kind: .None)
        
        if self.type == .Setup {
            self.title = __("设置密码")
            self.setupState = .Setting
            self.touchIDElementsContainer.isHidden = (kUtil.isTouchIDAvailable() == false)
        }
        else {
            self.validationState = .Validating
            self.touchIDElementsContainer.isHidden = true
            if PasscodeAssistant.default.isTouchIDEnrolledAndEnabled() {
                touchIDButtonInfo = PasscodeButtonInfo(text: nil, image: UIImage(named: "TouchID"), kind: .TouchID)
            }
        }
        
        self.passcodeButtonInfos = [
            PasscodeButtonInfo(text: "1", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "2", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "3", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "4", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "5", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "6", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "7", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "8", image: nil, kind: .Number),
            PasscodeButtonInfo(text: "9", image: nil, kind: .Number),
            touchIDButtonInfo,
            PasscodeButtonInfo(text: "0", image: nil, kind: .Number),
            PasscodeButtonInfo(text: nil, image: kUtil.tintImage(UIImage(named: "PasscodeDelete"), with: self.view.tintColor), kind: .Delete),
        ]
        
        for _ in 0..<3 {
            let separator = UIImageView(image: UIImage(named: "PasscodeSeparator"))
            separator.contentMode = .scaleAspectFit
            self.view.addSubview(separator)
            self.separatorImageViews.append(separator)
        }
        
        let color = self.primaryColor
        self.primaryColor = color
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.navigationBarTintColorOriginally == nil {
            self.navigationBarTintColorOriginally = self.navigationController?.navigationBar.tintColor
            self.navigationBarTitleAttributesOriginally = self.navigationController?.navigationBar.titleTextAttributes
            self.navigationBarBackgroundImageOriginally = self.navigationController?.navigationBar.backgroundImage(for: .default)
        }
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.tintColor = self.navigationBarTintColorOriginally
        self.navigationController?.navigationBar.titleTextAttributes = self.navigationBarTitleAttributesOriginally
        self.navigationController?.navigationBar.setBackgroundImage(self.navigationBarBackgroundImageOriginally, for: .default)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.backgroundImageView.frame = self.view.bounds
        
        var hintLabelY, spacingBetweenHintLabelAndPasscodeField, spacingBetweenTouchIDElementsContainerAndPasscodeButtonCollectionView: CGFloat
        var passcodeButtonCollectionViewSize: CGSize
        switch kUtil.screenInch() {
        case FEScreenInch3_5:
            hintLabelY = 60
            spacingBetweenHintLabelAndPasscodeField = 20
            spacingBetweenTouchIDElementsContainerAndPasscodeButtonCollectionView = (self.touchIDElementsContainer.isHidden ? -20 : 0)
            passcodeButtonCollectionViewSize = CGSize(width: self.view.width - 30, height: 320)
        case FEScreenInch4_0:
            hintLabelY = 60
            spacingBetweenHintLabelAndPasscodeField = 20
            spacingBetweenTouchIDElementsContainerAndPasscodeButtonCollectionView = (self.touchIDElementsContainer.isHidden ? 0 : 10)
            passcodeButtonCollectionViewSize = CGSize(width: self.view.width - 30, height: 350)
        case FEScreenInch4_7:
            hintLabelY = 80
            spacingBetweenHintLabelAndPasscodeField = 40
            spacingBetweenTouchIDElementsContainerAndPasscodeButtonCollectionView = (self.touchIDElementsContainer.isHidden ? 0 : 25)
            passcodeButtonCollectionViewSize = CGSize(width: self.view.width - 50, height: 360)
        case FEScreenInch5_5:
            hintLabelY = 100
            spacingBetweenHintLabelAndPasscodeField = 40
            spacingBetweenTouchIDElementsContainerAndPasscodeButtonCollectionView = (self.touchIDElementsContainer.isHidden ? 0 : 25)
            passcodeButtonCollectionViewSize = CGSize(width: self.view.width - 50, height: 380)
        default:
            hintLabelY = 200
            spacingBetweenHintLabelAndPasscodeField = 40
            spacingBetweenTouchIDElementsContainerAndPasscodeButtonCollectionView = (self.touchIDElementsContainer.isHidden ? 0 : 100)
            passcodeButtonCollectionViewSize = CGSize(width: 360, height: 380)
        }
        
        //
        self.hintLabel.frame = CGRect(x: 0, y: hintLabelY, width: self.view.width, height: self.hintLabel.font.pointSize + 2)
        
        self.passcodeField.frame = CGRect(x: 0, y: self.hintLabel.bottom + spacingBetweenHintLabelAndPasscodeField,
                                          width: 120, height: 12)
        self.passcodeField.centerX = self.view.halfWidth
        
        // touchIDElementsContainer & it's subviews
        self.touchIDElementsContainer.frame = CGRect(x: 0, y: self.passcodeField.bottom + 20, width: 250, height: 30)
        self.touchIDElementsContainer.centerX = self.view.halfWidth
        
        self.touchIDBackground.frame = self.touchIDElementsContainer.bounds
        self.touchIDSwitchButton.frame = self.touchIDBackground.frame
        
        //
        self.passcodeButtonCollectionView.frame = CGRect(x: 0,
                                 y: self.touchIDElementsContainer.bottom + spacingBetweenTouchIDElementsContainerAndPasscodeButtonCollectionView,
                                 width: passcodeButtonCollectionViewSize.width,
                                 height: passcodeButtonCollectionViewSize.height)
        self.passcodeButtonCollectionView.centerX = self.view.halfWidth
        
        //
        for (index, separator) in self.separatorImageViews.enumerated() {
            let separatorY = self.passcodeButtonCellSize.height * CGFloat(index+1) + self.passcodeButtonCollectionView.y
            separator.frame = CGRect(x: 0, y: separatorY, width: passcodeButtonCollectionViewSize.width, height: 5)
            separator.centerX = self.view.centerX
        }
    }
    
    // MARK: - - Public
    var setupState: SetupState = .Setting {
        didSet {
            switch self.setupState {
            case .Setting:
                self.hintLabel.text = __("")
            case .Confirming:
                self.hintLabel.text = __("再次输入密码")
            case .FailedAndStartSetting:
                self.hintLabel.text = __("两个密码不一致，请重新设置")
            case .Done:
                self.hintLabel.text = ""
            }
        }
    }
    
    var validationState: ValidationState = .Validating {
        didSet {
            switch self.validationState {
            case .Validating:
                self.hintLabel.text = __("输入密码")
            case .FailedAndStartValidating:
                self.hintLabel.text = ""
            case .Done:
                self.hintLabel.text = ""
            }
        }
    }
    
    var touchIDEnabled: Bool = false {
        didSet {
            let tintColor = self.touchIDEnabled ? self.primaryColor : UIColor.init(hex: 0xffffff)
            self.touchIDSwitchButton.setTitleColor(tintColor, for: .normal)
            
            let image = UIImage(named: "TouchIDBackground")
            self.touchIDBackground.image = kUtil.tintImage(image, with: tintColor)
        }
    }
    
    func showTouchIDValidationIfNeeded() {
        if PasscodeAssistant.default.isTouchIDEnrolledAndEnabled() == false {
            return
        }
        
        let context = LAContext()
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Unlock") { (success, error) in
            kUtil.performBlock(inMainThread: { 
                if success {
                    self.delegate?.validatePasscodeDidSucceed?(controller: self)
                }
            })
        }
    }
    
    // MARK: - - Handlers
    @IBAction func touchIDSwitchButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.touchIDEnabled = !self.touchIDEnabled
        }
    }
    
    // MARK: - - UICollectionView delegate & datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.passcodeButtonInfos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.passcodeButtonCellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.passcodeButtonCollectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath as IndexPath) as! PasscodeButtonCell
        cell.titleLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 28)
        
        let info = self.passcodeButtonInfos[indexPath.item]
        cell.titleLabel.text = info.text
        cell.imageView.image = info.tintedImage
        cell.backgroundHighlightColor = info.backgroundHighlightColor
        
        cell.isUserInteractionEnabled = (info.kind != .None)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let info = self.passcodeButtonInfos[indexPath.item]
        
        switch info.kind {
        case .Number:
            self.passcodeField.appendCharacter(character: info.text!)
        case .Delete:
            self.passcodeField.deleteLastCharacter()
        case .TouchID:
            self.showTouchIDValidationIfNeeded()
        default:
            ()
        }
    }

    // MARK: - - PasscodeFieldDelegate
    func passcodeFieldValueChanged(passcodeField: PasscodeField) {
        if passcodeField.text.characters.count == passcodeField.maxLenght {
            setTimeout(0.1, self.endInputingPasscode)
        }
    }
    
    // MARK: - - Private
    func endInputingPasscode() {
        if self.type == .Setup {
            if self.setupState == .Setting || self.setupState == .FailedAndStartSetting {
                self.stagedPasscode = self.passcodeField.text
                self.passcodeField.text = ""
                self.setupState = .Confirming
            }
            else if self.setupState == .Confirming {
                if self.stagedPasscode != self.passcodeField.text {
                    self.passcodeField.text = ""
                    self.setupState = .FailedAndStartSetting
                    kUtil.vibrate()
                }
                else {
                    PasscodeAssistant.default.savePasscode(text: self.passcodeField.text, touchIDEnabled: self.touchIDEnabled)
                    self.setupState = .Done
                    self.delegate?.setupPasscodeDidSucceed?(controller: self)
                }
            }
        }
        
        else if self.type == .Validation {
            if PasscodeAssistant.default.isPasscodeMatched(text: self.passcodeField.text) {
                self.validationState = .Done
                self.delegate?.validatePasscodeDidSucceed?(controller: self)
            }
            else {
                self.shakePasscodeField(completion: {
                    self.passcodeField.text = ""
                    self.validationState = .FailedAndStartValidating
                    self.delegate?.validatePasscodeDidFail?(controller: self)
                })
            }
        }
    }
    
    func shakePasscodeField(completion: @escaping () -> Void) {
        kUtil.vibrate()
        
        let duration = 0.25
        
        let animation = CAKeyframeAnimation(keyPath: "position.x")
        animation.values = [0, 10, -10, 10, -10, 10, 0]
        animation.keyTimes = [NSNumber.init(value: 0), NSNumber.init(value: 1.0/6.0), NSNumber.init(value: 2.0/6.0), NSNumber.init(value: 3.0/6.0), NSNumber.init(value: 4.0/6.0), NSNumber.init(value: 5.0/6.0), NSNumber.init(value: 1)];
        animation.duration = duration
        animation.isAdditive = true
        self.passcodeField.layer.add(animation, forKey: nil)
        self.hintLabel.layer.add(animation, forKey: nil)
        
        setTimeout(duration) { 
            completion()
        }
    }
    
    // MARK: - - Internal class
    class PasscodeButtonInfo {
        var text: String?
        var image: UIImage?
        var kind: Kind
        
        var backgroundHighlightColor: UIColor {
            switch self.kind {
            case .None:
                return UIColor.clear
            default:
                return UIColor.init(hex: 0xffffff, alpha: 0.5)
            }
        }
        
        var tintedImage: UIImage? {
            if self.image != nil {
                return self.kind == .TouchID ? self.image : kUtil.tintImage(self.image!, with: UIColor.init(hex:0xffffff))
            }
            return nil
        }
        
        enum Kind {
            case Number, Delete, TouchID, None
        }
        
        init(text: String?, image: UIImage?, kind: Kind) {
            self.text = text
            self.image = image
            self.kind = kind
        }
    }
}
