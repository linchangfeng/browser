//
//  BrightnessView.h
//  Coolplayer
//
//  Created by 罗思成 on 16/8/29.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrightnessView : UIView

- (void)appear;

@end
