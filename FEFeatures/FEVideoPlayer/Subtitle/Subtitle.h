//
//  Subtitle.h
//  VideoDemo
//
//  Created by flowdev on 16/4/11.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subtitle : NSObject

@property (nonatomic) double startTime;
@property (nonatomic) double endTime;
@property (nonatomic, copy) NSString *text;

@end
