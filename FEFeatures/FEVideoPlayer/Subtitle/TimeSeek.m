//
//  TimeSeek.m
//  VideoDemo
//
//  Created by flowdev on 16/4/14.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import "TimeSeek.h"
#import "FEVideoPlayer.h"

@interface TimeSeek()

@property (nonatomic, strong) UILabel *diffDisplay;
@property (nonatomic, strong) UILabel *timeDisplay;

@end

@implementation TimeSeek

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.backgroundColor = FEHexRGBA(0x000000, 0.8);
        self.layer.cornerRadius = 7;
        self.layer.masksToBounds = YES;
        
        _diffDisplay = [[UILabel alloc] init];
        _diffDisplay.textColor = [UIColor whiteColor];
        _diffDisplay.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_diffDisplay];
        
        _timeDisplay = [[UILabel alloc] init];
        _timeDisplay.textColor = [UIColor whiteColor];
        _timeDisplay.textAlignment = NSTextAlignmentCenter;
        _timeDisplay.font = [UIFont systemFontOfSize:FEVideoPlayerFirstFontSize];
        [self addSubview:_timeDisplay];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat paddingTop = 20;
    
    self.diffDisplay.frame = CGRectMake(0, paddingTop, CGRectGetWidth(self.bounds), 20);
    
    self.timeDisplay.frame = CGRectMake(0, 0, self.width, 20);
    self.timeDisplay.centerY = self.halfHeight + 20;
}

- (void)setDiffText:(double)diff {
    int floorDiff = floor(diff);
    
    if (floorDiff >= 0) {
        int miniute = floorDiff / 60;
        int second = floorDiff % 60;
        
        self.diffDisplay.text = [NSString stringWithFormat:@"+%02i:%02i", miniute, second];
    } else {
        floorDiff = -floorDiff;
        int miniute = floorDiff / 60;
        int second = floorDiff % 60;
        
        self.diffDisplay.text = [NSString stringWithFormat:@"-%02i:%02i", miniute, second];
    }
}

- (void)setTimeText:(double)time duration:(double)duration {
    int floorTime = floor(time);
    int floorDuration = floor(duration);
    
    int totalHour = floorDuration / 3600;
    int totalMinute = floorDuration / 60 % 60;
    int totalSecond = floorDuration % 60;
    
    int currentHour = floorTime / 3600;
    int currentMinute = floorTime / 60 % 60;
    int currentSecond = floorTime % 60;
    
    if (totalHour > 0) {
        self.timeDisplay.text = [NSString stringWithFormat:@"%02d:%02d:%02d/%02d:%02d:%02d",
                                 currentHour, currentMinute, currentSecond,
                                 totalHour, totalMinute, totalSecond];
    } else {
        self.timeDisplay.text = [NSString stringWithFormat:@"%02d:%02d/%02d:%02d",
                                 currentMinute, currentSecond,
                                 totalMinute, totalSecond];
    }
    
}

@end
