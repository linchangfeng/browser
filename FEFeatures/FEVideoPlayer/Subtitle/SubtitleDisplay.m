//
//  SubtitleDisplay.m
//  VideoDemo
//
//  Created by flowdev on 16/4/12.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import "SubtitleDisplay.h"
#import "SubtitleParser.h"
#import "Subtitle.h"

@implementation SubtitleDisplay

- (instancetype)initWithFrame:(CGRect)frame andFilePath:(NSString *)path {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        _subtitleArray = [[SubtitleParser parseWithFilePath:path] sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            double aStart = [(Subtitle *)a startTime];
            double bStart = [(Subtitle *)b startTime];
            return aStart > bStart;
        }];
        
        _subtitleText = [[UILabel alloc] init];
        _subtitleText.backgroundColor = [UIColor clearColor];
        _subtitleText.textColor = [UIColor whiteColor];
        _subtitleText.font = [UIFont systemFontOfSize:18];
        _subtitleText.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_subtitleText];
    }
    
    return self;
}

- (void)changeFilePath:(NSString *)filePath {
    _subtitleArray = [[SubtitleParser parseWithFilePath:filePath] sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        double aStart = [(Subtitle *)a startTime];
        double bStart = [(Subtitle *)b startTime];
        return aStart > bStart;
    }];
}

- (void)layoutSubviews {
    _subtitleText.frame = self.bounds;
}

- (NSString *)findSubtitleByTime:(double)time {
    NSInteger cursor;
    
    for (cursor = 0; cursor < [self.subtitleArray count]; cursor++) {
        @autoreleasepool {
            Subtitle *current = self.subtitleArray[cursor];
            if (current.startTime < time && current.endTime > time) {
                return current.text;
            }
        }
    }
    
    return @"";
}

- (NSString *)findSubtitleByTimeFast:(double)time {
    int left = 0, right = (int)[self.subtitleArray count];
    
    while (left < right) {
        int mid = left + (right - left) / 2;
        Subtitle *current = self.subtitleArray[mid];
        
        if (current.startTime > time) {
            right = mid;
        } else {
            int find = mid - 1;
            for (; find >= 0; find--) {
                Subtitle *tmp = self.subtitleArray[find];
                if (!(tmp.startTime < time && tmp.endTime > time)) {
                    break;
                }
            }
            
            Subtitle *small = self.subtitleArray[find+1];
            if (small.startTime < time && small.endTime > time) {
                return small.text;
            }
            
            find = mid;
            for (; find < right; find++) {
                Subtitle *tmp = self.subtitleArray[find];
                if (tmp.startTime < time && tmp.endTime > time) {
                    return tmp.text;
                }
            }
            
            return @"";
        }
    }
    
    return @"";
}

- (void)displayByTime:(double)time {
    self.subtitleText.text = [self findSubtitleByTimeFast:time];
}

@end
