//
//  SubtitleParser.m
//  VideoDemo
//
//  Created by flowdev on 16/4/11.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import "SubtitleParser.h"
#import "Subtitle.h"

@implementation SubtitleParser

+ (NSNumber *)timeTextValue:(NSString *)timeText {
    timeText = [timeText stringByReplacingOccurrencesOfString:@"," withString:@":"];
    double hh = 0, mm = 0, ss = 0;
    
    for (int i = 0; i < 3; i++) {
        NSRange range = [timeText rangeOfString:@":"];
        double value = -1;
        if (range.location != NSNotFound) {
            NSString *sub = [timeText substringToIndex:range.location];
            value = [sub doubleValue];
            timeText = [timeText substringFromIndex:range.location+1];
        } else {
            return @(-1);
        }
        
        switch (i) {
            case 0:
                hh = value;
                break;
            case 1:
                mm = value;
                break;
            case 2:
                ss = value;
            default:
                break;
        }
    }
    return @(hh * 3600 + mm * 60 + ss);
}

+ (NSArray *)parseWithFilePath:(NSString *)path {
    NSMutableArray *subtitleArray = [[NSMutableArray alloc] init];
    
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    NSScanner *scanner = [NSScanner scannerWithString:content];
    while (![scanner isAtEnd]) {
        @autoreleasepool {
            NSString *indexString;
            [scanner scanUpToCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&indexString];
            NSString *startString;
            [scanner scanUpToString:@" --> " intoString:&startString];
            [scanner scanString:@"-->" intoString:nil];
            NSString *endString;
            [scanner scanUpToCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&endString];
            NSString *textString;
            [scanner scanUpToString:@"\n\n" intoString:&textString];
            textString = [textString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            textString = [textString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            textString = [textString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
//            NSLog(@"textString = %@", textString);
            NSRange rangeLeft = [textString rangeOfString:@"<"];
            NSRange rangeRight = [textString rangeOfString:@">"];
            while (rangeLeft.location != NSNotFound && rangeRight.location != NSNotFound
                   && rangeLeft.location < rangeRight.location) {
                textString = [textString stringByReplacingCharactersInRange:NSMakeRange(rangeLeft.location, rangeRight.location - rangeLeft.location + 1) withString:@""];
                rangeLeft = [textString rangeOfString:@"<"];
                rangeRight = [textString rangeOfString:@">"];
            }
            
            
            Subtitle *sub = [[Subtitle alloc] init];
            sub.startTime = [[self timeTextValue:startString] doubleValue];
            sub.endTime = [[self timeTextValue:endString] doubleValue];
            sub.text = textString;
            
            if (sub.startTime > -0.1 && sub.endTime > -0.1) {
                [subtitleArray addObject:sub];
            }
        }
    }
    
    return [subtitleArray copy];
}

@end
