//
//  VideoInfo.m
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoInfo.h"
#import "FFmpegWrapper.h"
#import <libavcodec/avcodec.h>
#import <libavformat/avformat.h>
#import <libavutil/imgutils.h>
#import <libswscale/swscale.h>
#import <Photos/Photos.h>

@implementation VideoInfo

+ (NSInteger)durationWithPath:(NSString *)path {
    // 使用系统默认的解码器
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
    
    // 获取时长
    CMTime videoDuration = asset.duration;
    double videoDurationSeconds = CMTimeGetSeconds(videoDuration);
    NSLog(@"duration=%lf", videoDurationSeconds);
    
    if (videoDurationSeconds > 0.5) {
        return (NSInteger)videoDurationSeconds;
    }
    
    // 使用ffmpeg解码器
    AVFormatContext *pFormatCtx;
    
    av_register_all();
    avformat_network_init();
    pFormatCtx = avformat_alloc_context();
    
    if (avformat_open_input(&pFormatCtx, [path UTF8String], NULL, NULL) != 0){
        NSLog(@"VideoInfo::Couldn't open input stream");
        return 0;
    }
    
    if (avformat_find_stream_info(pFormatCtx, NULL) < 0){
        NSLog(@"VideoInfo::Couldn't find stream information");
        return 0;
    }
    
    NSInteger total = pFormatCtx->duration / AV_TIME_BASE;
    
    avformat_close_input(&pFormatCtx);
    
    return total;
}

+ (void)setProgressWithProgress:(double)progress filePath:(NSString *)filePath {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setDouble:progress forKey:[@"progress#" stringByAppendingString:filePath]];
    [userDefaults synchronize];
}

+ (double)progressWithFilePath:(NSString *)filePath {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    double progress = [userDefaults doubleForKey:[@"progress#" stringByAppendingString:filePath]];
    return progress;
}

@end
