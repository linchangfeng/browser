//
//  VideoProgress.h
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoProgress : NSObject

+ (void)setProgressWithProgress:(double)progress filePath:(NSString *)filePath;
+ (double)progressWithFilePath:(NSString *)filePath;

@end
