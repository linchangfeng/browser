//
//  FFMpegWrapper.h
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FFmpegWrapperCreateScreenshotCompletionHandler) (NSString *screenshotPath);

@interface FFmpegWrapper : NSObject

+ (instancetype)defaultWrapper;
- (void)extractSubtitleFromVideo:(NSString *)videoPath;

- (void)createScreenshotOfVideoAtPath:(NSString*)path atTime:(NSTimeInterval)time size:(CGSize)size completion:(FFmpegWrapperCreateScreenshotCompletionHandler)completion;
- (NSTimeInterval)durationOfVideoAtPath:(NSString*)path;

@end
