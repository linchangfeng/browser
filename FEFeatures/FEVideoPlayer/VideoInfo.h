//
//  VideoInfo.h
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoInfo : NSObject

// 获取视频时长
+ (NSInteger)durationWithPath:(NSString *)path;

// 获取播放进度
+ (double)progressWithFilePath:(NSString *)filePath;

// 设置播放进度
+ (void)setProgressWithProgress:(double)progress filePath:(NSString *)filePath;

@end
