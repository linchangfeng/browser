//
//  VideoViewController.m
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "FEVideoPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "FFmpegWrapper.h"
#import "SubtitleDisplay.h"
#import "TimeSeek.h"
#import "VideoNavigationBarView.h"
#import "VideoLeftSideBar.h"
#import "VideoRightSideBar.h"
#import "VideoControlPanel.h"
#import "VideoRepeatBar.h"
#import "VideoSubtitleControl.h"
#import "VideoInfo.h"
#import <IJKMediaFramework/IJKMediaFramework.h>
#import <MediaPlayer/MPVolumeView.h>
#import <Masonry.h>
#import <MBProgressHUD.h>
#import "FEVideoPlayer.h"

#import "VideoSubtitleSettingViewController.h"
#import "VideoScreenScaleSettingViewController.h"
#import "VideoPlaylistViewController.h"
#import "VideoSpeedSlider.h"

static const CGFloat kTimeSeekWidth = 160;
static const CGFloat kTimeSeekHeight = 100;

@interface FEVideoPlayerViewController ()<UIPopoverPresentationControllerDelegate>

// 播放器实例
@property (nonatomic, strong) id<IJKMediaPlayback> player;

// 进度条
@property (nonatomic) BOOL isSliderDraging;
@property (nonatomic, strong) NSTimer *sliderTimer;
@property (nonatomic, strong) NSTimer *timer;

// 字幕
@property (nonatomic, strong) SubtitleDisplay *subDisplay;
@property (nonatomic, strong) TimeSeek *timeSeek;

// 视频控制
@property (nonatomic, strong) VideoNavigationBarView *topBar;
@property (nonatomic, strong) VideoLeftSideBar *leftBar;
@property (nonatomic, strong) VideoRightSideBar *rightBar;
@property (nonatomic, strong) VideoControlPanel *controlPanel;
@property (nonatomic, strong) VideoRepeatBar *repeatBar;
@property (nonatomic, strong) VideoSubtitleControl *subtitleControl;

@property (nonatomic) double repeatStart;
@property (nonatomic) double repeatEnd;
@property (nonatomic, strong) NSTimer *repeatTimer;

@property (nonatomic, strong) VideoSpeedSlider *speedSlider;

@property (nonatomic) CGFloat playerSizeRatio; // 高/长

@property (nonatomic, copy) NSString *folderPath;
@property (nonatomic, strong) UITableView *subtitleFinder;

@property (nonatomic) CGPoint panStart;
@property (nonatomic) CGPoint panEnd;

// 控制界面是否允许旋转
@property (nonatomic) BOOL autoRotate;
@property (nonatomic) BOOL firstLandscape;
@property (nonatomic) UIInterfaceOrientation orientation;

// status bar 控制
@property (nonatomic) BOOL isStatusBarHidden;

// 屏幕锁定
@property (nonatomic) BOOL isScreenLocked;
@property (nonatomic, strong) UIButton *unlock;
@property (nonatomic, strong) UIView *unlockBackground;

// 音量控制
@property (nonatomic) MPVolumeView *volumView;

// 亮度控制
@property (nonatomic, strong) MBProgressHUD *brightnessHUD;

@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation FEVideoPlayerViewController

- (instancetype)initWithItems: (NSMutableArray *)items atIndex:(NSInteger)index {
    self = [super init];
    
    if (self) {
        self.autoRotate = YES;
        self.isStatusBarHidden = NO;
        self.isScreenLocked = NO;
        self.playerSizeRatio = -1;
        self.index = index;
        self.items = items;
        self.filePath = items[index];
        
        self.firstLandscape = YES;
    }
    
    return self;
}

- (instancetype)initWithFilePath:(NSString *)filePath {
    self = [super init];
    
    if (self) {
        self.autoRotate = YES;
        self.filePath = filePath;
        self.isStatusBarHidden = NO;
        self.isScreenLocked = NO;
        self.playerSizeRatio = -1;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    FFmpegWrapper *wrapper = [FFmpegWrapper defaultWrapper];
    
    [wrapper extractSubtitleFromVideo:self.filePath];
    
    [self setupPlayer];
    [self setupTimeSeek];
    [self setupTopBar];
    [self setupLeftBar];
    [self setupRightBar];
    [self setupControlPanel];
    [self setupUnlock];
    [self setupPlayerGesture];
    
    __weak FEVideoPlayerViewController *weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf setupSubDisplay];
    });
    
    /**
     *  开始生成 设备旋转 通知
     */
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    
    /**
     *  添加 设备旋转 通知
     *
     *  当监听到 UIDeviceOrientationDidChangeNotification 通知时，调用handleDeviceOrientationDidChange:方法
     *  @param handleDeviceOrientationDidChange: handleDeviceOrientationDidChange: description
     *
     *  @return return value description
     */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDeviceOrientationDidChange:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil
     ];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupMovieNotificationObservers];
    
    [self.player prepareToPlay];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subtitleFontFamilyChanged:) name:kUDKeySubtitleFontFamily object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subtitleFontSizeChanged:) name:kUDKeySubtitleFontSize object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subtitleFontColorChanged:) name:kUDKeySubtitleFontColor object:nil];
    
    return;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDKeySubtitleFontFamily object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDKeySubtitleFontSize object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUDKeySubtitleFontColor object:nil];
    
    FESetUserDefaults(kUDKeyVideoScale, nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - 初始化组件

- (void)setupPlayer {
    [IJKFFMoviePlayerController setLogReport:NO];
    [IJKFFMoviePlayerController setLogLevel:k_IJK_LOG_SILENT];
    
    IJKFFOptions *options = [IJKFFOptions optionsByDefault];
    // 开启硬解码可能会导致视频画面比例错误
    //    [options setPlayerOptionIntValue:1 forKey:@"videotoolbox"];
    
    NSURL *fileURL = [NSURL fileURLWithPath:self.filePath];
    
//    if ([self.filePath containsString:@"http://"]) {
//        fileURL = [NSURL URLWithString:self.filePath];
//    }
    
    self.player = [[IJKFFMoviePlayerController alloc] initWithContentURL:fileURL withOptions:options];
    self.player.view.autoresizesSubviews = NO;
    
    self.player.scalingMode = IJKMPMovieScalingModeAspectFit;
    
    self.player.shouldAutoplay = NO;
    
    // 读取视频旋转角度并进行调整
    NSUInteger degree = 0;
    
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:self.filePath]];
    NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    if ([tracks count] > 0) {
        AVAssetTrack *videoTrack = [tracks objectAtIndex:0];
        CGAffineTransform t = videoTrack.preferredTransform;
        
        if (t.a == 0 && t.b == 1.0 && t.c == -1.0 && t.d == 0) {
            // Portrait
            degree = 90;
        } else if (t.a == 0 && t.b == -1.0 && t.c == 1.0 && t.d == 0) {
            // PortraitUpsideDown
            degree = 270;
        } else if (t.a == 1.0 && t.b == 0 && t.c == 0 && t.d == 1.0) {
            // LandscapeRight
            degree = 0;
        } else if (t.a == -1.0 && t.b == 0 && t.c == 0 && t.d == -1.0) {
            // LandscapeLeft
            degree = 180;
        }
    }
    
    CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI * degree / 180.0f);
    self.player.view.transform = transform;
    
    [self.view addSubview:self.player.view];
}

- (void)setupSubDisplay {
    if (![self isSubtitleExist]) {
        return;
    }
    
    NSString *videoName = [self.filePath lastPathComponent];
    NSString *folder = [[self.filePath stringByDeletingLastPathComponent] lastPathComponent];
    NSString *prefix = [NSString stringWithFormat:@"%@#%@_sub", folder, videoName];
    NSString *subtitleName = [NSString stringWithFormat:@"%@%d.srt", prefix, 0];
    NSString *subtitlePath = [kUtil.libraryPath stringByAppendingPathComponent:subtitleName];
    
    NSLog(@"subtitleName: %@", [NSString stringWithFormat:@"%@%d.srt", prefix, 0]);
    
    _subDisplay = [[SubtitleDisplay alloc] initWithFrame:CGRectZero andFilePath:subtitlePath];
    [self.view addSubview:_subDisplay];
    [self.view bringSubviewToFront:self.controlPanel];
}

- (void)setupUnlock {
    
    self.unlockBackground = [[UIView alloc] init];
    self.unlockBackground.backgroundColor = FEVideoPlayerUnlockBackgroundColor;
    self.unlockBackground.hidden = YES;
    
    self.unlock = [[UIButton alloc] init];
    [self.unlock setImage:[UIImage imageNamed:@"UnLock"] forState:UIControlStateNormal];
    [self.unlock addTarget:self action:@selector(actionUnlock) forControlEvents:UIControlEventTouchUpInside];
    [self.unlockBackground addSubview:self.unlock];
    [self.view addSubview:self.unlockBackground];
}

- (void)setupPlayerGesture {
    // 通过滑动改变视频进度
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self.view addGestureRecognizer:pan];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnPlayer:)];
    [self.view addGestureRecognizer:tap];
}

- (void)tapOnPlayer:(UITapGestureRecognizer *)sender {
    if (self.isScreenLocked) {
        self.unlockBackground.hidden = !self.unlockBackground.hidden;
        return;
    }
    
    self.isStatusBarHidden = !self.isStatusBarHidden;
    [UIApplication sharedApplication].statusBarHidden = self.isStatusBarHidden;
    [self setNeedsStatusBarAppearanceUpdate];
    self.topBar.hidden = !self.topBar.hidden;
    
    self.controlPanel.hidden = !self.controlPanel.hidden;
    self.leftBar.hidden = !self.leftBar.hidden;
    self.rightBar.hidden = !self.rightBar.hidden;
    self.subtitleControl.hidden = YES;
    self.speedSlider.hidden = YES;
    
    self.repeatBar.hidden = !self.repeatBar.hidden;

    if (self.rightBar.speed.selected) {
        self.rightBar.speed.selected = NO;
    }
}

- (void)setupTapGesture {
    // 点击 slider 改变位置
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSlider:)];
    [self.topBar.progressSlider addGestureRecognizer:tap];
}

- (void)setupTimeSeek {
    self.timeSeek = [[TimeSeek alloc] init];
    [self.timeSeek setHidden:YES];
    [self.view addSubview:self.timeSeek];
}

- (void)setupTopBar {
    __weak FEVideoPlayerViewController *weakSelf = self;
    self.topBar = [[VideoNavigationBarView alloc] initWithDismissAction:^{
        [weakSelf stop];
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    }];

    self.isSliderDraging = NO;
    
    // 开始拖动
    [self.topBar.progressSlider addTarget:self action:@selector(sliderStart:) forControlEvents:UIControlEventTouchDown];
    // 结束拖动
    [self.topBar.progressSlider addTarget:self action:@selector(sliderEnd:) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar.progressSlider addTarget:self action:@selector(sliderEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [self.topBar.progressSlider addTarget:self action:@selector(sliderEnd:) forControlEvents:UIControlEventTouchCancel];
    // 拖动过程
    [self.topBar.progressSlider addTarget:self action:@selector(sliderDragging:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.topBar];
}

- (void)setupLeftBar {
    __weak FEVideoPlayerViewController *weakSelf = self;
    self.leftBar = [[VideoLeftSideBar alloc] initWithLockRotateAction:^{
        
        self.autoRotate = !self.autoRotate;
        
    } repeatAction:^{
        
        weakSelf.repeatStart = 0;
        weakSelf.repeatEnd = weakSelf.player.duration;
        if (!weakSelf.repeatBar) {
            weakSelf.repeatBar = [[VideoRepeatBar alloc] initWithStartAction:^{
                if (!weakSelf.repeatTimer) {
                    weakSelf.repeatTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:weakSelf selector:@selector(actionRepeatPlay) userInfo:nil repeats:YES];
                }
                if (weakSelf.repeatBar.start.selected) {
                    weakSelf.repeatStart = weakSelf.player.currentPlaybackTime;
                    [weakSelf.repeatBar setStartTitleWithTime:weakSelf.repeatStart];
                } else {
                    weakSelf.repeatStart = 0;
                    [weakSelf.repeatBar setStartTitleWithTime:weakSelf.repeatStart];
                }
            } endAction:^{
                if (!weakSelf.repeatTimer) {
                    weakSelf.repeatTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:weakSelf selector:@selector(actionRepeatPlay) userInfo:nil repeats:YES];
                }
                if (weakSelf.repeatBar.end.selected) {
                    weakSelf.repeatEnd = weakSelf.player.currentPlaybackTime;
                    [weakSelf.repeatBar setEndTitleWithTime:weakSelf.player.currentPlaybackTime];
                } else {
                    weakSelf.repeatEnd = weakSelf.player.duration;
                    [weakSelf.repeatBar setEndTitleWithTime:weakSelf.repeatEnd];
                }
            }];
            
            [weakSelf.repeatBar setStartTitleWithTime:weakSelf.repeatStart];
            [weakSelf.repeatBar setEndTitleWithTime:weakSelf.repeatEnd];
            [weakSelf.view addSubview:weakSelf.repeatBar];
        } else {
            [weakSelf.repeatTimer invalidate];
            weakSelf.repeatTimer = nil;
            weakSelf.repeatBar.hidden = YES;
            weakSelf.repeatBar = nil;
        }
        
    } subtitleAction:^{
        
        if (!weakSelf.subtitleControl) {
            VideoSubtitleSettingViewController *videoSubtitleSettingVC = [[VideoSubtitleSettingViewController alloc] init];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:videoSubtitleSettingVC];
            
            CGFloat viewHeight, viewWidth;
            if (self.view.height > self.view.width) {
                viewHeight = self.view.height;
                viewWidth = self.view.width;
            } else {
                viewHeight = self.view.width;
                viewWidth = self.view.height;
            }
            
            CGFloat videoSubtitleSettingVCHeight = viewWidth - 140;
            CGFloat videoSubtitleSettingVCWidth = videoSubtitleSettingVCHeight * 204 / 235;
            
            if ([kUtil screenInch] > FEScreenInch4_7) {
                videoSubtitleSettingVCHeight = 235;
                videoSubtitleSettingVCWidth = 204;
            }
            
            videoSubtitleSettingVC.preferredContentSize = CGSizeMake(videoSubtitleSettingVCWidth, videoSubtitleSettingVCHeight);
            
            navigationController.modalPresentationStyle = UIModalPresentationPopover;
            UIPopoverPresentationController *popController = navigationController.popoverPresentationController;
            
            popController.delegate = self;
            popController.sourceView = self.leftBar;
            popController.sourceRect = self.leftBar.subtitle.frame;
            popController.backgroundColor = [UIColor whiteColor];
            
            navigationController.navigationBarHidden = YES;
            [self presentViewController:navigationController animated:YES completion:nil];
            
        } else {
            weakSelf.subtitleControl.hidden = !self.subtitleControl.hidden;
        }
    }];
    [self.view addSubview:self.leftBar];
}



- (void)actionRepeatPlay {
    if (self.player.currentPlaybackTime > self.repeatEnd || self.player.currentPlaybackTime < self.repeatStart - 1) {
        self.player.currentPlaybackTime = self.repeatStart;
    }
}

- (void)setupRightBar {
    __weak FEVideoPlayerViewController *weakSelf = self;
    self.rightBar = [[VideoRightSideBar alloc] initWithSpeedAction:^{
        if (!weakSelf.speedSlider) {
            weakSelf.speedSlider = [[VideoSpeedSlider alloc] initWithFrame:CGRectMake(0, 0, 170, 30)];
            weakSelf.speedSlider.centerY = weakSelf.view.centerY;
            weakSelf.speedSlider.centerX = CGRectGetWidth(weakSelf.view.bounds) - weakSelf.rightBar.width - 20;
            
            weakSelf.speedSlider.transform = CGAffineTransformMakeRotation(-M_PI_2);
            
            weakSelf.speedSlider.value = 1;
            weakSelf.speedSlider.minimumValue = 0.5;
            weakSelf.speedSlider.maximumValue = 4;
            
            [weakSelf.speedSlider setThumbImage:[self thumbImageForProcess:weakSelf.speedSlider.value] forState:UIControlStateNormal];
            
            weakSelf.speedSlider.tintColor = FEVideoPlayerSpeedSliderTintColor;
            weakSelf.speedSlider.maximumTrackTintColor = FEVideoPlayerSpeedSliderMaximumTrackTintColor;
            
            [weakSelf.speedSlider addTarget:weakSelf action:@selector(actionSpeedSlider) forControlEvents:UIControlEventValueChanged];
            [weakSelf.view addSubview:weakSelf.speedSlider];

        } else {
            weakSelf.speedSlider.hidden = !weakSelf.speedSlider.hidden;
        }
    } aspectAction:^{
        VideoScreenScaleSettingViewController *videoScreenScaleSettingViewController = [[VideoScreenScaleSettingViewController alloc] initWithAspectChangedBlock:^(CGFloat aspect, BOOL defaultSize) {
            
            if (defaultSize) {
                weakSelf.playerSizeRatio = -1;
                weakSelf.player.scalingMode = IJKMPMovieScalingModeAspectFit;
                [weakSelf viewWillLayoutSubviews];
            } else {
                weakSelf.playerSizeRatio = 1 / aspect;
                weakSelf.player.scalingMode = IJKMPMovieScalingModeAspectFill;
                [weakSelf viewWillLayoutSubviews];
            }

        }];
        
        CGFloat viewHeight, viewWidth;
        if (self.view.height > self.view.width) {
            viewHeight = self.view.height;
            viewWidth = self.view.width;
        } else {
            viewHeight = self.view.width;
            viewWidth =self.view.height;
        }
        
        CGFloat videoSubtitleSettingVCHeight = viewWidth - 140;
        CGFloat videoSubtitleSettingVCWidth = videoSubtitleSettingVCHeight * 204 / 235;
        
        if ([kUtil screenInch] > FEScreenInch5_5) {
            videoSubtitleSettingVCHeight = 235;
            videoSubtitleSettingVCWidth = 204;
        }
        
        videoScreenScaleSettingViewController.preferredContentSize = CGSizeMake(videoSubtitleSettingVCWidth, videoSubtitleSettingVCHeight);
        
        videoScreenScaleSettingViewController.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popController = videoScreenScaleSettingViewController.popoverPresentationController;
        
        popController.delegate = self;
        popController.sourceView = self.rightBar;
        popController.sourceRect = self.rightBar.aspect.frame;
        popController.backgroundColor = [UIColor whiteColor];
        
        [self presentViewController:videoScreenScaleSettingViewController animated:YES completion:nil];

    } listAction:^{
        VideoPlaylistViewController *videoPlaylistViewController = [[VideoPlaylistViewController alloc] initWithlist:self.items selectedAction:^(NSInteger index) {
            weakSelf.filePath = weakSelf.items[index];
            weakSelf.index = index;
            [weakSelf actionChangePlayer];
        }];
        
        videoPlaylistViewController.index = weakSelf.index;
        
        CGFloat viewHeight, viewWidth;
        if (self.view.height > self.view.width) {
            viewHeight = self.view.height;
            viewWidth = self.view.width;
        } else {
            viewHeight = self.view.width;
            viewWidth =self.view.height;
        }
        
        CGFloat videoSubtitleSettingVCHeight = viewWidth - 140;
        CGFloat videoSubtitleSettingVCWidth = videoSubtitleSettingVCHeight * 204 / 235;
        
        if ([kUtil screenInch] >= FEScreenInch5_5) {
            videoSubtitleSettingVCHeight = 235;
            videoSubtitleSettingVCWidth = 204;
        }
        
        videoPlaylistViewController.preferredContentSize = CGSizeMake(videoSubtitleSettingVCWidth, videoSubtitleSettingVCHeight);
        
        videoPlaylistViewController.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *popController = videoPlaylistViewController.popoverPresentationController;
        
        popController.permittedArrowDirections = UIPopoverArrowDirectionRight;
        popController.delegate = self;
        popController.sourceView = self.rightBar;
        popController.sourceRect = self.rightBar.list.frame;
        popController.backgroundColor = [UIColor whiteColor];

        [self presentViewController:videoPlaylistViewController animated:YES completion:nil];
    }];
    [self.view addSubview:self.rightBar];
}

- (void)actionSpeedSlider {
    [self.speedSlider setThumbImage:[self thumbImageForProcess:self.speedSlider.value] forState:UIControlStateNormal];
    self.player.playbackRate = self.speedSlider.value;
}

- (void)setupControlPanel {
    __weak FEVideoPlayerViewController *weakSelf = self;
    self.controlPanel = [[VideoControlPanel alloc] initWithPlayAction:^{
        [weakSelf.player play];
    } pauseAction:^{
        [weakSelf.player pause];
    } lockAction:^{
        weakSelf.isScreenLocked = YES;
        [weakSelf hideStatusBar];
        weakSelf.topBar.hidden = YES;
        weakSelf.controlPanel.hidden = YES;
        weakSelf.leftBar.hidden = YES;
        weakSelf.rightBar.hidden = YES;
        weakSelf.subtitleControl.hidden = YES;
        weakSelf.repeatBar.hidden = YES;
        weakSelf.unlockBackground.hidden = NO;
        weakSelf.speedSlider.hidden = YES;
        weakSelf.rightBar.speed.selected = NO;
    } rewindAction:^{
        if (self.index != 0) {
            self.index--;

            weakSelf.filePath = self.items[self.index];
            [weakSelf actionChangePlayer];
        } else {
            return;
        }

    } forwardAction:^{
        if (self.index != self.items.count - 1) {
            self.index++;
            
            weakSelf.filePath = self.items[self.index];
            [weakSelf actionChangePlayer];
        } else {
            return;
        }
    }];
    [self.view addSubview:self.controlPanel];
}

- (void)actionChangePlayer {
    FFmpegWrapper *wrapper = [FFmpegWrapper defaultWrapper];
    
    [wrapper extractSubtitleFromVideo:self.filePath];

    [self removeMovieNotificationObservers];
    [self.player shutdown];
    [self.player.view removeFromSuperview];
    
    IJKFFOptions *options = [IJKFFOptions optionsByDefault];
    // 开启硬解码可能会导致视频画面比例错误
    //    [options setPlayerOptionIntValue:1 forKey:@"videotoolbox"];
    
    NSURL *fileURL = [NSURL fileURLWithPath:self.filePath];
    
    self.player = [[IJKFFMoviePlayerController alloc] initWithContentURL:fileURL withOptions:options];
    self.player.view.autoresizesSubviews = NO;
    [self setupMovieNotificationObservers];
    
    self.player.scalingMode = IJKMPMovieScalingModeAspectFit;
    self.player.shouldAutoplay = NO;
    [self.player prepareToPlay];
    
    // 读取视频旋转角度并进行调整
    NSUInteger degree = 0;
    
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:self.filePath]];
    NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    if ([tracks count] > 0) {
        AVAssetTrack *videoTrack = [tracks objectAtIndex:0];
        CGAffineTransform t = videoTrack.preferredTransform;
        
        if (t.a == 0 && t.b == 1.0 && t.c == -1.0 && t.d == 0) {
            // Portrait
            degree = 90;
        } else if (t.a == 0 && t.b == -1.0 && t.c == 1.0 && t.d == 0) {
            // PortraitUpsideDown
            degree = 270;
        } else if (t.a == 1.0 && t.b == 0 && t.c == 0 && t.d == 1.0) {
            // LandscapeRight
            degree = 0;
        } else if (t.a == -1.0 && t.b == 0 && t.c == 0 && t.d == -1.0) {
            // LandscapeLeft
            degree = 180;
        }
    }
    
    CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI * degree / 180.0f);
    self.player.view.transform = transform;
    [self.view addSubview:self.player.view];

    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.player play];
    });
    
    [self.view bringSubviewToFront:self.timeSeek];
    [self.view bringSubviewToFront:self.topBar];
    [self.view bringSubviewToFront:self.leftBar];
    [self.view bringSubviewToFront:self.rightBar];
    [self.view bringSubviewToFront:self.controlPanel];
    [self.view bringSubviewToFront:self.unlockBackground];
    [self.view bringSubviewToFront:self.speedSlider];
    
    self.speedSlider.value = 1.0;
    [self.speedSlider setThumbImage:[self thumbImageForProcess:1.0] forState:UIControlStateNormal];
    
    [self setupPlayerGesture];
    
    [self setupTimer];
    [self.controlPanel changePlayButton:VideoPlayStatePlaying];
    
    __weak FEVideoPlayerViewController *weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf setupSubDisplay];
    });
}

- (void)setupTimer {
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    }
}

- (void)disableTimer {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - 设置组件大小和位置

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGFloat playerHeight = CGRectGetWidth(self.view.bounds) * 9 / 16;
    
    CGFloat bottomCenterY = self.view.bottom - 50/2;
    CGFloat subDisplayCenterY = MIN(self.view.halfHeight + playerHeight/2, bottomCenterY);
    
    self.subDisplay.frame = CGRectMake(0, 0, self.view.width, 50);
    self.subDisplay.center = CGPointMake(self.view.halfWidth, subDisplayCenterY);

    if (self.playerSizeRatio < 0) {
        self.player.view.frame = self.view.bounds;
    } else {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        if (orientation == UIInterfaceOrientationPortrait) {
            CGFloat playerHeight = self.player.view.width * self.playerSizeRatio;
            self.player.view.frame = CGRectMake(0, (self.view.height-playerHeight)/2, self.view.width, playerHeight);
        } else {
            CGFloat playerWidth = self.player.view.height / self.playerSizeRatio;
            self.player.view.frame = CGRectMake((self.view.width-playerWidth)/2, 0, playerWidth, self.view.height);
        }
    }
    
    self.timeSeek.frame = CGRectMake(0, 0, kTimeSeekWidth, kTimeSeekHeight);
    self.timeSeek.center = self.view.center;
    
    self.topBar.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 70);
    
    self.leftBar.frame = CGRectMake(0, 0, 60, 3 * 44 + 2 * 16);
    self.leftBar.centerY = self.view.centerY;
    
    self.rightBar.frame = CGRectMake(0, 0, 60, 3 * 44 + 2 * 16);
    self.rightBar.right = self.view.right;
    self.rightBar.centerY = self.view.centerY;
    
    self.controlPanel.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) - 70, CGRectGetWidth(self.view.bounds), 70);
    
    
    self.unlockBackground.frame = CGRectMake(15, 0, 44, 44);
    self.unlockBackground.centerY = self.view.bottom - 35;
    self.unlockBackground.layer.cornerRadius = self.unlockBackground.halfHeight;
    self.unlockBackground.layer.masksToBounds = YES;
    self.unlock.frame = CGRectMake(0, 0, 44, 44);
    
    self.repeatBar.frame = CGRectMake(0, 0, 265, 45);
    self.repeatBar.centerX = self.view.centerX;
    self.repeatBar.bottom = self.view.bottom - self.controlPanel.height - 20;
    
    self.subtitleControl.width = self.view.width / 2;
    self.subtitleControl.height = 200;
    self.subtitleControl.center = self.view.center;
    
    self.speedSlider.centerY = self.view.centerY;
    self.speedSlider.centerX = CGRectGetWidth(self.view.bounds) - self.rightBar.width - 20;
}

#pragma mark - 停止播放
- (void)stop {
    [self disableTimer];
    [self.repeatTimer invalidate];
    
    [VideoInfo setProgressWithProgress:self.player.currentPlaybackTime filePath:self.filePath];
    
    [self.player shutdown];
    [self.player.view removeFromSuperview];
    [self.leftBar removeFromSuperview];
    [self.rightBar removeFromSuperview];
    [self removeMovieNotificationObservers];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIDeviceOrientationDidChangeNotification
                                                  object:nil
     ];
    
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

#pragma mark - 滑动相关的动作

- (void)sliderStart:(UISlider *)sender {
    self.isSliderDraging = YES;
    [self.player pause];
    self.sliderTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(sliderTimerAction) userInfo:nil repeats:YES];
}

- (void)sliderTimerAction {
    self.player.currentPlaybackTime = self.player.duration * self.topBar.progressSlider.value / 100.0f;
    [self.topBar setTimeDisplayWithTime:(self.player.duration * self.topBar.progressSlider.value / 100.0f) duration:self.player.duration];
}

- (void)sliderDragging:(UISlider *)sender {
    [self.topBar setTimeDisplayWithTime:(self.player.duration * sender.value / 100.0f) duration:self.player.duration];
}

- (void)sliderEnd:(UISlider *)sender {
    [self.sliderTimer invalidate];

    [self.topBar setTimeDisplayWithTime:(self.player.duration * sender.value / 100.0f) duration:self.player.duration];
    
    self.player.currentPlaybackTime = self.player.duration * sender.value / 100.0f;
    NSLog(@"sliderEnd:%lf", self.player.currentPlaybackTime);
    [self.player play];
    
    self.isSliderDraging = NO;
}

- (void)pan:(UIPanGestureRecognizer *)sender {
    if (self.isScreenLocked) {
        return;
    }
    typedef NS_ENUM(NSUInteger, UIPanGestureRecognizerDirection) {
        UIPanGestureRecognizerDirectionUndefined,
        UIPanGestureRecognizerDirectionUp,
        UIPanGestureRecognizerDirectionDown,
        UIPanGestureRecognizerDirectionLeft,
        UIPanGestureRecognizerDirectionRight
    };
    
    static UIPanGestureRecognizerDirection direction = UIPanGestureRecognizerDirectionUndefined;
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan: {
            if (direction == UIPanGestureRecognizerDirectionUndefined) {
                CGPoint velocity = [sender velocityInView:self.view];
                BOOL isVerticalGesture = fabs(velocity.y) > fabs(velocity.x);
                
                if (isVerticalGesture) {
                    if (velocity.y > 0) {
                        direction = UIPanGestureRecognizerDirectionDown;
                    } else {
                        direction = UIPanGestureRecognizerDirectionUp;
                    }
                    // 初始化音量控制
                    CGPoint locationPoint = [sender locationInView:self.view];
                    if (locationPoint.x > self.view.halfWidth) {
                        self.volumView = [[MPVolumeView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
                    }
                } else {
                    if (velocity.x > 0) {
                        direction = UIPanGestureRecognizerDirectionRight;
                    } else {
                        direction = UIPanGestureRecognizerDirectionLeft;
                    }
                    CGPoint translatedPoint = [sender translationInView:self.view];
                    CGFloat x = sender.view.center.x + translatedPoint.x;
                    CGFloat y = sender.view.center.y + translatedPoint.y;
                    self.panStart = CGPointMake(x, y);
                    self.isSliderDraging = YES;
                    [self.timeSeek setHidden:NO];
                }
            }
        } break;
        case UIGestureRecognizerStateEnded: {
            switch (direction) {
                case UIPanGestureRecognizerDirectionUp:
                case UIPanGestureRecognizerDirectionDown:
                    [self handleUpAndDownGesture:sender];
                    break;
                case UIPanGestureRecognizerDirectionLeft:
                case UIPanGestureRecognizerDirectionRight:
                    [self handleLeftAndRightGesture:sender];
                    break;
                default: {
                    break;
                }
            }
            direction = UIPanGestureRecognizerDirectionUndefined;
            self.volumView.hidden = YES;
            self.volumView = nil;
            [self.brightnessHUD hideAnimated:YES];
            self.brightnessHUD = nil;
        } break;
        default: {
            switch (direction) {
                case UIPanGestureRecognizerDirectionUp:
                case UIPanGestureRecognizerDirectionDown:
                    [self handleUpAndDownGesture:sender];
                    break;
                case UIPanGestureRecognizerDirectionLeft:
                case UIPanGestureRecognizerDirectionRight:
                    [self handleLeftAndRightGesture:sender];
                    break;
                default:
                    break;
            }
        } break;
    }
    
}

- (void)handleUpAndDownGesture:(UIPanGestureRecognizer *)sender {
    CGPoint translatedPoint = [sender translationInView:self.view];
    CGFloat y = translatedPoint.y;
    CGFloat diff = (-y)/self.view.height/20;
    
    if (self.volumView) {
        UISlider* volumeViewSlider = nil;
        for (UIView *view in [self.volumView subviews]){
            if ([view.class.description isEqualToString:@"MPVolumeSlider"]) {
                volumeViewSlider = (UISlider*)view;
                break;
            }
        }
        
        // retrieve system volume
        float systemVolume = volumeViewSlider.value;
        
        // change system volume, the value is between 0.0f and 1.0f
        [volumeViewSlider setValue:systemVolume+diff animated:YES];
    } else {
        if (!self.brightnessHUD) {
            self.brightnessHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            self.brightnessHUD.mode = MBProgressHUDModeText;
        }
        
        CGFloat brightness = [UIScreen mainScreen].brightness;
        [[UIScreen mainScreen] setBrightness:brightness+diff];
        
        self.brightnessHUD.label.text = [NSString stringWithFormat:@"%@: %.0lf%%", _(@"亮度"), [UIScreen mainScreen].brightness*100];
    }
}

// 横向移动
- (void)handleLeftAndRightGesture:(UIPanGestureRecognizer *)sender {
    CGPoint translatedPoint = [sender translationInView:self.view];
    CGFloat x = sender.view.center.x + translatedPoint.x;
    CGFloat diff = x - self.panStart.x;
    CGFloat delta = (diff / CGRectGetWidth(self.view.bounds)) * self.player.duration;
    CGFloat nextPosition = self.player.currentPlaybackTime+delta;
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan: break;
        case UIGestureRecognizerStateEnded: {
            if (self.player.duration > 0) {
                if (nextPosition < 0) {
                    self.player.currentPlaybackTime = 0;
                } else if (nextPosition > self.player.duration) {
                    self.player.currentPlaybackTime = self.player.duration;
                } else {
                    self.player.currentPlaybackTime = nextPosition;
                }
            }
            self.isSliderDraging = NO;
            [self.timeSeek setDiffText:0];
            [self.timeSeek setHidden:YES];
        } break;
        case UIGestureRecognizerStateFailed:
        case UIGestureRecognizerStateCancelled: {
            self.isSliderDraging = NO;
            [self.timeSeek setHidden:YES];
        } break;
        default: {
            if (self.player.duration > 0) {
                if (nextPosition < 0) {
                    [self.timeSeek setTimeText:0 duration:self.player.duration];
                } else if (nextPosition > self.player.duration) {
                    [self.timeSeek setTimeText:self.player.duration duration:self.player.duration];
                } else {
                    [self.timeSeek setDiffText:delta];
                    [self.timeSeek setTimeText:self.player.currentPlaybackTime+delta duration:self.player.duration];
                }
            }
            self.isSliderDraging = NO;
        } break;
    }
}

- (void)tapOnSlider:(UITapGestureRecognizer *)sender {
    NSLog(@"%s", __FUNCTION__);
    
    [self.player pause];
    CGPoint location = [sender locationInView:self.topBar.progressSlider];
    [self.topBar.progressSlider setValue:location.x * 100 / self.topBar.progressSlider.bounds.size.width];
    self.player.currentPlaybackTime = self.topBar.progressSlider.value * self.player.duration / 100;
    [self.player play];
}

#pragma mark - 定时动作

- (void)timerAction {
    if (!self.isSliderDraging && self.player.duration > 0.01) {
        [self.topBar.progressSlider setValue:(self.player.currentPlaybackTime * 100.0f / self.player.duration) animated:YES];
        [self.subDisplay displayByTime:self.player.currentPlaybackTime];
        [self.topBar setTimeDisplayWithTime:self.player.currentPlaybackTime duration:self.player.duration];
        NSLog(@"currentPlaybackTime = %lf", self.player.currentPlaybackTime);
    }
}

#pragma mark - 视频旋转控制

- (BOOL)shouldAutorotate {
    return self.autoRotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    if ([FEUserDefaultsValueForKey(kUDKeyVideoLandscape) boolValue]) {
        return UIInterfaceOrientationLandscapeRight;
    } else {
        return UIInterfaceOrientationPortrait;
    }
}

- (void)handleDeviceOrientationDidChange:(UIInterfaceOrientation)interfaceOrientation
{
//    if (!self.shouldAutorotate) {
//        [[UIApplication sharedApplication] setStatusBarOrientation:self.orientation animated:YES];
//    }
    //1.获取 当前设备 实例
    UIDevice *device = [UIDevice currentDevice];

    
    /**
     *  2.取得当前Device的方向，Device的方向类型为Integer
     *
     *  必须调用beginGeneratingDeviceOrientationNotifications方法后，此orientation属性才有效，否则一直是0。orientation用于判断设备的朝向，与应用UI方向无关
     *
     *  @param device.orientation
     *
     */
    switch (device.orientation) {
        case UIDeviceOrientationFaceUp:
            NSLog(@"屏幕朝上平躺");
            break;
            
        case UIDeviceOrientationFaceDown:
            NSLog(@"屏幕朝下平躺");
            break;
            
            //系統無法判斷目前Device的方向，有可能是斜置
        case UIDeviceOrientationUnknown:
            NSLog(@"未知方向");
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            NSLog(@"屏幕向左横置");
            break;
            
        case UIDeviceOrientationLandscapeRight:
            NSLog(@"屏幕向右橫置");
            break;
            
        case UIDeviceOrientationPortrait:
            NSLog(@"屏幕直立");
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            NSLog(@"屏幕直立，上下顛倒");
            break;
            
        default:
            NSLog(@"无法辨识");
            break;
    }
}

#pragma mark - 视频锁定控制

- (void)actionUnlock {
    self.unlockBackground.hidden = YES;
    self.controlPanel.hidden = NO;
    self.topBar.hidden = NO;
    self.leftBar.hidden = NO;
    self.rightBar.hidden = NO;
    [self showStatusBar];
    self.isScreenLocked = NO;
}

#pragma mark - 字幕操作

- (void)changeSubDisplay:(NSString *)subtitlePath {
    if (!self.subDisplay) {
        self.subDisplay = [[SubtitleDisplay alloc] initWithFrame:CGRectZero andFilePath:subtitlePath];
        [self.view addSubview:_subDisplay];
        [self.view bringSubviewToFront:self.controlPanel];
    } else {
        [self.subDisplay changeFilePath:subtitlePath];
    }
}

#pragma mark - Check Subtitle File

- (BOOL)isSubtitleExist {
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *videoName = [self.filePath lastPathComponent];
    NSString *folder = [[self.filePath stringByDeletingLastPathComponent] lastPathComponent];
    NSString *prefix = [NSString stringWithFormat:@"%@#%@_sub", folder, videoName];
    NSString *subtitleName = [NSString stringWithFormat:@"%@%d.srt", prefix, 0];
    NSString *subtitlePath = [kUtil.libraryPath stringByAppendingPathComponent:subtitleName];
    
    NSLog(@"sub: %@", subtitlePath);
    
    BOOL isDirectory = NO;
    BOOL exist = [fm fileExistsAtPath:subtitlePath isDirectory:&isDirectory];
    
    return exist && !isDirectory;
}

- (BOOL)isSubtitleNameValid:(NSString *)fileName {
    NSString *ext = [[fileName lowercaseString] pathExtension];
    return [ext isEqualToString:@"srt"] || [ext isEqualToString:@"ass"];
}

#pragma mark - 状态栏控制

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return self.isStatusBarHidden;
}

- (void)showStatusBar {
    self.isStatusBarHidden = NO;
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)hideStatusBar {
    self.isStatusBarHidden = YES;
    [self setNeedsStatusBarAppearanceUpdate];
}

#pragma mark - 初始化通知中心

- (void)setupMovieNotificationObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidChange:)
                                                 name:IJKMoviePlayerPlaybackStateDidChangeNotification
                                               object:_player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidFinish:)
                                                 name:IJKMoviePlayerPlaybackDidFinishNotification
                                               object:_player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                 name:IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                               object:_player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoButtonDeselected)
                                                 name:FEVideoPlayerVideoButtonDeselectedNotification
                                               object:nil];
}

#pragma mark - 移除通知中心

- (void)removeMovieNotificationObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:IJKMPMoviePlayerPlaybackStateDidChangeNotification
                                                  object:_player];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:IJKMPMoviePlayerPlaybackDidFinishNotification
                                                  object:_player];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:IJKMPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                                  object:_player];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:FEVideoPlayerVideoButtonDeselectedNotification
                                                  object:nil];
}

- (void)appDidEnterBackground {
    [self.player pause];
}

#pragma mark - 视频状态改变

- (void)moviePlayBackStateDidChange:(NSNotification *)notification {
    switch (_player.playbackState) {
        case IJKMPMoviePlaybackStateStopped: {
            [self.controlPanel changePlayButton:VideoPlayStateStopped];
            [self disableTimer];
            break;
        }
        case IJKMPMoviePlaybackStatePlaying: {
            [self.controlPanel changePlayButton:VideoPlayStatePlaying];
            [self setupTimer];
            break;
        }
        case IJKMPMoviePlaybackStatePaused: {
            [self.controlPanel changePlayButton:VideoPlayStateStopped];
            [self disableTimer];
            break;
        }
        case IJKMPMoviePlaybackStateInterrupted: {
            [self.controlPanel changePlayButton:VideoPlayStateStopped];
            [self disableTimer];
            break;
        }
        default: {
            NSLog(@"IJKMPMoviePlayBackStateDidChange %d: unknown", (int)_player.playbackState);
            break;
        }
    }
}

- (void)moviePlayBackStateDidFinish:(NSNotification *)notification {
    int reason = [[[notification userInfo] valueForKey:IJKMPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
    
    if (reason == IJKMPMovieFinishReasonPlaybackEnded) {
        if ([FEUserDefaultsValueForKey(kUDKeyVideoAutoPlayNext) boolValue]) {
            if (self.index != self.items.count - 1) {
                self.index++;
                
                self.filePath = self.items[self.index];
                [self actionChangePlayer];
            }
        }
    }
}

- (void)mediaIsPreparedToPlayDidChange:(NSNotification *)notification {
    // 将播放视频加入到播放列表中
    NSMutableDictionary *playItem = [NSMutableDictionary dictionary];
    NSString *fileName = [self.filePath lastPathComponent];
    NSString *folder = [[self.filePath stringByDeletingLastPathComponent] lastPathComponent];
    playItem[@"path"] = [folder stringByAppendingPathComponent:fileName];
    playItem[@"date"] = [NSDate date];
    playItem[@"name"] = [self.filePath lastPathComponent];
    playItem[@"duration"] = [NSNumber numberWithDouble:self.player.duration];
    
    [self.player setCurrentPlaybackTime:[VideoInfo progressWithFilePath:self.filePath]];
    [self.player play];
    
    [self.topBar.progressSlider setValue:(self.player.currentPlaybackTime * 100.0f / self.player.duration) animated:YES];
    [self.subDisplay displayByTime:self.player.currentPlaybackTime];
    [self.topBar setTimeDisplayWithTime:self.player.currentPlaybackTime duration:self.player.duration];
}

#pragma mark - UIPopoverPresentationControllerDelegate
- (UIModalPresentationStyle) adaptivePresentationStyleForPresentationController: (UIPresentationController * ) controller {
    return UIModalPresentationNone;
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    [self videoButtonDeselected];
    return YES;
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller traitCollection:(UITraitCollection *)traitCollection {
    
    return UIModalPresentationNone;
}

#pragma mark - Subtitle font changed notification
- (void)subtitleFontFamilyChanged:(NSNotification *)notification {
    UIFont *font = (UIFont *)[notification object];
    NSLog(@"font: %@", font);
    self.subDisplay.subtitleText.font = font;
}

- (void)subtitleFontSizeChanged:(NSNotification *)notification {
    NSInteger size = [(NSNumber *)[notification object] integerValue];
    NSLog(@"size: %ld", (long)size);
    self.subtitleControl.fontSize = size;
    self.subDisplay.subtitleText.font = [UIFont fontWithName:FEUserDefaultsValueForKey(kUDKeySubtitleFontFamily) size:size];
}

- (void)subtitleFontColorChanged:(NSNotification *)notification {
    UIColor *color = (UIColor *)[notification object];
    NSLog(@"color: %@", color);
    self.subtitleControl.fontColor = color;
    self.subDisplay.subtitleText.textColor = color;
}

#pragma mark - Util

- (UIImage *)thumbImageForProcess:(CGFloat)process {
    CGSize size = CGSizeMake(25, 25);
    
    UIView *thumbView = [[UIView alloc] init];
    thumbView.size = size;
    thumbView.origin = CGPointMake(0, 0);
    thumbView.backgroundColor = FEVideoPlayerSpeedSliderThumbnailColor;
    thumbView.layer.cornerRadius = thumbView.halfHeight;
    thumbView.layer.masksToBounds = YES;
    
    UIView *contentView = [[UIView alloc] init];
    contentView.size = CGSizeMake(size.width - 2, size.height - 2);
    contentView.center = thumbView.center;
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = contentView.halfHeight;
    contentView.layer.masksToBounds = YES;
    
    UILabel *speed = [[UILabel alloc] init];
    speed.size = CGSizeMake(20, 20);
    speed.center = thumbView.center;
    speed.text = [NSString stringWithFormat:@"%0.1fx", process];
    speed.textColor = FEVideoPlayerSppedSliderThumbnailTextColor;
    speed.textAlignment = NSTextAlignmentCenter;
    speed.transform = CGAffineTransformMakeRotation(M_PI_2);
    [speed setFont:[UIFont systemFontOfSize:9]];
    
    [thumbView addSubview:speed];
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    [thumbView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSLog(@"image: %@", image);
    return image;
}

- (void)videoButtonDeselected {
    self.leftBar.subtitle.selected = NO;
    self.rightBar.aspect.selected = NO;
    self.rightBar.list.selected = NO;
}

@end
