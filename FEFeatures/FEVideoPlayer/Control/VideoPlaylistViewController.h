//
//  VideoPlaylistViewController.h
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoPlaylistViewController : UIViewController

@property (nonatomic) NSInteger index;

- (instancetype)initWithlist:(NSArray *)list selectedAction:(void (^)(NSInteger))block;

@end
