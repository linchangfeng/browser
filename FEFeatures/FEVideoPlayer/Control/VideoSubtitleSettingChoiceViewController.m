//
//  VideoSubtitleSettingChoiceViewController.m
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoSubtitleSettingChoiceViewController.h"
#import "VideoSettingTableViewCell.h"
#import "Masonry.h"
#import "FEVideoPlayer.h"

@interface VideoSubtitleSettingChoiceViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *data;

@property (nonatomic, strong) UIView *titleContentView;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation VideoSubtitleSettingChoiceViewController

- (instancetype)initWithSettingType:(SubtileSettingType)settingType {
    self = [super init];
    
    self.settingType = settingType;
    
    [self initialData];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
    switch (self.settingType) {
        case SubtitleSettingTypeFontFamily: {
            NSString *fontFamily = FEUserDefaultsValueForKey(kUDKeySubtitleFontFamily);
            if (fontFamily != nil) {
                NSInteger location = [self.data indexOfObject:fontFamily];
                self.index = location;
            }
            break;
        }
        case SubtitleSettingTypeFontSize: {
            NSInteger subtitleFontSize = FEUserDefaultsFloatForKey(kUDKeySubtitleFontSize);
            if (subtitleFontSize != 0) {
                self.index = [self.data indexOfObject:[NSString stringWithFormat:@"%ld %@", (long)subtitleFontSize, _(@"号字体")]];
            }
            break;
        }
        case SubtitleSettingTypeFontColor: {
            NSString *subtitleFontColor = FEUserDefaultsValueForKey(kUDKeySubtitleFontColor);
            if (subtitleFontColor != nil) {
                self.index = [self.data indexOfObject:subtitleFontColor];
            }
        }
        default:
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.index inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Table view delegate & data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoSettingTableViewCell *cell = (VideoSettingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.label.text = self.data[indexPath.row];
    
    [cell.label setFont:[UIFont systemFontOfSize:FEVideoPlayerSecondFontSize]];
    [cell.label setLineBreakMode:NSLineBreakByTruncatingMiddle];
    
    if (indexPath.row == self.index) {
        cell.label.textColor = FEVideoPlayerSettingSelectedColor;
    } else {
        cell.label.textColor = FEVideoPlayerSettingDiselectedColor;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (self.settingType) {
        case SubtitleSettingTypeFontFamily: {
            UIFont *font = [UIFont fontWithName:self.data[indexPath.row] size:FEUserDefaultsIntegerForKey(kUDKeySubtitleFontSize)];
            FESetUserDefaults(kUDKeySubtitleFontFamily, self.data[indexPath.row]);
            [[NSNotificationCenter defaultCenter] postNotificationName:kUDKeySubtitleFontFamily object:font];
        }
            break;
            
        case SubtitleSettingTypeFontSize: {
            NSString *fontSizeString = self.data[indexPath.row];
            NSRange range = [fontSizeString rangeOfString:@"\\d+" options:NSRegularExpressionSearch];
            NSInteger fontSize = [[fontSizeString substringToIndex:range.length] integerValue];
            [[NSUserDefaults standardUserDefaults] setInteger:fontSize forKey:kUDKeySubtitleFontSize];
            [[NSNotificationCenter defaultCenter] postNotificationName:kUDKeySubtitleFontSize object:[NSNumber numberWithFloat:fontSize]];
        }
            break;
            
        case SubtitleSettingTypeFontColor: {
            UIColor *fontColor;
            switch (indexPath.row) {
                case 0:{
                    fontColor = [UIColor whiteColor];
                }
                    break;
                case 1: {
                    fontColor = [UIColor blackColor];
                }
                    break;
                case 2: {
                    fontColor = [UIColor redColor];
                }
                    break;
                case 3: {
                    fontColor = [UIColor blueColor];
                }
                    break;
                case 4: {
                    fontColor = [UIColor yellowColor];
                }
                    break;
                default:
                    fontColor = [UIColor blackColor];
                    break;
                    
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:self.data[indexPath.row] forKey:kUDKeySubtitleFontColor];
            [[NSNotificationCenter defaultCenter] postNotificationName:kUDKeySubtitleFontColor object:fontColor];
            
        }
            break;
            
        default:
            break;
    }
    
    self.index = indexPath.row;
    [self.tableView reloadData];

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Util
- (void)initialData {
    switch (self.settingType) {
        case SubtitleSettingTypeFontFamily:
            [self allSystemFonts];
            break;
        case SubtitleSettingTypeFontSize: {
            NSString *format = [NSString stringWithFormat:@"%%d %@", _(@"号字体")];
            self.data = [NSMutableArray arrayWithObjects:
                         [NSString stringWithFormat:format, 10],
                         [NSString stringWithFormat:format, 12],
                         [NSString stringWithFormat:format, 14],
                         [NSString stringWithFormat:format, 16],
                         [NSString stringWithFormat:format, 18], nil];
        }
            break;
        case SubtitleSettingTypeFontColor:
            self.data = [NSMutableArray arrayWithObjects:_(@"白色"), _(@"黑色"), _(@"红色"), _(@"蓝色"), _(@"黄色"), nil];
            break;
        default:
            break;
    }
    
    [self.tableView reloadData];
}

- (void)setupUI {
    self.titleContentView = [[UIView alloc] init];
    self.titleContentView.backgroundColor = [UIColor clearColor];
    
    self.backButton = [[UIButton alloc] init];
    [self.backButton setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    self.backButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.backButton.imageEdgeInsets = UIEdgeInsetsMake(9, 0, 9, 0);
    [self.backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel = [[UILabel alloc] init];
    [self.titleLabel setFont:[UIFont systemFontOfSize:FEVideoPlayerSecondFontSize weight:UIFontWeightBold]];
    
    switch (self.settingType) {
        case SubtitleSettingTypeFontFamily:
            self.titleLabel.text = _(@"字体");
            break;
        case SubtitleSettingTypeFontSize:
            self.titleLabel.text = _(@"字体大小");
            break;
        case SubtitleSettingTypeFontColor:
            self.titleLabel.text = _(@"字体颜色");
            break;
        default:
            break;
    }
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerClass:[VideoSettingTableViewCell class] forCellReuseIdentifier:@"cell"];
    
    [self.titleContentView addSubview:self.backButton];
    [self.titleContentView addSubview:self.titleLabel];
    [self.view addSubview:self.titleContentView];
    [self.view addSubview:self.tableView];
    
    [self addConstraint];
}

- (void)addConstraint {
    [self.titleContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.view);
        make.height.mas_equalTo(47);
    }];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(32);
        make.left.equalTo(self.titleContentView).offset(10);
        make.centerY.equalTo(self.titleContentView);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.titleContentView);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleContentView.mas_bottom);
        make.width.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

- (void)allSystemFonts {
    NSArray *familys = [UIFont familyNames];
    self.data = [NSMutableArray arrayWithArray:familys];
}

#pragma mark - Button action
- (void)backButtonAction {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
