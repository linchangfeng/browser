//
//  VideoNavigationBarView.h
//  VideoDemo
//
//  Created by flowdev on 16/4/14.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoNavigationBarView : UIView

@property (nonatomic, strong) UISlider *progressSlider;
@property (nonatomic, strong) UIButton *dismiss;

- (instancetype)initWithDismissAction:(void (^)())dismissAction;

- (void)setTimeDisplayWithTime:(double)time duration:(double)duration;

@end
