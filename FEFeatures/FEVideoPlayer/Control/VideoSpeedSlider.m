//
//  VideoSpeedSlider.m
//  Coolplayer
//
//  Created by flowdev on 16/7/5.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoSpeedSlider.h"

@implementation VideoSpeedSlider

- (CGRect)trackRectForBounds:(CGRect)bounds {
    bounds.size.height = 5;
    return bounds;
}

@end
