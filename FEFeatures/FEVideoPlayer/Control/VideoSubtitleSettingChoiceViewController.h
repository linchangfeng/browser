//
//  VideoSubtitleSettingChoiceViewController.h
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SubtileSettingType){
    SubtitleSettingTypeFontFamily = 1 << 0,
    SubtitleSettingTypeFontSize = 1 << 1,
    SubtitleSettingTypeFontColor = 1 << 2
};

@interface VideoSubtitleSettingChoiceViewController : UIViewController

@property (nonatomic) SubtileSettingType settingType;
@property (nonatomic) NSInteger index;

- (instancetype)initWithSettingType:(SubtileSettingType)settingType;

@end
