//
//  VideoSettingTableViewCell.h
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoSettingTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *label;

@end
