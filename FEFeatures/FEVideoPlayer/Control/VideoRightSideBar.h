//
//  VideoRightSideBar.h
//  Coolplayer
//
//  Created by flowdev on 16/5/16.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoRightSideBar : UIView

@property (nonatomic, strong) UIButton *aspect;
@property (nonatomic, strong) UIButton *speed;
@property (nonatomic, strong) UIButton *list;

- (instancetype)initWithSpeedAction:(void (^)())aSpeedAction
                       aspectAction:(void (^)())aAspectAction
                         listAction:(void (^)())aListAction;

@end
