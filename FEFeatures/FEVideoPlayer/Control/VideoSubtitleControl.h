//
//  VideoSubtitleControl.h
//  Coolplayer
//
//  Created by flowdev on 16/5/17.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoSubtitleControl : UIView

@property (nonatomic) NSInteger fontSize;
@property (nonatomic, strong) UIColor *fontColor;
@property (nonatomic, strong) UITableView *tableView;

- (instancetype)initWithCellAction:(void (^)(NSInteger cellIndex))cellAction;

@end
