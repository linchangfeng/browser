//
//  VideoControlPanel.h
//  VideoDemo
//
//  Created by flowdev on 16/4/15.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, VideoPlayState) {
    VideoPlayStatePlaying,
    VideoPlayStateStopped
};

@interface VideoControlPanel : UIView

- (instancetype)initWithPlayAction:(void (^)())playAction
                       pauseAction:(void (^)())pauseAction
                        lockAction:(void (^)())lockAction
                      rewindAction:(void (^)())rewindAction
                     forwardAction:(void (^)())forwardAction;

- (void)changePlayButton:(VideoPlayState)state;

@end
