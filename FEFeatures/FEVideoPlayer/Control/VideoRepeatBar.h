//
//  VideoRepeatBar.h
//  Coolplayer
//
//  Created by flowdev on 16/5/17.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoRepeatBar : UIView

@property (nonatomic, strong) UIButton *start;
@property (nonatomic, strong) UIButton *end;

- (instancetype)initWithStartAction:(void (^)())aStartAction
                          endAction:(void (^)())endAction;

- (void)setStartTitleWithTime:(double)time;

- (void)setEndTitleWithTime:(double)time;

@end
