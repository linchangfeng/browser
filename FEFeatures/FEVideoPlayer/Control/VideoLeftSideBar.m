//
//  VideoLeftSideBar.m
//  Coolplayer
//
//  Created by flowdev on 16/5/16.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoLeftSideBar.h"
#import <Masonry.h>

#define UIColorFromRGB(rgbValue, alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]

//static const NSInteger kBackgroundColor = 0x3A3A3A;
static const CGFloat kButtonWidth = 44;
static const CGFloat kButtonHeight = 44;

@interface VideoLeftSideBar ()

@property (nonatomic, copy) void (^lockRotateAction)();
@property (nonatomic, copy) void (^repeatAction)();
@property (nonatomic, copy) void (^subtitleAction)();

@property (nonatomic, strong) UIButton *lockRotate;


@end

@implementation VideoLeftSideBar

- (instancetype)initWithLockRotateAction:(void (^)())aLockRotateAction
                            repeatAction:(void (^)())aRepeatAction
                          subtitleAction:(void (^)())aSubtitleAction {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.lockRotateAction = aLockRotateAction;
        self.repeatAction = aRepeatAction;
        self.subtitleAction = aSubtitleAction;
        
        self.lockRotate = [[UIButton alloc] init];
        [self.lockRotate setImage:[UIImage imageNamed:@"LockRotate"] forState:UIControlStateNormal];
        [self.lockRotate setImage:[UIImage imageNamed:@"LockRotate2"] forState:UIControlStateSelected];
        [self.lockRotate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.lockRotate addTarget:self action:@selector(actionLockRotate) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.lockRotate];
        
        self.repeat = [[UIButton alloc] init];
        [self.repeat setImage:[UIImage imageNamed:@"Circulation"] forState:UIControlStateNormal];
        [self.repeat setImage:[UIImage imageNamed:@"Circulation2"] forState:UIControlStateSelected];
        [self.repeat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.repeat addTarget:self action:@selector(actionRepeat) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.repeat];
        
        self.subtitle = [[UIButton alloc] init];
        [self.subtitle setImage:[UIImage imageNamed:@"Group"] forState:UIControlStateNormal];
        [self.subtitle setImage:[UIImage imageNamed:@"Group2"] forState:UIControlStateSelected];
        [self.subtitle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.subtitle addTarget:self action:@selector(actionSubtitle) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.subtitle];
        
        [self setupConstraints];
    }
    return self;
}

- (void)actionLockRotate {
    if (self.lockRotateAction) {
        self.lockRotate.selected = !self.lockRotate.selected;
        self.lockRotateAction();
    }
}

- (void)actionRepeat {
    if (self.repeatAction) {
        self.repeat.selected = !self.repeat.selected;
        self.repeatAction();
    }
}

- (void)actionSubtitle {
    if (self.subtitleAction) {
        self.subtitle.selected = !self.subtitle.selected;
        self.subtitleAction();
    }
}

- (void)setupConstraints {
    [self.lockRotate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kButtonWidth, kButtonHeight));
        make.left.equalTo(self).offset(10);
        make.top.equalTo(self);
    }];
    
    [self.repeat mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kButtonWidth, kButtonHeight));
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(10);
    }];

    [self.subtitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kButtonWidth, kButtonHeight));
        make.bottom.equalTo(self);
        make.left.equalTo(self).offset(10);
    }];

}

@end
