//
//  VideoProgress.m
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoProgress.h"

@implementation VideoProgress

+ (void)setProgressWithProgress:(double)progress filePath:(NSString *)filePath {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setDouble:progress forKey:[@"progress#" stringByAppendingString:filePath]];
    [userDefaults synchronize];
}

+ (double)progressWithFilePath:(NSString *)filePath {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    double progress = [userDefaults doubleForKey:[@"progress#" stringByAppendingString:filePath]];
    return progress;
}

@end
