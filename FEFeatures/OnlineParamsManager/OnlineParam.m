//
//  OnlineParam.m
//  Coolplayer
//
//  Created by Tracy on 7/8/16.
//  Copyright © 2016 flowdev. All rights reserved.
//

#import "OnlineParam.h"

@interface OnlineParam ()

@end

@implementation OnlineParam

- (NSInteger)integerValue {
    @try {
        return [_value integerValue];
    }
    @catch (NSException *e) {
        return 0;
    }
}

- (BOOL)boolValue {
    @try {
        return [_value boolValue];
    }
    @catch (NSException *e) {
        return NO;
    }
}

- (void)setRawValue:(id)rawValue {
    _rawValue = rawValue;
    _value = rawValue;
    
    if ([_rawValue isKindOfClass:[NSString class]]) {
        NSString *text = [rawValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ([text hasPrefix:@"{"] && [text hasSuffix:@"}"]) { // JSON
            NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            if ([dict.allKeys containsObject:@"OTHERS"] || [dict.allKeys containsObject:@"CN"] || [dict.allKeys containsObject:@"US"]) {
                NSDictionary *localizations = dict;
                _value = localizations[[self countryCode]];
                if (_value == nil) {
                    _value = localizations[@"OTHERS"];
                }
            }
            else {
                _value = dict;
            }
        }
        else if ([text hasPrefix:@"["] && [text hasSuffix:@"]"]) { // Array
            NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
            _value = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
    }
}

#pragma mark - Private
- (NSString*)countryCode {
    NSLocale *currentLocale = [NSLocale currentLocale];
    return [currentLocale objectForKey:NSLocaleCountryCode];
}

@end
