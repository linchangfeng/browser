//
//  OnlineParam.h
//  Coolplayer
//
//  Created by Tracy on 7/8/16.
//  Copyright © 2016 flowdev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnlineParam : NSObject

// input
@property (nonatomic, strong) id rawValue;
@property (nonatomic, strong) NSString *name;

// ouput
@property (nonatomic, readonly) id value;
@property (nonatomic, readonly) NSInteger integerValue;
@property (nonatomic, readonly) BOOL boolValue;

@end
