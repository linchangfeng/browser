//
//  AlbumVideoViewController.h
//  Coolplayer
//
//  Created by flowdev on 16/4/27.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumVideoPlayerViewController : UIViewController

- (instancetype)initWithAssetURL:(NSURL *)aURL;

@end
