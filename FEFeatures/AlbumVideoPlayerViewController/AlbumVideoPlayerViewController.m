//
//  AlbumVideoViewController.m
//  Coolplayer
//
//  Created by flowdev on 16/4/27.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "AlbumVideoPlayerViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AlbumVideoPlayerViewController ()

@property (nonatomic, strong) AVPlayerViewController *playerView;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVAudioSession *session;
@property (nonatomic, strong) NSURL *assetURL;

@end

@implementation AlbumVideoPlayerViewController

- (instancetype)initWithAssetURL:(NSURL *)aURL {
    self = [super init];
    
    if (self) {
        self.assetURL = aURL;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupSession];
    [self setupPlayerView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupSession {
    _session = [AVAudioSession sharedInstance];
    [_session setCategory:AVAudioSessionCategoryPlayback error:nil];
}

- (void)setupPlayerView {
    _player = [AVPlayer playerWithURL:self.assetURL];
    
    _playerView = [[AVPlayerViewController alloc] init];
    _playerView.player = _player;
    _playerView.videoGravity = AVLayerVideoGravityResizeAspect;
    _playerView.showsPlaybackControls = true;
    
    // 禁用 palyerview 内部 autolayout, 消除报错
    _playerView.view.translatesAutoresizingMaskIntoConstraints = YES;
    
    [self addChildViewController:_playerView];
    [self.view addSubview:_playerView.view];
    [_playerView.player play];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _playerView.view.frame = CGRectMake(0, self.navigationController.navigationBar.bounds.size.height, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - self.navigationController.navigationBar.bounds.size.height);
}

@end
