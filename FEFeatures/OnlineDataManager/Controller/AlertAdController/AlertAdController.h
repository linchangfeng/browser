//
//  AlertAdController.h
//  FreeMusic
//
//  Created by Tina on 16/8/7.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import "ADManager.h"
#import <FECommon/FECommon.h>

@interface AlertAdController : FEAlertController

- (id)initWithAdInfo:(ADAlertInfo*)adInfo resultHandler:(void (^)(BOOL confirmed))resultHandler;

@end
