//
//  ImageAdViewController.h
//  HiFolder
//
//  Created by Tracy on 15/8/1.
//
//

#import <UIKit/UIKit.h>
#import "ADManager.h"

@interface ImageAdViewController : UIViewController

@property (nonatomic, strong) ADImageInfo *adInfo;

@property (nonatomic, strong) void (^completionHandler)(ImageAdViewController *controller, BOOL canceled);

- (void)startEnterAnimation;

@end




