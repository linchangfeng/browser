//
//  ImageAdViewController.m
//  HiFolder
//
//  Created by Tracy on 15/8/1.
//
//

#import "ImageAdViewController.h"
#import <FECommon/FECommon.h>

@interface ImageAdViewController () {
    UIButton *_button;
    UIButton *_cancelButton;
}

@end

@implementation ImageAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = FEHexRGBA(0x000000, 0.5);
    
    _button = [[UIButton alloc] init];
    _button.imageView.contentMode = UIViewContentModeScaleAspectFill;
    _button.layer.cornerRadius = 4.0;
    _button.layer.masksToBounds = YES;
    [_button addTarget:(id)self action:@selector(buttonHandler:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_button];
    
    
    _cancelButton = [[UIButton alloc] init];
    [_cancelButton setImage:[UIImage imageNamed:@"CancelImageAd"] forState:UIControlStateNormal];
    [_cancelButton addTarget:(id)self action:@selector(cancelButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_cancelButton];
    
    if (_adInfo) {
        [self setAdInfo:_adInfo];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _button.width = self.view.width * 0.8;
    _button.height = _button.width * 1.2;
    _button.center = CGPointMake(self.view.width/2.0, self.view.height/2.0);
    
    _cancelButton.frame = CGRectMake(0, 0, 30, 30);
    _cancelButton.center = CGPointMake(_button.right, _button.y);
}

#pragma mark - Handler
- (void)buttonHandler:(id)sender {
    NSString *urlString = [_adInfo.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    if (url) {
        [[UIApplication sharedApplication] openURL:url];
    }
    else {
        [[FEUtil sharedInstance] openAppInAppStoreWithAppID:_adInfo.app.appID];
    }
    
    [ADManager setAppDownloaded:_adInfo.app.appID];
    
    if (self.completionHandler) {
        self.completionHandler(self, NO);
    }
}

- (void)cancelButtonHandler:(id)sender {
    if (self.completionHandler) {
        self.completionHandler(self, YES);
    }
}

#pragma mark - Public
- (void)setAdInfo:(ADImageInfo *)adInfo {
    _adInfo = adInfo;
    
    UIImage *image = [UIImage imageWithContentsOfFile:_adInfo.imagePath];
    [_button setImage:image forState:UIControlStateNormal];
}

- (void)startEnterAnimation
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    
    CATransform3D scale2 = CATransform3DMakeScale(1.05, 1.05, 1);
    CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
    
    NSArray *frameValues = [NSArray arrayWithObjects:
                            [NSValue valueWithCATransform3D:scale2],
                            [NSValue valueWithCATransform3D:scale4],
                            nil];
    
    [animation setValues:frameValues];
    
    NSArray *frameTimes = [NSArray arrayWithObjects:
                           [NSNumber numberWithFloat:0.1],
                           [NSNumber numberWithFloat:1.0],
                           nil];
    [animation setKeyTimes:frameTimes];
    
    animation.fillMode = kCAFillModeForwards;
    animation.duration = 0.1;
    animation.repeatCount = 1;
    
    [_button.layer addAnimation:animation forKey:nil];
}

@end
