//
//  NoticesManager.m
//  Photorange
//
//  Created by Darlene on 14-1-8.
//
//

#import "NoticesManager.h"
#import "OnlineDataManager.h"
#import <FECommon/FECommon.h>

#define kUDKeyNoticesExistUnreadUpdatesFlag @"NoticesManager.m.1"
#define kUDKeyNoticesVersion @"NoticesManager.m.2"

@implementation NoticesManager

+ (void)work
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{
                                                              kUDKeyNoticesVersion: @1,
                                                              kUDKeyNoticesExistUnreadUpdatesFlag: @YES}];
}

+ (void)updateToVersion:(int)version
{
    FESetUserDefaults(kUDKeyNoticesVersion, [NSNumber numberWithInt:version]);
    FESetUserDefaults(kUDKeyNoticesExistUnreadUpdatesFlag, @YES);
    [[NSNotificationCenter defaultCenter] postNotificationName:NoticesVersionDidUpdateNotification object:nil];
}

+ (NSURL*)remoteNoticesURL {
    return [OnlineDataManager noticeURL];
}

+ (BOOL)existUnreadUpdates
{
    return FEUserDefaultsBoolForKey(kUDKeyNoticesExistUnreadUpdatesFlag);
}

+ (void)hasReadUpdates
{
    FESetUserDefaults(kUDKeyNoticesExistUnreadUpdatesFlag, @NO);
}

+ (int)currentVersion
{
    return FEUserDefaultsIntegerForKey(kUDKeyNoticesVersion);
}

@end
