//
//  ResourceVersionManager.m
//  iVault
//
//  Created by Darlene on 14-1-8.
//
//

#import "OnlineDataManager.h"
#import "AFNetworking.h"

static NSString * appKey;

@implementation OnlineDataManager

+ (void)setupWithAppKey:(NSString *)key {
    appKey = key;
}

+ (void)run {
    if (appKey == nil) {
        [NSException raise:@"OnlineDataManagerError" format:@"先调用+ [OnlineDataManager setupWithAppKey:]"];
    }
    
    [ADManager run];
    
    [self checkVersions];
}

+ (NSURL*)versionURL {
    NSString *urlString = [NSString stringWithFormat:@"https://flowever.net/onlinedata/%@/versions.json", appKey];
    return [NSURL URLWithString:urlString];
}

+ (NSURL*)adURL {
    NSString *urlString = [NSString stringWithFormat:@"https://flowever.net/onlinedata/%@/ad.zip", appKey];
    return [NSURL URLWithString:urlString];
}

+ (NSURL*)noticeURL {
    NSString *urlString = [NSString stringWithFormat:@"https://flowever.net/onlinedata/%@/notices.html", appKey];
    return [NSURL URLWithString:urlString];
}

+ (void)checkVersions
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *url = [self versionURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
#ifdef DEBUG
    NSLog(@"%s %@", __func__, url);
#endif
    
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id JSON, NSError *error) {
        if (error) {
#ifdef DEBUG
            NSLog(@"%s OnlineDataManager Error", __func__);
#endif
            return;
        }
        
        NSLog(@"versions %@", JSON);
        
        //notifications
        int version = [JSON[@"notices"] intValue];
        int localVersion = [NoticesManager currentVersion];
        if (version != localVersion) {
            NSLog(@"update notices version");
            [NoticesManager updateToVersion:version];
        }
        
        //ad
        version = [JSON[@"ad"] intValue];
        localVersion = [ADManager currentVersion];
        if (version != localVersion) {
            NSLog(@"update ad version");
            [ADManager updateToVersion:version];
        }

    }];
    
    [task resume];
}
@end
