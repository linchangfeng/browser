//
//  ResourceVersionManager.h
//  iVault
//
//  Created by Darlene on 14-1-8.
//
//

#import <Foundation/Foundation.h>
#import "NoticesManager.h"
#import "ADManager.h"

@interface OnlineDataManager : NSObject

+ (void)setupWithAppKey:(NSString*)key;
+ (NSURL*)versionURL;
+ (NSURL*)adURL;
+ (NSURL*)noticeURL;

+ (void)run;

@end
